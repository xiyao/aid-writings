\documentclass[12pt]{article}  

\usepackage[margin=1in]{geometry}
\usepackage{amsmath}  
\usepackage{amssymb}  
\usepackage{amsfonts}  

\usepackage{textcomp, gensymb}

\usepackage{amsthm}

\usepackage{fancyhdr}
\pagestyle{fancy}  
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
\setlength{\headheight}{18pt}

\chead{}
\rhead{\textsc{Oral Exam}}
\lfoot{\today}
\cfoot{}
\rfoot{\thepage\ of \ref{NumPages}}

\makeatletter
\AtEndDocument{\immediate\write\@auxout{\string\newlabel{NumPages}{{\thepage}}}}
\makeatother

\usepackage{amsthm}
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\renewcommand*{\proofname}{\textbf{Solution}}

\begin{document}
    \begin{problem}
        Explain the implementation of Deconvolution Process.
    \end{problem}
    \textit{The key idea of our implementation is to transform both Convolution and Deconvolution into a matrix multiplication, so that we could reuse our Laws. We will first use several examples to demonstrate how it works, and then discuss the general cases.}

    \begin{proof}
        \underline{Example 1 (stride=1)}. Assume we have a $(3,3)$ input matrix $X$ and a $(2,2)$ filter matrix $F$ as below:
        
        $$X=\left[ {\begin{array}{*{20}c} 
			x_1 & x_2 & x_3  \\
            x_4 & x_5 & x_6  \\
            x_7 & x_8 & x_9  \\
            \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
                1 & 2 & 3  \\
                4 & 5 & 6  \\
                7 & 8 & 9  \\
                \end{array} } \right], 
          F=\left[ {\begin{array}{*{20}c} 
            f_1 & f_2 \\
            f_3 & f_4  \\
            \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
                1 & 2 \\
                3 & 4 
                \end{array} } \right]$$
        Then we first compute it using the method \textbf{Direct Computation (in page 14 of the report)}. We will slide the filter on the input matrix from up to bottom, from left to right with a unit stride. The sliding process will form four windows, and in each window we perform dot product. After sliding, we will have four windows:
        \begin{itemize}
            \item Upper left: $A=\left[ {\begin{array}{*{20}c} 
                x_1 & x_2 \\
                x_4 & x_5 
                \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
                    1 & 2 \\
                    4 & 5 
                    \end{array} } \right]$.
            \item Upper right: $B=\left[ {\begin{array}{*{20}c} 
                x_2 & x_3 \\
                x_5 & x_6 
                \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
                    2 & 3 \\
                    5 & 6 
                    \end{array} } \right]$.
            \item Bottom left: $C=\left[ {\begin{array}{*{20}c} 
                x_4 & x_5 \\
                x_7 & x_8 
                \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
                    4 & 5 \\
                    7 & 8 
                    \end{array} } \right]$.
            \item Bottom right: $D=\left[ {\begin{array}{*{20}c} 
                x_5 & x_6 \\
                x_8 & x_9 
                \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
                    5 & 6 \\
                    8 & 9 
                    \end{array} } \right]$.
        \end{itemize}
        Then we perform dot product of these windows and the filter, i.e. we now compute $F\cdot A , F\cdot B, F\cdot C$ and $F\cdot D$. We will have \begin{itemize}
            \item $y_1=F\cdot A=f_1x_1+f_2x_2+f_3x_4+f_4x_5=1*1+2*2+3*4+4*5=37$.
            \item $y_2=F\cdot B=f_1x_2+f_2x_3+f_3x_5+f_4x_6=1*2+2*3+3*5+4*6=47$.
            \item $y_3=F\cdot C=f_1x_4+f_2x_5+f_3x_7+f_4x_8=1*4+2*5+3*7+4*8=67$.
            \item $y_4=F\cdot D=f_1x_5+f_2x_6+f_3x_8+f_4x_9=1*5+2*6+3*8+4*9=77$.
        \end{itemize}
        Hence, from Direct Computation, we get the output as $Y=\left[ {\begin{array}{*{20}c} 
            y_1 & y_2 \\
            y_3 & y_4 
            \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
            37 & 47 \\
            67 & 77 
            \end{array} } \right]$.
        
        Now that we want to perform the convolution operation with matrix multiplication. To do so, we first flatten the input matrix into an $1$d vector. We will have:

            $$X^*=\left[ {\begin{array}{*{20}c} 
                x_1 & x_2 & \cdots & x_9 \\
                \end{array} } \right]^T=\left[ {\begin{array}{*{20}c} 
                    1 & 2 & \cdots & 9 \\
                    \end{array} } \right]^T$$
            
            The expected output will also become a $1$d vector $\left[ {\begin{array}{*{20}c} 
                y_1 & y_2 & y_3 & y_4 \\
                \end{array} } \right]^T$.

        So we have a $(9,1)$ input $X^*$ and a $(4,1)$ output. If we left multiply a $(4,9)$ matrix $W$ to the input, i.e. $WX^*$, we will get a $(4,1)$ output. Hence, the only problem is \textbf{how we construct such a matrix?} We will perform dot product of every row of this matrix with the input and the results of this dot product will form $\left[ {\begin{array}{*{20}c} 
            y_1^* & y_2^* & y_3^* & y_4^* \\
            \end{array} } \right]^T$. Hence, we could figure this matrix out in the following way:
        \begin{itemize}
            \item Assume the first row is $\left[ {\begin{array}{*{20}c} 
                a_1 & a_2 & \cdots & a_9 \\
                \end{array} } \right]$, then the result of dot product between this row and the input vector $X^*$ will be:
                $$y_1^*=a_1x_1+a_2x_2+\cdots+a_9x_9$$ 
            Our expected output is $y_1=f_1x_1+f_2x_2+f_3x_4+f_4x_5$, hence, we let $a_1=f_1=1, a_2=f_2=2, a_4=f_3=3, a_5=f_4=4$ and let others be zero. By doing so, We will have the expected result. Hence, the first row will be $\left[ {\begin{array}{*{20}c} 
                1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 & 0 \\
                \end{array} } \right]$.
            \item Similarly, the second row will be $\left[ {\begin{array}{*{20}c} 
                0 & 1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 \\
                \end{array} } \right]$. The third row will be $\left[ {\begin{array}{*{20}c} 
                    0 & 0 & 0 & 1 & 2 & 0 & 3 & 4 & 0 \\
                    \end{array} } \right]$. The forth row will be $\left[ {\begin{array}{*{20}c} 
                    0 & 0 & 0 & 0 & 1 & 2 & 0 & 3 & 4 \\
                \end{array} } \right]$.
            \item Finally, we will form the matrix as $W=\left[ {\begin{array}{*{20}c} 
                1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 & 0 \\
                0 & 1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 \\
                0 & 0 & 0 & 1 & 2 & 0 & 3 & 4 & 0 \\
                0 & 0 & 0 & 0 & 1 & 2 & 0 & 3 & 4 \\
                \end{array} } \right]$. The output will be $Y=WX^*=\left[ {\begin{array}{*{20}c} 
                    37 & 47 & 67 & 77 \\
                    \end{array} } \right]^T$.
        \end{itemize}
    
    By doing the above steps, we could transform the convolution operation into a matrix multiplication. 
    
    Now that we want to do the deconvolution, we also start from the direct computation. In direct computation, we take the element from $Y$ one by one, multiply each element with the filter matrix, and then place them back to a matrix that has the same shape as the input. More precisely, we follow the following steps:
    \begin{itemize}
        \item We know that the original input is a $3\times 3$ matrix, hence we first initialize a zero matrix with the shape $(3,3)$. 
        \item The first element is $37$ and it comes from the upper left corner. We multiply it with the filter matrix $\left[ {\begin{array}{*{20}c} f_1 & f_2 \\
            f_3 & f_4 
            \end{array} } \right]=\left[ {\begin{array}{*{20}c} 1 & 2 \\
            3 & 4 
            \end{array} } \right]$, so we will have $\left[ {\begin{array}{*{20}c}
                y_1 & y_2 \\
                y_3 & y_4
                \end{array} } \right]=\left[ {\begin{array}{*{20}c}
                37 & 74 \\
                121 &  148
                \end{array} } \right]$. Then we place it back to where it comes from: the upper left corner. Hence we will have $\left[ {\begin{array}{*{20}c}
                    f_1y_1 & f_2y_1 & 0 \\
                    f_3y_1 & f_4y_1 & 0 \\
                    0 & 0 & 0
                    \end{array} } \right]=\left[ {\begin{array}{*{20}c}
                    37 & 74 & 0 \\
                    121 & 148 & 0 \\
                    0 & 0 & 0
                    \end{array} } \right]$.
        \item Similarly, we will have another three matrix: $\left[ {\begin{array}{*{20}c}
            0 & f_1y_2 & f_2y_2 \\
            0 & f_3y_2 & f_4y_2 \\
            0 & 0 & 0
            \end{array} } \right]=\left[ {\begin{array}{*{20}c}
            0 & 47 & 94 \\
            0 & 141 & 188 \\
            0 & 0 & 0
            \end{array} } \right]$, $\left[ {\begin{array}{*{20}c}
                0 & 0 & 0 \\
                f_1y_3 & f_2y_3 & 0 \\
                f_3y_3 & f_4y_3 & 0 \\
                \end{array} } \right]=\left[ {\begin{array}{*{20}c}
                0 & 0 & 0 \\
                67 & 134 & 0 \\
                201 & 268 & 0
                \end{array} } \right]$ and $\left[ {\begin{array}{*{20}c}
                    0 & 0 & 0 \\
                    0 & f_1y_4 & f_2y_4 \\
                    0 & f_3y_4 & f_4y_4 \\
                    \end{array} } \right]=\left[ {\begin{array}{*{20}c}
                    0 & 0 & 0 \\
                    0 & 77 & 154 \\
                    0 & 231 & 308
                    \end{array} } \right]$.
        \item The final output is defined as the sum of these four matrices. We will have 
        
        $$X=\left[ {\begin{array}{*{20}c}
            f_1y_1 & f_2y_1+f_1y_2 & f_2y_2 \\
            f_1y_3+f_3y_1 & f_4y_1+f_3y_2+f_2y_3+f_1y_4 & f_4y_2+f_2y_4 \\
            f_3y_3 & f_4y_3+f_3y_4 & f_4y_4
            \end{array} } \right]=\left[ {\begin{array}{*{20}c}
                37 & 121 & 94 \\
                178 & 500 & 342 \\
                201 & 499 & 308
                \end{array} } \right]$$
    \end{itemize}

    Now we want to compute the Deconvolution by matrix multiplication. Similar to what we did in convolution, we need to find a $9\times 4$ matrix $W$ that multiplies $\left[ {\begin{array}{*{20}c}
        y_1 & y_2 & y_3 & y_4 \\
        \end{array} } \right]^T$, and we will solve this problem row by row.

    \begin{itemize}
        \item Assume that the first row is $\left[ {\begin{array}{*{20}c}
            a_1 & a_2 & a_3 & a_4 \\
            \end{array} } \right]$
    \end{itemize}
    
    \end{proof}


    \end{document}
