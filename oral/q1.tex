\documentclass[12pt]{article}  

\usepackage[margin=1in]{geometry}
\usepackage{amsmath}  
\usepackage{amssymb}  
\usepackage{amsfonts}  

\usepackage{textcomp, gensymb}

\usepackage{amsthm}

\usepackage{fancyhdr}
\pagestyle{fancy}  
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
\setlength{\headheight}{18pt}

\chead{}
\rhead{\textsc{Oral Exam}}
\lfoot{\today}
\cfoot{}
\rfoot{\thepage\ of \ref{NumPages}}

\makeatletter           
\AtEndDocument{\immediate\write\@auxout{\string\newlabel{NumPages}{{\thepage}}}}
\makeatother

\usepackage{amsthm}
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\renewcommand*{\proofname}{\textbf{Solution}}

\begin{document}
    \begin{problem}
        Assume a 1-layer fully connected neural network, and the input is a $(2,1)$ vector, and the output is a $(2,1)$ vector. The loss function is defined as $L=\frac{1}{2}(y-\hat{y})^2$, where $y$ is the ground truth and $\hat{y}$ is the prediction. This fully connected layer has the weight matrix $w=\left[ {\begin{array}{*{20}c} 
            w_{11} & w_{12} \\
            w_{21} & w_{22} 
        \end{array} } \right]$.

        Given an input instance $x=\left[ {\begin{array}{*{20}c} 
            x_{1} \\
            x_{2}
        \end{array} } \right]$ and the ground truth $y=\left[ {\begin{array}{*{20}c} 
            y_{1} \\
            y_{2}
        \end{array} } \right]$, the output of forward propagation is $\hat{y}=w\times x=\left[ {\begin{array}{*{20}c} 
            \hat{y}_{1} \\
            \hat{y}_{2}
        \end{array} } \right]$. Compute the $\frac{\partial L}{\partial w_{11}}$ in the back propagation using chain rule and demonstrate each step of calculation.
    \end{problem}

    \begin{proof}
        In the definition, $L=\frac{1}{2}(y-\hat{y})^2$. As $y$ and $\hat{y}$ are both $(2,1)$ vectors, then $y-\hat{y}$ will also become a $(2,1)$ vector. In this case, the $(y-\hat{y})^2$ will be the dot product between $y_1-\hat{y_1}$ and $y_2-\hat{y_2}$, i.e. $L=\frac{1}{2}[(y_1-\hat{y}_1)^2+(y_2-\hat{y}_2)^2]$.

    \null
    \noindent
    \textit{\underline{Method 1}}. Since there are two intermediate variables between $w$ and $L$: $y_1$ and $y_2$. So we have $\frac{\partial L}{\partial w_{11}}=\frac{\partial L}{\partial \hat{y}_1}\frac{\partial \hat{y}_1}{\partial w_{11}}+\frac{\partial L}{\partial \hat{y}_2}\frac{\partial \hat{y}_2}{\partial w_{11}}$ (1). Then we first compute $\frac{\partial L}{\partial \hat{y}_i}$, we will have 
    
    \begin{itemize}
        \item $\frac{\partial L}{\partial \hat{y}_1}=\frac{1}{2}\times 2 \times (y_1-\hat{y}_1)\times (-1)=\hat{y_1}-y_1$.
        \item Similarly, $\frac{\partial L}{\partial \hat{y}_2}=\hat{y_2}-y_2$.
    \end{itemize}
    
    Then we compute $\frac{\partial \hat{y}_1}{\partial w_{11}}$ and $\frac{\partial \hat{y}_2}{\partial w_{11}}$. To do so, we explicitly write the forms of $\hat{y}_1$ and $\hat{y}_2$. We will have 
    \begin{itemize}
        \item $\hat{y}=w\times x=\left[ {\begin{array}{*{20}c} 
            w_{11} & w_{12} \\
            w_{21} & w_{22} 
        \end{array} } \right]\left[ {\begin{array}{*{20}c} 
            x_{1} \\
            x_{2}
        \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
            w_{11}x_{1}+w_{12}x_{2} \\
            w_{21}x_{1}+w_{22}x_{2}
        \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
            \hat{y}_{1} \\
            \hat{y}_{2}
        \end{array} } \right]$
        \item $\frac{\partial \hat{y}_1}{w_{11}}=\frac{\partial (w_{11}x_1+w_{12}x_2)}{\partial w_{11}}=x_1$.
        \item $\frac{\partial \hat{y}_2}{w_{11}}=\frac{\partial (w_{21}x_1+w_{22}x_2)}{\partial w_{11}}=0$.
    \end{itemize}

        Then we can take the results back to (1) and we will have 
        
        $$
        \frac{\partial L}{\partial w_{11}}=\frac{\partial L}{\partial \hat{y}_1}\frac{\partial \hat{y}_1}{\partial w_{11}}+\frac{\partial L}{\partial \hat{y}_2}\frac{\partial \hat{y}_2}{\partial w_{11}}=(\hat{y_1}-y_1)\times x_1+(\hat{y}_2-y_{2})\times 0=x_1(\hat{y_1}-y_1)
        $$
    \end{proof}

    \null
    \noindent
    \textit{\underline{Method 2 (By Using the Laws from Report)}}. In the report, we have induced two laws for computing the derivative of linear transformations, as $\frac{\partial L}{\partial w}=\frac{\partial L}{\partial \hat{y}}x^T$. (\textbf{see page 6, Law 2 in the report.}) The reason why we use Law 2 is that we want to compute the derivative of $L$ with respect to $w$, and it is defined as $y=wx$ (right multiplication).
    
    Hence, we could compute a $(2,2)$ matrix $\left[ {\begin{array}{*{20}c} 
        \frac{\partial L}{\partial w_{11}},\frac{\partial L}{\partial w_{12}} \\
        \frac{\partial L}{\partial w_{21}},\frac{\partial L}{\partial w_{22}}
    \end{array} } \right]$ directly and then we will know $\frac{\partial L}{\partial w_{11}}$. In our case, we have $\frac{\partial L}{\partial \hat{y}}=\left[ {\begin{array}{*{20}c} 
        \frac{\partial L}{\partial \hat{y}_{1}} \\
        \frac{\partial L}{\partial \hat{y}_{2}}
    \end{array} } \right]$, then $\frac{\partial L}{\partial \hat{y}}x^T=\left[ {\begin{array}{*{20}c} 
        \frac{\partial L}{\partial \hat{y}_{1}} \\
        \frac{\partial L}{\partial \hat{y}_{2}}
    \end{array} } \right][x_1,x_2]=\left[ {\begin{array}{*{20}c} 
        x_1\frac{\partial L}{\partial \hat{y}_{1}} & x_2\frac{\partial L}{\partial \hat{y}_{1}} \\
        x_1\frac{\partial L}{\partial \hat{y}_{2}} & x_2\frac{\partial L}{\partial \hat{y}_{2}}
    \end{array} } \right]$

    Hence, we have $\frac{\partial L}{\partial w_{11}}=x_1(\frac{\partial L}{\partial \hat{y}_1})$. In the first method, we have already computed $\frac{\partial L}{\partial \hat{y}_1}=\hat{y}_1-y_1$. Hence $\frac{\partial L}{\partial w_{11}}=x_1(\hat{y}_1-y_1)$.

    \null
    \noindent
    \textit{\underline{Method 3 (Numerical Verification)}}. If we use a small $\epsilon=0.01$, and we give some values to the input, weight and ground truth: $w_{11}=0.1, w_{12}=0.2, w_{21}=0.3, w_{22}=0.4$, $x_1=0.5, x_2=0.7$ and $y_1=0.01$ and $y_2=0.99$. Then we will have 
    \begin{itemize}
        \item $\hat{y}=wx=\left[ {\begin{array}{*{20}c} 
            w_{11} & w_{12} \\
            w_{21} & w_{22} 
        \end{array} } \right]\left[ {\begin{array}{*{20}c} 
            x_{1} \\
            x_{2}
        \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
            0.1 & 0.2 \\
            0.3 & 0.4 
        \end{array} } \right]\left[ {\begin{array}{*{20}c} 
            0.5 \\
            0.7
        \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
            0.19 \\
            0.43
        \end{array} } \right]$.
        \item $L=\frac{1}{2}[(0.01-0.19)^2-(0.99-0.43)^2]=0.173$.
    \end{itemize}
    Then we define $w_{left}=w_{11}-\epsilon=0.09$ and $w_{right}=w_{11}+\epsilon=0.11$. Then we have
    \begin{itemize}
        \item $\hat{y}_{left}=\left[ {\begin{array}{*{20}c} 
            w_{left} & w_{12} \\
            w_{21} & w_{22} 
        \end{array} } \right]\left[ {\begin{array}{*{20}c} 
            x_{1} \\
            x_{2}
        \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
            0.185 \\
            0.43
        \end{array} } \right]$.
        \item $L_{left}=\frac{1}{2}[(0.01-0.185)^2-(0.99-0.43)^2]=0.1721125$.
        \item $\hat{y}_{right}=\left[ {\begin{array}{*{20}c} 
            w_{right} & w_{12} \\
            w_{21} & w_{22} 
        \end{array} } \right]\left[ {\begin{array}{*{20}c} 
            x_{1} \\
            x_{2}
        \end{array} } \right]=\left[ {\begin{array}{*{20}c} 
            0.195 \\
            0.43
        \end{array} } \right]$.
        \item $L_{right}=\frac{1}{2}[(0.01-0.195)^2-(0.99-0.43)^2]=0.1739125$.
    \end{itemize}
    Hence, we can compute the $\frac{\partial L}{\partial w_{11}}$ numerically by $\frac{L_{right}-L_{left}}{2\epsilon}=\frac{0.1739125-0.1721125}{0.02}=0.09$.
    By using \textit{Method 1} and \textit{Method 2}, the result will be $x_1(\hat{y_1}-y_1)=0.5*(0.19-0.01)=0.5*0.18=0.09$, and it is consistent as we did numerically.
    \end{document}
