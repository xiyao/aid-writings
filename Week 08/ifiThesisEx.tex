\documentclass[abstracton,12pt]{scrreprt}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{times}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{neuralnetwork}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
\lstset{style=mystyle}

% --------- 

\titlehead{Department of Informatics, University of Zürich}
\subject{\vspace*{2cm}MSc Basic Module}
\title{TinyNet: The Deep Learning Framework Implemented in Pure Python for Intepretability Research.}
\author{
  Xiaozhe Yao\\[-5pt]
  \scriptsize Matrikelnummer: 19-759-570\\[-5pt]
  \scriptsize Email: \texttt{xiaozhe.yao@uzh.ch}
}
\date{\vspace*{2cm}July 11, 2020}
\publishers{
  % \small supervised by Prof.\ Dr.\ x.\ yyy and y.\ zzz \\[5cm]
  \begin{tikzpicture}[overlay]
    \node at (-3,-3) {\includegraphics[height=1.5cm]{IFIlogo}};
    \node at (7,-3) {\includegraphics[height=1.5cm]{dbtgBW}};
  \end{tikzpicture}
}

% --------- 

\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newenvironment{proof}
  {\noindent{\bf Proof:\rm}}{\hfill$\Box$\vspace{\medskipamount}}

\def\bbbr{{\rm I\!R}}
\def\bbbm{{\rm I\!M}}
\def\bbbn{{\rm I\!N}}
\def\bbbz{{\rm I\!Z}}

% --------- 

\begin{document}

\maketitle

% \chapter*{Acknowledgements}

% \tableofcontents

\chapter{Introduction}

Large convolutional neural networks (CNNs) have played an important role in the
development of machine learning. Even though the CNNs have performed very well
in many different tasks, such as image classification, we still do not fully
understand what they have actually learned during the training process. There
are some researches that focus on reveal the internal mechanism of CNNs such as
\cite{matthew2014visualizing}, \cite{selvaraju2017grad}, and most of them are
implemented in mature deep learning frameworks, e.g. TensorFlow, PyTorch, etc.
However, from our aspects, there are some drawbacks with these mature frameworks
when performing interpretability research and teaching:
\begin{itemize}
	\item In order to be efficient and compatible with every application, most of these frameworks are implemented in C, and
	      provide a Python interface. It will be tedious to read into the
	      source code if the users need to modify a function that are implemented in C.
	\item With automatic differentiation, users do not need to write the code for
	      back propagation. As a result, the users have little visibility into the
	      differentiation process and have to rely on some other mechanisms to look into
	      it (in PyTorch, it is Hooks) \cite{paszke2017automatic}.
\end{itemize}

To tackle those two challenges, we will present a tiny deep learning framework
that only supports the convolutional neural network and fully connected network. We will then use our framework to reproduce the paper
\cite{matthew2014visualizing}. From the scope of a basic module, we will cover
the following components:
\begin{itemize}
	\item TinyNet. We will need to implement the following components:
	      \begin{itemize}
	      	\item Layers.
	      	      \begin{itemize}
	      	      	\item (With parameters): Fully Connected
	      	      	      Layer(\checkmark\footnote{\checkmark means that this component has been implemented.}), Convolutional
	      	      	      Layer(\checkmark), and 
	      	      	      Convolutional Transpose Layer.
	      	      	\item (Activations): ReLu and Softmax functions.
	      	      	\item (Others): Pooling, Unpooling and Flatten Layers.
	      	      \end{itemize}
	      	\item Optimizers. Stochastic Gradient Descent Optimizer (\checkmark) and possibly some others.
	      	\item Loss Functions. Mean Square Error Loss(\checkmark) and Cross Entropy Loss.
	      	\item Learner. A batch-based learner(\checkmark).
	      	\item Utilities. Intermediate results visualizer, Logger(\checkmark), and
	      	      possibly others.
	      	\item Combining above components to perform image classification, and
	      	      reproduce \cite{matthew2014visualizing}.
	      \end{itemize}
	\item PyTorch as control group. As most layers have already been implemented
	      in PyTorch, we will investigate how to use the following:
	      \begin{itemize}
	      	\item Convolutional (Conv2D) and Conv2DTranspose (DeConv2D).
	      	\item Fully connected Layer (Linear).
	      	\item ReLu.
	      	\item MaxPooling2D and MaxunPooling2D.
	      	\item Combining above components to reproduce \cite{matthew2014visualizing}
	      \end{itemize}
\end{itemize}

In this report, we will thoroughly go over these components, compute its forward
pass and the back propagation, and then implement it with pure Python. It will
be organized in the following part:
\begin{itemize}
	\item Back Propagation and Stochastic Gradient Descent (SGD). In this chapter, we will introduce the motivation of
	      back propagation and stochastic gradient descent. These two concepts will be
	      the fundamentals of our implementation.
	\item Theoretical Induction. In this chapter, we will focus on calculating the
	      formulas in deep convolutional neural networks. 
	\item Design and Implementation. In this chapter, we will introduce how we
	      implement different layers in our system.
	\item Examples of using TinyNet. We will briefly introduce how to use the
	      components inside TinyNet to perform image classification in this section.
	\item Reproduction of \cite{matthew2014visualizing} with TinyNet and PyTorch.
\end{itemize}

\chapter{Back Propagation and SGD}

In linear regression, we try to find such a linear function $f$ such that $y-f(x)$ is
minimal. Similarly, in neural networks, we want to find a non-linear and complex
function $f$ that also makes $y-f(x)$ minimal. The function that we are looking
for is non-linear, non-convex and there is usually no formulas for it, thus we
usually use gradient descent to find the (approximate) local minimal. 

The gradient descent algorithm is based on a fact that the function $f$ decreases fastest along the direction of the negative gradient. Formally, at a point $a_n$, if we want to find the lowest value of $f(a)$ around the point $a$, we will have $a_{n+1}=a_n-\epsilon\nabla f(a)$. If $\epsilon$ is small enough, we will have $f(a_{n+1})\leq f(a_n)$. Considering this, if we want to find the local minimal of the function $f$, we can start at a random point $a_0$, and follows the negative direction of the gradient. We will then have a sequence $a_1, a_2,...,a_n$ that abides $a_{n+1}=a_n-\epsilon\nabla f(a)$, and with the sequence, we will have $f(a_n)\leq f(a_{n-1})\leq\cdots\leq f(a_0)$. By doing so, we could find an approximate value $a_n$ such that $f(a_n)$ is the local minimal.

In the process of gradient descent, we found that we need to compute the gradient of our function $f$ in every step. Back Propagation is an efficient algorithm for calculating the gradient in deep neural networks. The main motivation behind back propagation is that the deep neural networks can be regarded as a composition of functions, and therefore we can use the chain rule to calculate the derivatives. For example, assume we have the following neural networks:

\begin{center}
	\begin{figure}[h!]
		\centering
		\includegraphics[width=330pt]{images/fcn.pdf}
		\caption{Illustration of an example fully connected layer}
		\label{fig_1}
	\end{figure}
\end{center}

Then we know that $a_3=w_{21}a_{21}+w_{22}a_{22}$, $a_{21}=w_{11}a_{11}+w_{12}a_{12}$, etc.  As $a_3$ is the final output, we can compute the difference between our output and ground truth $loss = (a_3-y)$. Then since we want to know how $w_{21}$ and $w_{22}$ should change, we can then compute $\frac{\partial{l}}{\partial{w_{21}}}$ and $\frac{\partial{l}}{\partial{w_{22}}}$. With the computed gradient, we can then compute $w_{21}^{new}=w_{21}-\lambda \frac{\partial{l}}{\partial{w_{21}}}$ where $\lambda$ is a preset hyperparameter. Then we do the same for $w_{22}$, $w_{11}$ and $w_{12}$. After that, we update all these four weights, and then compute the new loss. If the new loss is not satisfied, we can continuously repeat the process until we are satisified.

Formally, the process of back propagation and gradient descent can be stated as below:
\begin{itemize}
	\item First Forward Pass. At the beginning of the algorithm, we randomly set the weights of the neural network and perform the forward pass. In the forward pass, we simply compute the matrix multiplication of the weight $W$ and the input $x$.
	\item Compute Loss. Then we calculate the loss between the output and given ground truth.
	\item Backward Pass. During the backward pass, we compute the gradient layer by layer, and update their parameters. 
	\item Iterative Learning. After the first round, we then need to perform forward pass again, acquire a new loss, and perform backward pass again. If the loss is not utilized, we can repeat this process again and again.
\end{itemize}

\chapter{Theoretical Induction}

In the following induction, we will by default define the following symbols:

\begin{itemize}
	\item $w_i$ and $b_i$: the weight and bias of the $i$-th layer in a neural network.
	\item $x_i$: the input to the $i$-th layer in a neural network.
	\item $y_i$: the output of the $i$-th layer. On the contrary, if there is corresponding ground truth label, it is notated as $\hat{y}_i$.
	\item $l$: the value of loss function. It can be defined as $l=\frac{1}{n}\sum_{i=1}^{n}(y_i-\hat{y}_i)^2$ (Mean square error), $l=-\sum_{i}^n\hat{y_i}log(y_i)$ (cross entropy loss) or any other form.
\end{itemize}

There are two fundamental observations in back propagation:
\begin{itemize}
	\item In the $i$-th layer, we always know the gradient of the loss with respects to the output of $i$-th layer. That means, in $i$-th layer, $\frac{\partial l}{\partial y_i}$ is given.
	\item Since we know that the output of $(i-1)$-th layer is the input of $i$-th layer, when performing backward pass, we have $\frac{\partial{l}}{\partial{x_i}}=\frac{\partial{l}}{\partial{y_{i-1}}}$.
\end{itemize}

\section{Fully Connected Layers}

In forward pass, the output of fully connected layers is simple: $y_i=w_i \times x_i + b_i$. 

Then in order to know how $w$ changes will affect the loss, we need to calculate $\frac{\partial{l}}{\partial{w_i}}$. By using the chain rule, we have $\frac{\partial{l}}{\partial{w_i}}=\frac{\partial{l}}{y_i}\frac{\partial{y_i}}{\partial{w_i}}=\frac{\partial{l}}{y_i}x_i$, and $\frac{\partial{l}}{\partial{b_i}}=\frac{\partial{l}}{y_i}\frac{\partial{y_i}}{\partial{b_i}}=\frac{\partial{l}}{y_i}$. We can then successfully update our weight and bias in this layer.

After updating the weight and bias in $i$-th layer, we also need to pass the gradient of loss with respect to the input to the previous layer. So we need to compute the gradient that the $i$-th layer passed to previous layer by $\frac{\partial{l}}{\partial{x_i}}=\frac{\partial{l}}{\partial{y_i}}\frac{\partial{y_i}}{\partial{x_i}}=\frac{\partial{l}}{\partial{y_i}}w_i$.

\section{ReLu}

The purpose of using activation functions is to bring some non-linearity into the deep neural networks, so that the networks can fit the real world. One of the most popular activation function is the \textbf{re}ctifier \textbf{l}inear \textbf{u}nit (ReLu). 

The function is defined as $f(x)=max(0,x)$. Thus the forward pass is simple: $y_i=max(0, x_i)$. 

In the ReLu function, we do not have any weight or bias to update. Hence we only need to compute the gradient to previous layer. We have $\frac{\partial{l}}{\partial{x_i}}=\frac{\partial{l}}{\partial{y_i}}\frac{\partial{y_i}}{\partial{x_i}}$. Then we have:

\begin{equation}
	\frac{\partial{l}}{\partial{x_i}}=
	\begin{cases}
		$0$                               & \text{$x_i<0$} \\
		\frac{\partial{l}}{\partial{y_i}} & \text{$x_i>0$} \\
		undefined                         & \text{$x_i=0$} 
	\end{cases}
\end{equation}

We see that the derivative is not defined at the point $x_i=0$, but when computing, we can set it to be $0$, or $1$, or any other values between.

\section{Softmax}

Softmax is such a function that takes the output of the fully connected layers, and turn them into the probability. Formally, it takes an $n$-d vector, and normalizes it to $n$ probabilities proportional to the exponentials of the input number. It is defined as $$f(x)=\frac{e^{x_i}}{\sum e^{x_j}}$$, where $x_i$ is the $i$-th input number.

We can then compute the derivative by using the quotient rule (if $f(x)=\frac{g(x)}{h(x)}$, then $f'(x)=\frac{g'(x)h(x)-g(x)h(x)}{h^2(x)}$). In our case, we have $g_i=e^{x_i}$ and $h_i=\sum e^{x_j}$. Then we have $\frac{\partial g_i}{x_j}=e^{x_i} \: (i=j)$ or $0 \: (i\neq j)$. For $h_i$, no matter the relation between $i$ and $j$, the derivative will always be $e^{x_i}$.

Thus we have:

When $i=j$, $$\frac{\partial f}{\partial x_i}=\frac{e^{x_i}\sum e^{x_j}-e^{x_i}e^{x_j}}{(\sum e^{x_j})^2}=\frac{e^{x_i}}{\sum{e^{x_j}}}\times \frac{(\sum e^{x_i} - e^{x_i})}{\sum{e^{x_j}}} = f(x_i)(1-f(x_i))$$

When $i\neq j$, $$\frac{\partial f}{\partial x_i}=\frac{0-e^{x_i}e^{x_j}}{(\sum e^{x_j})^2}=-\frac{e^{x_i}}{\sum e^{x_j}}\times \frac{e^{x_j}}{\sum e^{x_j}}=-f(x_i)f(x_j)$$

\section{Mean Square Loss}

The mean square error is defined as $l = \frac{1}{n}\sum (y_i-\hat{y}^i)^2$. Since this is the last derivative we need to compute, we will only need to compute $\frac{\partial l}{\partial y_i}$. Let $g(y_i)=y_i-\hat{y_i}$, then $\frac{\partial g}{\partial y_i}=1$.

$$\frac{\partial l}{\partial y_i}=\frac{\partial l}{\partial g}\times \frac{\partial g}{{\partial y_i}}=\frac{2}{n}(y_i-\hat{y_i})$$

\section{Cross Entropy Loss}

The cross-entropy loss is defined as $l=-\sum_i^n \hat{y_i}log(p(y_i))$ where $p(y_i)$ is the probability of the output number, i.e. we usually use cross-entropy loss after a softmax layer. By this nature, we could actually compute the derivative of cross-entropy loss with respect to the original output $y_i$ rather than $p(y_i)$.

Then we have: 

$$\frac{\partial l}{\partial y_i}=- \sum_j \hat{y_j} \frac{\partial log(p(y_j))}{\partial y_i} = -\sum_j \hat{y_j} \frac{1}{p(y_j)}\frac{\partial p(y_j)}{\partial y_i}$$

Then as we know there will be a $k=i$ such that $\frac{p(y_k)}{\partial y_i}=p(y_j)(1-p(y_j))$, and for other $k\neq i$,  we have $\frac{p(y_k)}{\partial y_i}=-p(y_j)p(y_i)$.

Then we have:
\begin{equation}
	\begin{array}{l}
		$$-\sum_j \hat{y_j} \frac{1}{p(y_j)}\frac{\partial p(y_j)}{\partial y_i} \\ 
		= (-y_i)(1-p(y_i))-\sum_{j\neq i} \hat{y_j} \frac{1}{p(y_j)}p(y_j)p(y_i) \\
		= -y_i + p(y_i)y_i + \sum_{j\neq i}y_jp(y_i)                             \\ 
		= -y_i + p(y_i)\sum_{j\neq i} y_j                                        \\ 
		= -y_i + p(y_i)\sum_{j}p(y_j)                                            \\
		= p(y_i) - y_i $$                                                        
	\end{array}
\end{equation}

The form is very elegant, and easy to compute. Therefore we usually hide the computational process of the derivative of softmax in the computation of the cross- entropy loss.

\section{Dropout Layer}

In deep neural networks, we may encounter over fitting when our network is
complex and with many parameters. In \cite{JMLR:v15:srivastava14a}, N.Srivastava
et al proposed a simple technique named \textit{Dropout} that could prevent
overfitting. It refers to dropping out some neurons in a neural network
randomly. The mechanism is equivalent to training different neural networks with
different architecture in every batch.

The parameters for this layer is a preset probability $p_0$. It indicates the
probability of dropping a neuron. For example, if $p_0=0.5$, then it means that
every neuron in this layer has a $0.5$ chance of being dropped. With the given
probability, we can define the dropout layer to be a function $y_i=f(x_i)$ such that

\begin{equation}
	y_i=
	\begin{cases}
		$0$ & \text{$r_i<p$}     \\
		x_i & \text{$r_i\geq p$} \\
	\end{cases}
\end{equation} , where $r_i$ is randomly generated. However, if we use this function, the expectations of the output of dropout layer will be scaled to $p_0$. For example, if the original output is $1$ and $p_0=0.5$, the output will become $0.5$. This is unsatifactory because when we are testing the neural networks, we do not want the output to be scaled. Thus, in practice we define the function to be 

\begin{equation}
	y_i=
	\begin{cases}
		0     & \text{$r_i<p$}     \\
		x_i/p & \text{$r_i\geq p$} \\
	\end{cases}
\end{equation}

Then the backward computation becomes straightforward: 

\begin{equation}
	\frac{\partial l}{\partial x_i}=
	\begin{cases}
		0 \times \frac{\partial l}{\partial y_i}=0                                                                         & \text{$r_i<p$}     \\
		\frac{\partial l }{\partial y_i}\times\frac{\partial y_i}{\partial x_i}=\frac{1}{p}\frac{\partial l}{\partial y_i} & \text{$r_i\geq p$} \\
	\end{cases}
\end{equation}

\section{Convolutional Layer}

Compared with fully connected layers, convolutional layers is more difficult to
compute. In convolutional layers, we have a spatially small window sliding on
the image. For example, assume we have two matrix $A_{3\times 3}$, $B_{2\times
2}$, $B$ is the sliding window, and 

$$A=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 3 \\
		4 & 5 & 6   \\
		7 & 8 & 9   
		\end{array} } \right], B=\left[ {\begin{array}{*{20}c}
		1 & 2    \\
		3 & 4   
	\end{array} } \right]$$

We use $a_{ij}$ and $b_{ij}$ to denote the elements in $A$ and $B$, then the output after convolutional layer would be 

$$C=\left[ {\begin{array}{*{20}c}
		\sum_{i,j=1}^2 a_{ij}b_{ij} & \sum_{i=1}^{2}\sum_{j=2}^{3} a_{ij}b_{ij-1}   \\
		\sum_{i=2}^{3}\sum_{j=1}^{2} a_{ij}b_{i-1j} & \sum_{i,j=2}^3 a_{ij}b_{i-1j-1}    \\
		\end{array} } \right]=\left[ {\begin{array}{*{20}c}
		37 & 47    \\
		67 & 77   
	\end{array} } \right]$$

Formally, we can write the forward pass of the convolutional layer as 
$$
f(x_{ij}) = o_{ij} = \sum_{p=1}^{m}\sum_{q=1}^{n}w_{ij} \times x_{i+p-1, j+q-1} + b
$$

Even though we can compute $\frac{\partial f(x_{ij})}{\partial x_{ij}}$ successfully with the formula, it can be expected to be complicated with at least two loops. Computing for the forward and backward passes with loops is inefficient, thus we need a way to vectorize the computation process and use matrix multiplication to compute the general matrix problem. Such technique is called GEMM (\textbf{GE}neral \textbf{M}atrix to \textbf{M}atrix Multiplication). To do so, we need to formalize the convolution operation in more detail. 

The input to a convolution operation is a $w\times h\times c$ matrix where $w$ is the width of the image, $h$ is the height of the image, and $c$ is the channel of the image. For example, a colored RGB image of size $(224,224)$ can be represented as a $224\times 224\times 3$ tensor. At the same time, the sliding window has three properties, the height, the width of the filter and the number of output feature maps. These properties are denoted by $h_f$, $w_f$ and $K$. Then since we are sliding the window on the image, we have a vertical stride $s_v$ and a horizontal stride $s_h$. In the edge region, there might be no data in a window, and in that case, we need to add padding to the original input. In summary, we have the following notations as shown in table \ref{table_1}

\begin{center}
	\captionof{table}{Table of hyperparameter in convolutional layer.} 
	\begin{tabular}{||c c c c||}
		\hline
		Parameter & Meaning                         &   &   \\ [0.5ex] 
		\hline\hline
		$N$       & Number of data in a batch       &   &   \\ 
		\hline
		$W$       & Width of the 2D data            &   &   \\
		\hline
		$H$       & Height of the 2D data           &   &   \\
		\hline
		$C$       & Channel of the 2D data          &   &   \\
		\hline
		$H_f$     & Height of the filters           &   &   \\
		\hline
		$W_f$     & Width of the filters            &   &   \\
		\hline
		$K$       & Number of output feature maps   &   &   \\
		\hline
		$S_v$     & Vertical stride of the window   &   &   \\
		\hline
		$S_h$     & Horizontal stride of the window &   &   \\
		\hline
		$Pad_v$   & Vertical padding                &   &   \\
		\hline
		$Pad_h$   & Horizontal padding              &   &   \\
		\hline
	\end{tabular}
	\label{table_1}
\end{center}

With these notations, we can formalize the input, sliding window and the output as below:
\begin{itemize}
	\item The input is a $(N, C, H, W)$ tensor.
	\item The sliding window, or called filter is a $(K, C, H_f, W_f)$ tensor.
	\item The output is a $(N, K, P, Q)$ tensor, where $P, Q$ is the width and height of the output.
\end{itemize}

The technique that improve the efficiency is called \textit{im2col}. Aas suggested in \cite{chetlur2014cudnn}, we can convert the input $(N,C,H,W)$ tensor to a matrix with the size $(CRS, NPQ)$ and reshape the filter from a $(K, C, H_f, W_f)$ tensor to a $(K, CH_fW_f)$ matrix. To achieve this goal, we use a sliding window with the size $(H_f, W_f)$ and extract all the regions from the input, and then reshape them into columns. After that we combine all the columns together and will get a $(CRS, NPQ)$ matrix. For the filters, it is straightforward as we only need to concatenate the last three dimensions. For example, with the matrix $A$ and $B$, we can to convert them as

$$A^*=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 4 & 5   \\
		2 & 3 & 5 & 6   \\
		4 & 5 & 7 & 8   \\
		5 & 6 & 8 & 9
		\end{array} } \right], B^*=\left[ {\begin{array}{*{20}c}
		1 & 2 & 3 & 4    \\
		   
	\end{array} } \right]$$

Then we have $C^*=A^*B^*=[37, 47, 67, 77]$. If we reshape them back to $2\times 2$, we will have $C^*_{2\times 2}=C$.

\begin{center}
	\begin{figure}[h!]
		\centering
		\includegraphics[width=330pt]{images/im2col.pdf}
		\caption{Illustration of convering input data and filter into matrices}
		\label{fig_2}
	\end{figure}
\end{center}

In Fig. \ref{fig_2}, we have a colored image and the input can be represented as a $(1,3,3,3)$ tensor $A$, and filters set with $2$ output features map and these filters can be represented as a $(2,3,2,2)$ tensor $B$. We can then convert them into matrices $A^*$ and $B^*$ as shown in the image, and we will then have $C^*=A^*B^*$. We can then reshape $C^*$ to get the output of convolutional layer. 

The benefit of using the \textit{im2col} technique is that we can convert the complex convolutional operation into matrix multiplication. We can use $x^*$ to denote the input data in column format and $w^*$, $b^*$ to denote the parameters in the convolutional layer. Then similar to fully connected layer, we will have $f(x_{ij}^*)=w^*x_{ij}^*+b^*$, and the derivative also becomes straightforward as we have: \begin{itemize}
\item $\frac{\partial l}{\partial w^*_{ij}}=\frac{\partial l}{\partial f}\frac{\partial f}{\partial w^*_{ij}}=\frac{\partial l}{\partial f}x^*_{ij}$.
\item $\frac{\partial l}{\partial b^*_{ij}}=\frac{\partial l}{\partial f}$
\item $\frac{\partial l}{\partial x^*_{ij}}=\frac{\partial l}{\partial f}\frac{\partial f}{\partial x^*_{ij}}=\frac{\partial l}{\partial f}w^*_{ij}$
\end{itemize}

After computing these derivatives, all we need to do is to convert it back to original tensors.

\section{Max Pooling Layer}

Pooling layer is another important component of convolutional neural networks. There are many ways of performing pooling in this layer, such as max pooling, average pooling, etc. In this part, we will only discuss max-pooling layer as it is used most commonly in convolutional neural networks.

In Max pooling layer, we also have a spatially small sliding window called the kernel. In the window, only the largest value will be remained and all other values will be dropped. For example, assume we have 

$$A=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 3 \\
		4 & 5 & 6   \\
		7 & 8 & 9   
	\end{array} } \right]$$ and a $2\times 2$ max-pooling kernel. Then the output $C$ will be 
$$C=\left[ {\begin{array}{*{20}c} 
		5 & 6 \\
		8 & 9 \\
	\end{array} } \right]$$

With the given kernel size $K_w$ and $K_h$, We can formalize the max-pooling process as 

\begin{equation}
	f(x_{ij})=
	\begin{cases}
		x_{ij} & \text{$x_{ij}\geq x_{mn}, \forall m\in [i-K_w, i+K_w], n\in [j-K_h,j+K_h]$} \\
		0      & \text{otherwise}                                                            \\
	\end{cases}
\end{equation}

Hence we can compute the derivative as below: 

\begin{equation}
	\frac{\partial l}{\partial x_{ij}}=\frac{\partial l}{\partial f}\frac{\partial f}{\partial x_{ij}}=
	\begin{cases}
		\frac{\partial l}{\partial f} & \text{$x_{ij}\geq x_{mn}, \forall m\in [i-K_w, i+K_w], n\in [j-K_h,j+K_h]$} \\
		0                             & \text{otherwise}                                                            \\
	\end{cases}
\end{equation}

\section{Convolutional Transpose Layer}

\section{Max Unpooling Layer}

\section{Flatten Layer}

The output of convolutional 

In Flatten layers, we simply reshape the input into a $1$-d vector such that our fully connected layers can successfully use the input. The forward and backward processes are all straightforward, and only reshape the input data.

\chapter{Design and Implementation}

In TinyNet, we made the following modules to perform the training:
\begin{itemize}
	\item Core. In this module, we implement a base class for all parameters that needs to be updated during training. A parameter includes two components: the tensor (to save the data) and the gradient (to save the gradient for updating).
	\item Layers. In this module, we will implement all the layers that we need, such as the fully connected layer, convolutional layer, softmax, ReLu, etc. All these layers are classes that extend from a base class, and need to implement \textit{forward} and \textit{backward} function.
	\item Loss Functions. In this module, we will implement the forward and backward of mean square error loss and cross-entropy loss.
	\item Net. Net is a class that composite several layers, and provide three functions: \textit{forward}, \textit{backward} and \textit{update}. The forward function will perform forward passing from the beginning of the given layers, and the backward function will first reverse the layers, and perform backward from the end of the given layers. The update function simply updates all the parameters in the net, i.e. all layers updated at once.
	\item Optimizer. The optimizer simply updates the parameter with the given formula $w^{new}_i=w^{old}_i-\lambda \nabla_i$. It should be possible to add more optimizers.
\end{itemize}

As an example, the fully connected layer is implemented as below:

\lstinputlisting[caption={Implementation of Linear Layer in TinyNet} ]{implementations/Linear.py}

In order to maintain this report clean, we have made all the code accessible online at GitHub\footnote{https://github.com/xzyaoi/tinynet} rather than copying them to the report.

\section{Tricks in Implementation}



\chapter{Examples}

In this chapter, we will present three examples: 

\begin{itemize}
	\item A manually set neural networks. We will compute the training process by hand, and compare it with the result from TinyNet.
	\item The hand written digits recognition with both fully connected neural networks and convolutional neural networks in TinyNet.
	\item The cat classifier built with TinyNet.
\end{itemize}

\section{Computation Example of Training Process}

In this section, we will use a fully connected neural network shown as in Fig. \ref{fig_3}.

\begin{center}
	\begin{neuralnetwork}[height=4, toprow=false,title=Fig 5.1 Illustration of the simple neural network we will be using.]
		\label{fig_3}
		\newcommand{\x}[2]{$x_#2$}
		\newcommand{\y}[2]{$y_#2$}
		\newcommand{\hfirst}[2]{\small $h^{(1)}_#2$}
		\newcommand{\hsecond}[2]{\small $h^{(2)}_#2$}
		\inputlayer[count=2, bias=true, title=Input\\layer, text=\x]
		\hiddenlayer[count=2, bias=true, title=Hidden\\layer 1, text=\hfirst] \linklayers
		\outputlayer[count=2, title=Output\\layer 2, text=\y] \linklayers
	\end{neuralnetwork}  
\end{center}

We will use the following notation to help us demonstrate the process. In the following, by default we will have 
\begin{itemize}
  \item $x_i$ is the input of the neural network. There are two inputs in our examples which are $x_1=0.25, x_2=0.65$ and the bias $x_0=0.30$.
  \item $h_i^{(j)}$ are the data in $i$-th node of $j$-th layer. Then the weights that $j$-th layer use to multiply with its input is denoted by $w_{pq}^{j}$ where $q$ is the $q$-th node of $j$-th layer, and $p$ is the node in $(j-1)$-th layer. For example, the weight between $x_1$ and $h_1^{(1)}$ are denoted by $w^{1}_{11}$. In addition, we use $b_j$ to denote the bias in $j$-th layer. We assume that we have $w^{1}_{11}=0.20, w^{1}_{12}=0.25, w^{1}_{21}=0.30, w^{1}_{22}=0.35, b_{1}=0.40$ and $w^{2}_{11}=0.50, w^{2}_{12}=0.55, w^{2}_{21}=0.60, w^{2}_{22}=0.65$. In our case, there is only one bias in the hidden layer $h_0^{(1)}$ and we set it to be $h_0^{(1)}=0.20$
  \item $y_i$ is the $i$-th predicted output, and $y_i$ is the corresponding ground truth. In our case, we have $\hat{y_1}=0.99$ and $\hat{y_2}=0.01$.
  \item In this example, we will use mean square error, i.e. $l=\frac{1}{2}\sum_{i=1}^{2}(y_i-\hat{y_i})^2$. We will use stochastic gradient descent (SGD) to optimize our weights, and in our optimizer, the learning rate $\lambda$ is set to be $0.1$. We will use ReLu activation layer after every layer.
\end{itemize}

With these parameters, we can perform the first forward pass: 

\noindent
$h_1^{(1)}=x_1 * w^{1}_{11} + x_2 * w^{1}_{21} + x_0=0.25*0.20+0.65*0.30+0.30=0.545$ and \\ $h_2^{(2)}=x_1 * w^{1}_{12} + x_2 * w^{1}_{22} + x_0 = 0.25*0.25+0.65*0.35+0.30=0.59$. 

Then after the relu activation these values remained since they are all positive, then 

\noindent
$\hat{y_1}=h_1^{(1)} * w^{2}_{11} + h_2^{(1)} * w^{2}_{21} + h_0^{1}=0.545 * 0.50 + 0.59 * 0.60 + 0.20 = 0.8265$ \\
$\hat{y_2}=h_1^{(1)} * w^{2}_{12} + h_2^{(1)} * w^{2}_{22} + h_0^{1}=0.545 * 0.55 + 0.59 * 0.65 + 0.20 = 0.88325$

Then after the relu activation these values remained since they are all positive.

Then we can compute the loss with this iteration, $l=\frac{1}{2}\sum_{i=1}^{2}(y_i-\hat{y_i})^2=\frac{1}{2}(0.8265-0.99)^2+(0.88325-0.01)^2=0.39465$. Then the gradient will be 

\noindent
$\frac{\partial l}{\partial y_1}=(y_1-\hat{y_1}) = 0.8265-0.99=-0.1635$ and \\ $\frac{\partial l}{\partial y_2}=(y_2-\hat{y_2}) = 0.88325-0.01=0.87325$.

After this, we want to compute the derivative of the loss with regards to the weight and bias in the output layer. We have 

\noindent
$\frac{\partial l}{\partial w^{2}_{11}} = \frac{\partial l}{\partial y_1}\frac{\partial y_1}{\partial w^2_{11}}=-0.1635 * h_1^{(1)}=-0.1635 * 0.545 = -0.0891075$ \\
$\frac{\partial l}{\partial w^{2}_{12}} = \frac{\partial l}{\partial y_2}\frac{\partial y_2}{\partial w^2_{12}}=0.87325 * h_1^{(1)}=0.87325 * 0.545 = 0.47592125$ \\
$\frac{\partial l}{\partial w^{2}_{21}} = \frac{\partial l}{\partial y_1}\frac{\partial y_1}{\partial w^2_{21}}=-0.1635 * h_1^{(1)}=-0.1635 * 0.59 = -0.096465$ \\
$\frac{\partial l}{\partial w^{2}_{22}} = \frac{\partial l}{\partial y_2}\frac{\partial y_2}{\partial w^2_{22}}=0.87325 * h_1^{(1)}=0.87325 * 0.59 = 0.5152175$ 

Then we found that the bias $h_0$ will impact the loss in two ways: by impacting $y_1$ and $y_2$. Thus we can first convert the bias into a vector, and optimize the values in the vector accordingly. For example, in our case, we have $h_0^{(1)}=[h_{01}^{1}, h_{02}^{1}]=[0.20, 0.20]$, and the gradient will be \\
$\frac{\partial l}{\partial h_{01}^{1}}=\frac{\partial l}{\partial y_1}\frac{\partial y_1}{\partial h_{01}^{1}}=\frac{\partial l}{\partial y_1}=-0.1635$ \\
$\frac{\partial l}{\partial h_{02}^{1}}=\frac{\partial l}{\partial y_2}\frac{\partial y_2}{\partial h_{02}^{1}}=\frac{\partial l}{\partial y_2}=0.87325$

Then the gradient that we will pass to the previous layer, i.e. $\frac{\partial l}{\partial h^{(1)}_{ij}}$ can be computed as below:

$\frac{\partial l}{\partial h^{(1)}_{1}}=\frac{\partial l}{\partial y_1}\frac{\partial y_1}{\partial h^{(1)}_{1}} + \frac{\partial l}{\partial y_2}\frac{\partial y_2}{\partial h^{(1)}_{1}}=-0.1635 * w^2_{11} + 0.87325 * w^2_{12}=-0.1635 * 0.50 + 0.87325 * 0.55 =0.3985375$

$\frac{\partial l}{\partial h^{(1)}_{2}}=\frac{\partial l}{\partial y_1}\frac{\partial y_1}{\partial h^{(1)}_{2}} + \frac{\partial l}{\partial y_2}\frac{\partial y_2}{\partial h^{(1)}_{2}}=-0.1635 * w^2_{21} + 0.87325 * w^2_{22}=-0.1635 * 0.60 + 0.87325 * 0.65 =0.4695125$

With these derivatives, we can update the weight and bias in the output layer, here we will use the learning rate $\lambda = 0.1$. Then 

\noindent
$w^{2}_{11}=w^{2}_{11} - \lambda \frac{\partial l}{\partial w^{2}_{11}}=0.50-0.1*(-0.0891075)=0.50891075$ \\
$w^{2}_{12}=w^{2}_{12} - \lambda \frac{\partial l}{\partial w^{2}_{12}}=0.55-0.1*(0.47592125)=0.502407875$ \\
$w^{2}_{21}=w^{2}_{21} - \lambda \frac{\partial l}{\partial w^{2}_{21}}=0.60-0.1*(-0.096465)=0.6096465$ \\
$w^{2}_{22}=w^{2}_{22} - \lambda \frac{\partial l}{\partial w^{2}_{22}}=0.65-0.1*(0.5152175)=0.59847825$ and \\
$h^{2}_{01}=h^{2}_{01} - \lambda \frac{\partial l}{\partial h^{2}_{01}}=0.20-0.1*(-0.1635)=0.21635$ \\
$h^{2}_{02}=h^{2}_{02} - \lambda \frac{\partial l}{\partial h^{2}_{02}}=0.20-0.1*(0.87325)=0.112675$

Until now, we have successfully updated the value in the output layer, and afterwards, we will need to iteratively update the value in previous layers. Before working on the iterative process, in order to make the computation clean, we will use the matrix format. Here we already have the gradient that the output layer passed to hidden layer denoted by $\nabla_2 = [\frac{\partial l}{\partial h^{(1)}_{1}}, \frac{\partial l}{\partial h^{(1)}_{2}}]=[0.3985375, 0.4695125]$. Then 

$\frac{\partial l}{\partial w_{ij}^{1}}=\frac{\partial l}{\partial h^{1}_{j}}\frac{\partial h^{1}_{j}}{\partial w_{ij}^{1}}$. Thus  we will have 

$$\left[ {\begin{array}{*{20}c} 
	\frac{\partial l}{\partial w_{11}^{1}} & \frac{\partial l}{\partial w_{12}^{1}}  \\
	\frac{\partial l}{\partial w_{21}^{1}} & \frac{\partial l}{\partial w_{22}^{1}} 
	\end{array} } \right]=\left[ {\begin{array}{*{20}c} 
		0.25  \\
		0.65 
		\end{array} } \right]\left[ {\begin{array}{*{20}c} 
			0.3985375 & 0.4695125\\
			\end{array} } \right]=\left[ {\begin{array}{*{20}c} 
				0.09963437 & 0.11737812  \\
				0.25904938 & 0.30518312
				\end{array} } \right]$$

Then for the bias, we have $\frac{\partial l}{\partial x_0}=\frac{\partial l}{\partial h_j}$. Therefore $[\frac{\partial l}{\partial x_{01}},\frac{\partial l}{\partial x_{02}}]=[0.3985375, 0.4695125]$.

With these parameters, we can update the parameters in the hidden layer.

\noindent
$w^{1}_{11}=w^{1}_{11} - \lambda \frac{\partial l}{\partial w^{1}_{11}}=0.20-0.1*(0.09963437)=0.190036563$ \\
$w^{1}_{12}=w^{1}_{12} - \lambda \frac{\partial l}{\partial w^{1}_{12}}=0.25-0.1*(0.11737812)=0.238262188$ \\
$w^{1}_{21}=w^{1}_{21} - \lambda \frac{\partial l}{\partial w^{1}_{21}}=0.30-0.1*(0.25904938)=0.274095062$ \\
$w^{1}_{22}=w^{1}_{22} - \lambda \frac{\partial l}{\partial w^{1}_{22}}=0.35-0.1*(0.30518312)=0.319481688$ and \\
$x_{01}=x_{01} - \lambda \frac{\partial l}{\partial x_{01}}=0.20-0.1*(0.3985375)=0.16014625$ \\
$x_{02}=x_{02} - \lambda \frac{\partial l}{\partial x_{02}}=0.20-0.1*(0.4695125)=0.15304875$

Until now, we have successfully computed a forward pass, a backward pass and updated all the parameters in the neural network. With the same procedure, we can compute the new loss $l=0.2384542802724947$. If the new loss is still unsatifactory, we can repeat the process again and again to lower the loss until it is satifactory.

The process can be easily implemented in TinyNet with the following code snippets.

\lstinputlisting[caption={Implementation of Back Propagation in TinyNet} ]{implementations/example_bp.py}

\section{Hand Written Digits Recognition}

\section{Cat Classifier with VGG-16 Net}

\chapter{Reproduction}

\bibliographystyle{alpha}
\bibliography{refs.bib}

\end{document}