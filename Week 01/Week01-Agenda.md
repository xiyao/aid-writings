**Objective**

1. **Set and fix all administrative staffs.**
   1. Communication. Use Teams/Skype to keep everything transparent if it is already being used in DBTG.
   2. Basic Module Materials.
   3. Others
2. **Help understand how AID works.**
   1. Demonstrate the workflow of using AID.
   2. Go over the documents (Introduction)
3. **Set up plans and Schedule.**
   1. Set up a schedule for reading papers/basic module and feedbacks. (1 paper / week?)
   2. Method: Write Paper Feedback every week to abstract what I learned.
   3. Order: MLFlow -> TF-Serving -> TFJS -> TF Eager

**Agenda**

1. Go over administrative staff.
2. AID Demo:
   1. Build from source. (prerequisite: Docker, Go>1.12, CGO_ENABLED, go Mod)
   2. initialize configuration
   3. Command Line Operation
      1. Install New Repository (Sentiment or Detectron)
      2. Build Image (See notification)
      3. Start Container
      4. Test with cURL.
      5. Create new Package
   4. Web UI
      1. Inference with Web UI (Now Detectron Only)
      2. Webhooks etc.
3. Go over documents
   1. AID is a Skeleton, not a whole-solution. 
   2. Manage Entities and Events -> Give flexibility.
      1. Entities are rather stable: models, packages, datasets, solvers..
      2. Events are too flexible and changes rapidly.
4. Plans and Schedule:
   1. Paper Reading Session (lasts around 1 month at least)
   2. June Release Features: Cluster, Batch Processing, and Event Processing

**Feedback**

1. Read paper together, starting from the MLFlow paper.
2. Make the claim for solver, package more clear (presentation might be much better, not the progress but from the architecture view)
3. Insights part, there needs more work
4. Structure the meeting agenda.
