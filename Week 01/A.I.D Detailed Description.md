# AID Detailed Description

## Table of Contents

* Overview: Briefly introduce the problems that AID aims to tackle, and the features/deliverables that we currently planned.

* Implementations/Approaches: Introduce the approaches that we currently used in the implementation.

* Competitors: Introduce other similar projects and compare AID with them.

* Timetable: The features that we have already implemented, and a very rough plan for the following months.

* Appendix: I put all links that are related to the projects here for quick references.

## Overview

In the past few years, machine learning is one of the hottest computer science research areas, and there has been lots of frameworks and libraries that have greatly reduced the effort of designing, implementing and evaluating machine learning algorithms. However, there is still a huge gap between laboratories-designed models to online machine learning services. AID aims to provide a set of tools that could boost the process of evaluating, deploying and sharing machine learning algorithms.

### Problems that AID aims to tackle

AID has two main targeted audiences:

* Researchers. For machine learning researchers, the problem that they are facing is when they want to reproduce others' results, they have to set up environments, try to reimplement from the paper or download existing source code and evaluate on their dataset. It costs extra time and effort to go over the processes, and it would be more efficient if they could easily adopt other algorithms to evaluate. Besides, as several significant conferences, for example, NIPS 2019 required all authors to answer a checklist for reproducibility and ICML 2020 highly encourages authors to submit code. It would be helpful for reviewers to have a unified tool to check the reproducibilities of different submissions.

* Small companies. For small companies and startups, they can hardly afford to hire machine learning developers. It would help them reduce costs if they could reuse some well-established machine learning packages. Not only companies will benefit from the reusable packages, but researchers can also adopt their algorithms into applications and monetize their researchers.

Currently, AID is open-sourced at [https://github.com/autoai-org/aid](https://github.com/autoai-org/aid). The full documents can be found at [https://aid.autoai.org](https://aid.autoai.org). We also have some demo videos that show how it works on [YouTube](https://www.youtube.com/playlist?list=PL2BhlRIBzYXIkKBOTjU4nzYvxpXqemb2n).

### (Planned) Key Features

AID aims to provide the following features:

* Quickly turn machine learning algorithms into web services for both production and debugging purpose. The deployment targets include bare-metal machines to cloud services and clusters.

* Manage existing dataset, algorithms, pretrained models and other machine learning resources.

* Support other advancements in machine learning researches, such as model interpretability, compression, etc.

* A place where people could share their models with ease.

### Expected Deliverables

As a master project, the expected deliverables include:

* A command-line interface to manage packages, deployed services, etc.
  
  <img title="" src="/Volumes/QVO/Onedrive/OneDrive%20-%20email.szu.edu.cn/Writings/images/98f9ca7c234793f71f00f7baf8e33ca5482f3787.png" alt="cli.png" width="362" data-align="center">

* A web user interface to interact with.
  
  <img title="" src="/Volumes/QVO/Onedrive/OneDrive%20-%20email.szu.edu.cn/Writings/images/b0682e0cb92de45deb22f8de1f18cc9e21b6b425.PNG" alt="5.PNG" width="426" data-align="center">

* Several pre-built models and extensions as showcases. There are already several built models at the showroom: [https://aid.autoai.org/docs/examples/showroom](https://aid.autoai.org/docs/examples/showroom).

* If possible, there will be a "discovery" service, which works like a "store" and users can find well-established models/extensions there.

## Implementations and Approaches

### Key Concepts

We abstract the concepts in machine learning and DevOps into the following:

- ***Solver***. The solver is the minimal unit to solve a certain machine learning task. It is ideally a single Python class that extends from `aid.Solver`, and implement the `__init__` function (where to load the pretrained file, initialize the inference class, etc) and `infer` function (where actually do the prediction). In addition, it can optionally implement `train` function (This is still in discussion), which will allow users to upload a training dataset and get their fine-tuned model. With the class defined, AID will be able to automatically turn a solver into a web service. AID will also create a `batch` function, which reads a `.csv` file and call the `infer` function row by row.

- ***Package***. The package is a set of solvers. This is because several solvers can share something in common when performing machine learning tasks. For example, when performing face recognition, in some cases we will have a face embedding model. With the model, we can calculate the distances between two faces, but also the distance between the new face to the faces space. The latter can be used for face detection tasks. Conceptually, the package is such a set of solvers that have similar tasks, like face recognition and face detection, and in our implementation, they will share same environment variables, enabled extensions, dependencies, etc. The required file for a package can be seen as below:<img title="" src="/Volumes/QVO/Onedrive/OneDrive%20-%20email.szu.edu.cn/Writings/images/0bf58c41e81106ac7387f6bde1af0662767378ab.PNG" alt="package.PNG" width="384" data-align="center">

- ***Event***. Event is a protocol used to call external services. Every time a certain operation, such as onImageBuilt, onServiceUp, etc, is finished, an event is triggered. Then AID will iterate over all registered external services (named extensions, we will introduce this later) to check if they support this kind of service. If so, AID will trigger the service to run with the given data.

- ***Runtime***. Runtime is where the solver program actually runs. In the past, we let the solver run on bare-metal, without any isolation across different packages. That old approach led to several problems, especially the incompatibility of dependencies across two packages. Thus, in the latest version, we allow users to use container/docker as their runtime, which greatly reduced the effort in managing dependencies.

- ***Extension***. The extension is external services and is of two types: Webhooks (often used for notifications) and local programs. 

### Architecture

Currently, AID is implemented as a four-layer architecture, illustrated as below:

<img title="" src="/Volumes/QVO/Onedrive/OneDrive%20-%20email.szu.edu.cn/Writings/images/c2d302ec7df94520d8c978e4c95c058ed0da2f99.png" alt="AID-Codebase.png" width="674" data-align="center">

Overall, there are four layers. On top is the user interface where we provide a command-line interface, a web-based interface and a management/inference API. Except for the command-line, all other services are supported by the daemon program which will run as a system service after installation. Then the command line utility and the daemon can directly manipulate the database, runtime, extensions and all other modules. 

From bottom to top, they are:

* ***Utility***. In this layer, we implemented database operations, with which we save all our entities, including ```solver```, ```package```, ```extension``` and so on so forth (All entities can be found at [AID/components/cmd/pkg/entities at master · autoai-org/AID · GitHub](https://github.com/autoai-org/AID/tree/master/components/cmd/pkg/entities). We use a very tiny ORM library to map database actions (INSERT, DELETE, UPDATE and SELECT) into operations on our entities. Besides, we also have a file storage utility that will handle all file-related operations and a network request utility. 

* ***Runtime***. In the runtime layer, we mainly implement the ```containerd``` service that includes ```build image```, ```create container```, ```export image```, etc. The runtime layer in the past support running in bare metals without any isolation of the environment, <u>***and I am still considering if it is actually needed***</u>. Besides of this, in this layer we also handle extensions: when a certain event is triggered, the runtime will either run a shell command (for local programs) or make an HTTP call to external services (for webhooks).

* ***Daemon***. The daemon is a service that will be installed when requested. Just like what ```mysqld```, ```dockerd``` do, the daemon runs in the background (or for debugging purpose in the front end) and receives HTTP requests from Web interfaces and manipulate the runtime, database and all other resources as requested. For inference requests, it will run as a reverse proxy and forward the inference requests to the runtime (In multi-node mode, it will randomly pick a node to forward).

* ***User Interface***. The user interface is where users can interact with the system.

### Workflow

<img src="/Volumes/QVO/Onedrive/OneDrive%20-%20email.szu.edu.cn/Writings/images/7cf0bbba8d6d371f9d1c883698f28bdc317ab530.png" title="" alt="AID-Workflow.png" width="667">

The overall workflow is illustrated above. Overall there are four most important processes: *Install Package*, *Start Service*, *Receive Inference Requests* and *Process events*. *<u>It seems pretty clear on how AID handles each process. If needed I will add more explanation.</u>*

## Competitors

There are several different projects that aim at model sharing, deploying, and have a similar ambition with AID.

* Tensorflow Serving/TorchServe. Tensorflow Serving and TorchServe are proposed by the official TensorFlow/Torch developers and are designed for a single framework. Even though people can convert code in other frameworks into TensorFlow/torch code by using an intermediate representation, it still costs lots of work and can hardly achieve 100% transformation. Compared with them, AID is **framework-agnostic** and support all python libraries, from scikit to TensorFlow. Besides, both TensorFlow serving and TorchServe only support serving the models while AID supports use external extensions to compress, utilize models, which reduces the effort in finding effective external tools.

* [cortexlabs/cortex](https://github.com/cortexlabs/cortex). Cortex is an open-source platform for deploying machine learning models as production web services. Though it is open-sourced, it is dedicated to AWS and does not support other environments. Also, it does not support any kind of interactions with external services. Besides, it relies on Tensorflow Serving and ONNX runtime to convert and deploy models while AID uses a **vanilla python webserver** by default.

* [MLFlow](https://mlflow.org). MLFlow is an open-source platform for machine learning lifecycle management. Compared with AID and Cortex, it focuses more on training phase tracking and optimization rather than inference phase (need more investigation). MLFlow has extension support, but unlike AID, it mainly focuses on tracking scores, saving artifact etc.

Overall, AID more focuses on providing a more flexible infrastructure, where users can choose whatever kind of tech stacks they are willing to use, and deploy to whatever environments they like. A.AIDas the following features:

* It is framework-agnostic. Unlike TF-Serving, AID does not rely on internal implementations and can be used for a wider variety of machine learning frameworks.

* It uses a vanilla web server rather than advanced tools. Unlike Cortex, which forces users to use Kubernetes and AWS stack, AID allows users to choose their load balancer, cluster method, etc.

* Extension Support. AID has full-phase extension support with its event mechanism. By handling different types of events, third-party codes can inject into any phase of the machine learning lifecycle.

## Timetable and Plans

<u>**(It is still very rough)**</u>

I am considering using AID in production for the Big Data Analytics FS 20. Prior to June 2020, I plan to finish:

| No. | Title              | Description                                                                                                                                                                                 |
|:---:| ------------------ |:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1   | Event Processing   | Currently, it is still very basic and only supports webhooks. I still need to finish the processing of local programs.                                                                      |
| 2   | Cluster Management | I will implement a very basic master/slave cluster management. It will only support picking nodes randomly and all other advanced features will be implemented as Kubernetes/Swarm plugins. |
| 3   | Batch Processing   | I will implement a new endpoint ```/batch```, which receives a ```.csv``` file and returns the result after processing row-by-row.                                                          |

For more plans and timetable, I created an online table where I could update and use it as a project tracker. It is at [Airtable](https://airtable.com/invite/l?inviteId=inv8SqrYR0Bn1oPo8&inviteToken=333b3768419e1d21eb78e02c6928a7403c8f11ba7c1aee1db32aeaa9a63af6b0) and if this is not accessible, I have also attached an exported version along with this file.

## Appendix

* [Project Source Code](https://github.com/autoai-org/aid)

* [Documents](https://aid.autoai.org)

* [Demo Video](https://www.youtube.com/playlist?list=PL2BhlRIBzYXIkKBOTjU4nzYvxpXqemb2n)

* [Project Planner](https://airtable.com/invite/l?inviteId=inv8SqrYR0Bn1oPo8&inviteToken=333b3768419e1d21eb78e02c6928a7403c8f11ba7c1aee1db32aeaa9a63af6b0)
