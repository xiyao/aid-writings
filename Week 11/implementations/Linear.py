class Linear(Layer):
    def __init__(self, name, input_dim, output_dim):
        super().__init__(name)
        weight = np.random.randn(
            input_dim, output_dim) * np.sqrt(1/input_dim)
        bias = np.random.randn(output_dim) * np.sqrt(1/input_dim)
        self.type = 'Linear'
        self.weight = self.build_param(weight)
        self.bias = self.build_param(bias)
    
    def forward(self, input):
        self.input = input
        return np.matmul(input, self.weight.tensor) + self.bias.tensor
    
    def backward(self, in_gradient):
        self.weight.gradient = np.matmul(self.input.T, in_gradient)
        self.bias.gradient = np.sum(in_gradient, axis=0)
        return np.matmul(in_gradient, self.weight.tensor)
    