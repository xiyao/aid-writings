class TestLinearLayer(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        # the same with the forward test case
    def test_backward(self):
        self.test_forward()
        output_grad = self.tnn_linear.backward(self.gradient)
        self.torch_output.backward(torch.from_numpy(self.gradient))
        self.assertTrue(
            np.absolute((self.torch_input.grad - output_grad) < EPSILON).all())
        self.assertTrue(
            np.absolute((self.torch_linear.weight.grad.numpy() -
                         self.tnn_linear.weight.gradient) < EPSILON).all())
        self.assertTrue(
            np.absolute((self.torch_linear.bias.grad.numpy() -
                         self.tnn_linear.bias.gradient) < EPSILON).all())
