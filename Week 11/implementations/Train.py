learner = Learner(model, cross_entropy_with_softmax_loss,
                  SGDOptimizer(lr=0.1, momentum=0.9))

learner.fit(x_train, y_train, epochs=5, batch_size=1024)

model.export('saved.tnn')