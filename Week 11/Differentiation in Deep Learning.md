# Differentiation in Deep Learning

The computation for gradients is important in machine learning, especially in the process of back propagation. There are four categories of methods for the computation:

* **Manually working out derivatives** and coding them. This is exactly what we did in our implementation.

* **Numerical differentiation** using finite difference approximations. This is usually done to help us verify that our manually computed derivatives are correct.

* **Symbolic Differentitaion** using expression manipulation in computer algebra systems. The softwares that use symbolic differentiation includes Mathematica, etc. 

* **Automatic Differentation** is the most popular method for computing gradients in deep learning frameworks, such as PyTorch, TensorFlow, etc. It converts the program into a sequence of primitive operations, which have specified, pre-defined routines for computing derivatives. Then it uses chain rule to compute the deriavatives of the program.

## Numerical Differentiation

With a given function $J$, and if we want to compute its deriavatives at the point $\theta$, we have several ways of doing it numerically. We assume that we have a very small $\epsilon$ for computation.

* From the right-hand: $\nabla=\frac{J(\theta+\epsilon)-J(\theta)}{\epsilon}$
* From the left-hand: $\nabla=\frac{J(\theta)-J(\theta-\epsilon)}{\epsilon}$
* From the two-sided: $\nabla=\frac{J(\theta+\epsilon)-J(\theta-\epsilon)}{2\epsilon}$

Usually the two-sided form of approximating is closer than the other forms. Therefore we usually use the two-sided form to check if we have computed the gradient correctly. For example, assume we have a function $J(x)=x^2$ and we want to compute its derivative at $x=3$.

By manually working out the derivative, we have that $J'(x)=2x$ and therefore $J'(3)=6$.

Then if we want to compute it numerically, assume that $\epsilon=1e^-3$, we will have:

* The right hand form: $\nabla=\frac{(3+1e-3)^2-3^2}{1e-3}=6.000999999999479$.

* The two-sided form: $\nabla=\frac{(3+1e-3)^2-(3-1e-3)}{2\times 1e-3}=5.999999999999339$
