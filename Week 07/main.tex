\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2019

% ready for submission
% \usepackage{neurips_2019}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%     \usepackage[preprint]{neurips_2019}

% to compile a camera-ready version, add the [final] option, e.g.:
\usepackage[preprint]{neurips_2019}
% to avoid loading the natbib package, add option nonatbib:
%     \usepackage[nonatbib]{neurips_2019}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{caption}
\usepackage{multirow}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\title{Week 07: Quantitative Analysis of Transfer Learning and Interpretation;\\ Formalization of a Proposed Problem}

\author{%
  Xiaozhe Yao
  \texttt{xiaozhe.yao@uzh.ch} \\
}

\begin{document}

\maketitle

\begin{abstract}
	In this article, we will quantitatively analyse the impact on the accuracy of Transfer Learning. On top of the quantitative analysis, we will also try to explain and interpret the reason why transfer learning works. In the end of this article, we will introduce and formalize the problem that we need to solve. This article will be organized as below:
	\begin{itemize}
		\item \textbf{Definition and Terms.} As usual, we will list all new terms and define them in this section.
		\item \textbf{Quantitative Analysis of Transfer Learning} In this section, we will first introduce an experiment to demonstrate the effect of transfer learning, and then compare several different cases in model training. 
		\item \textbf{Interpretation and Analyze of the results in Section. 2} In this section, we will introduce some techniques in the interpretability research of deep learning. After that, we will try to use these techniques to explain the interpret the results that we found in Section 2.
		\item \textbf{Formalization of a Proposed Problem} In this section, we will discuss some problems occurred during the interpretation process. After that, we will try to find some approaches to solve them, and discuss how these approaches can be integrated with AID.
	\end{itemize}
\end{abstract}

\section{Definition and Terms}
\begin{itemize}
	\item \textbf{Gradient Descent}. It is an iterative optimization algorithm used in machine learning to find the best results, i.e. minima of a curve.
	\item \textbf{Epoch}. It is a term in model training. One epoch is when the entire dataset is passed and learned through the neural network. When actually training the model, we usually need a large number of epochs so that our neural network can find the local optimum.
\end{itemize}
\section{Quantitative Analysis of Transfer Learning}

In the last week's writing, we that we can preserve some weights and parameters from a pretrained model, and train it on our smaller dataset. The motivation behind transfer learning is because we are using the neural networks for two purposes: feature extractor and classifier. Then naturally, if the feature extractor is good enough, we can keep that part fixed, and finetuning the classifier. Though we are clear about the motivation and how it works, there are still some questions left:
\begin{itemize}
	\item Is the approach Quantitatively feasible? Is transfer learning significantly better than train it from scratch?
	\item There are four approaches (use the pretrained model directly; reuse the full convolutional layers; reuse part of the convolutional layers; retrain the whole model from scratch) mentioned in Fig. \ref{fig_1}, which one is better?
\end{itemize}

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{types_of_transfer_learning.pdf}
		\caption{Illustration of different strategies for transfer learning.}
		\label{fig_1}
	\end{figure}
\end{center}

In this section, we will introduce an experiment to demonstrate the advantages of transfer learning Quantitatively. In this experiment, we want to perform a simple binary classification, more specifically, we will have two classes: cats and dogs in our dataset. Then we set some layers to be fixed, and use transfer learning to train the other layers. We then evaluate the accuracies of each condition. To be more precise and specific, We formalize the experiment and quantitative analysis as below:

Assume we have a model $M_1$ trained on the dataset $\mathcal{D}_1$, and now we have a new task requiring us to classify images from a new given dataset $\mathcal{D}_2$. To mimic the real use cases of transfer learning, where we have significantly smaller dataset in new tasks, in $\mathcal{D}_2$, we can only use a small amount of data for training purpose. Then we want to figure out:

\begin{itemize}
	\item In case we can infer the category in $\mathcal{D}_2$ from the category in $\mathcal{D}_1$, can $M_1$ be used directly without any modification? For example, in $\mathcal{D}_1$, if the output is `Afghan hound', we know it is actually a `dog'. 
	\item If we train a totally new model $M_2$ from scratch without reusing any layer, how accurate it will be for the classification task on $\mathcal{D}_2$?
	\item If we reuse some layers in $M_1$ and train some other layers, will the new model $M_3$ outperform $M_2$?
	\item On top of the third question, if we reuse more or less layers, how will the accuracy changes?
\end{itemize}

In the following part, we will start introducing the dataset and pretrained model that we used in this experiment, and then our experiment settings. After that we will compare the accuracy of different models.

\subsection{Dataset and Pretrained Model}

In our experiment, we use the Imagenet \cite{deng2009imagenet} as $\mathcal{D}_1$ and VGG-net \cite{simonyan2014very} as our network architecture. For $\mathcal{D}_2$, we use the training set from a Kaggle competition \cite{kaggle_cat_and_dog}. Here we only use the training dataset because the competition does not provide ground truth for its testing dataset. Then we manually split the dataset into two pieces, one with $1000$ images ($500$ cats and $500$ dogs) for training and other $24000$ images for testing ($12000$ cats and $12000$ dogs).

In $\mathcal{D}_1$ (the Imagenet dataset), we have $1000$ categories and there are several different types of dogs and cats among the $1000$ categories. Thus we can use the output of $M_1$ to infer that if the output is either `cat' or `dog'. In order to answer our first question (can $M_1$ be used directly for classifying dogs and cats), we manually map all the types of dogs in Imagenet (e.g. `Afghan hound", `African hunting dog", etc) to `dogs', and all the types of cats to `cats'. If the output is not any type of dogs or cats, it will be mapped to `others'.

\subsection{Experiment Settings}

We then setup several different settings for the experiment:

\begin{itemize}
	\item $E_1$: We directly use the VGG-net that is pretrained on Imagenet dataset to classify the image. If the output is a specific type of dog or cat, it will be mapped to the corresponding category (dog or cat).
	\item $E_2$: In $E_2$, we only use the architecture in VGG-net and discard all the pretrained weight. We use our training dataset from $\mathcal{D}_2$ to train the VGG-net from scratch and evaluate its performance.
	\item $E_3$: In $E_3$, we use part of the pretrained VGG-net. More precisely, we will preserve the parameters in convolutional layers and retrain the classifiers, i.e. the fully connected layers using the training dataset from $\mathcal{D}_2$.
	\item $E_4$ and others. On top of $E_3$, We then try to preserve the parameters in less convolutional layers, and retrain the other convolutional layers and fully connected layers.
\end{itemize}

In $E2$, $E_3$, $E_4$ and others, we need to train the models with our own dataset. In real use cases, we only have limited time and budget for transfer learning, thus in our experiment, we set the training epochs of gradient descent to be $30$ so that we can finish the training is a few minutes.

There are also some other hyperparameters in the training process, but it might be tedious and off-topic to list all of them here. To be precisely, we have appended them as the appendix, along with the code to reproduce the experiment.

\subsection{Results and Analysis}

In this subsection, we will discuss two results:
\begin{itemize}
	\item Comparing $E_1$, $E_2$ and $E_3$, can we find out that transfer learning can outperform other approaches?
	\item Comparing $E_3$, $E_4$ and others, can we find how many layers should we reuse?
\end{itemize}

\subsubsection{Comparison between $E_1$, $E_2$ and $E_3$}

The results of $E_1$, $E_2$ and $E_3$ are illustrated as in Tabel \ref{table_1}:

\begin{center}
	\captionof{table}{Comparison between the performance of different classifiers.} 
	\begin{tabular}{ |c|c|c|c|c|c| } 
		\hline
		Experiment               & Category & Accuracy                  & Precision & Recall & F1-Score \\
		\hline
		\multirow{2}{3em}{$E_1$} & cat      & \multirow{2}{3em}{$77\%$} & 0.72      & 0.89   & 0.80     \\ 
		                         & dog      &                           & 0.86      & 0.66   & 0.74     \\
		\hline
		\multirow{2}{3em}{$E_2$} & cat      & \multirow{2}{3em}{$50\%$} & 0.50      & 0.64   & 0.56     \\ 
		                         & dog      &                           & 0.50      & 0.36   & 0.42     \\
		\hline
		\multirow{2}{3em}{$E_3$} & cat      & \multirow{2}{3em}{$92\%$} & 0.90      & 0.94   & 0.92     \\ 
		                         & dog      &                           & 0.94      & 0.90   & 0.92     \\
		\hline
	\end{tabular}
	\label{table_1}
\end{center}  

\textit{Note:} In $E_1$, the classifier will output three classes: cat, dog or others. As we are building a binary classifier, we first check if the output is the `dog', if it is not, our classifier will set the result to be the `cat'. That is, when we are counting the correct labels, we by default ignore the `others' label and our classifier will never give `others' as output.

From the results above, we found that the accuracy of $E_3$(\textit{92\%}) is significantly higher than $E_1$(\textit{77\%}) and $E_2$(\textit{50\%}). 

In $E_1$, the pretrained model can classify the images into $1000$ classes but is found to be less accurate on our simple binary classification task. Besides, using the pretrained model is only possible if the dataset used for training the model includes the categories for the new dataset in the new task. To conclude, compared between $E_1$ and $E_3$, we found that \textbf{transfer learning can significantly improve the accuracy compared with using the pretrained model directly, and overcome the limitations on the dataset.}

In $E_2$, we found that the accuracy is around $50\%$, which is the same with a random guess classifier. Thus, the classifier in $E_2$ is not the classifier we want. Compared with $E_3$, we found that our transfer learning classifier also outperforms training from scratch. But to figure the reasons out, we will need to look at the changes of loss and accuracy during the training phase. The changes of loss and accuracy are illustrated in \ref{fig_2} and \ref{fig_3}.

\begin{center}
	\begin{figure}[htbp]
		\centering
		\begin{minipage}[t]{0.48\textwidth}
			\centering
			\includegraphics[width=175pt]{batch_loss.pdf}
			\caption{Changes of loss in $E_2$.}
			\label{fig_2}
		\end{minipage}
		\begin{minipage}[t]{0.48\textwidth}
			\centering
			\includegraphics[width=175pt]{accuracy.pdf}
			\caption{Changes of accuracy in $E_2$.}
			\label{fig_3}
		\end{minipage}
	\end{figure}
	  
\end{center}

In these figures, we found that the loss is dropping rapidly while the accuracy is not increasing as we expected. This indicates that with the limited data, we are unable to train a good classifier, at least with our current settings (the epoch numbers, optimizer, learning rate, etc.). \textit{Note: Though with our current settings, the classifier does not work very well, we can still change our settings and if we are lucky, the accuracy will be higher. It will need more experiments to find out the best settings and corresponding accuracy.}

In summary, we setup three experiments for quantitative analysis, and found that the classifier that we trained with transfer learning on our smaller dataset, outperforms the other two approaches (using the model directly and training the model from scratch) significantly. 

\subsubsection{Comparison between $E_3$, $E_4$ and others}

We then setup the following experiment in others:

\begin{itemize}
	\item $E_4$: We remove the parameters of the \textbf{last layer} in convolutional layers and all parameters in the fully connected layers.
	\item $E_5$: We remove the parameters of the \textbf{last two layers} in convolutional layers and all parameters in the fully connected layers.
	\item $E_6$: We remove the parameters of the \textbf{last three layers} in convolutional layers and all parameters in the fully connected layers.
	\item $E_7$: We remove the parameters of the \textbf{last four layers} in convolutional layers and all parameters in the fully connected layers.
\end{itemize}

The results are as in Table. \ref{table_2}:

\begin{center}
	\captionof{table}{Comparison between the performance of different classifiers.} 
	\begin{tabular}{ |c|c|c|c|c|c| } 
		\hline
		Experiment               & Category & Accuracy                  & Precision & Recall & F1-Score \\
		\hline
		\multirow{2}{3em}{$E_3$} & cat      & \multirow{2}{3em}{$92\%$} & 0.90      & \textbf{0.94}   & 0.92     \\ 
		                         & dog      &                           & 0.94      & 0.90   & 0.92     \\
		\hline
		\multirow{2}{3em}{$E_4$} & cat      & \multirow{2}{3em}{$91\%$} & \textbf{0.93}      & 0.89   & 0.91     \\ 
		                         & dog      &                           & 0.90      & \textbf{0.93}   & 0.91     \\
		\hline
		\multirow{2}{3em}{$E_5$} & cat      & \multirow{2}{3em}{$91\%$} & 0.91      & 0.92   & 0.92     \\ 
		                         & dog      &                           & 0.92      & 0.91   & 0.92     \\
		\hline
		\multirow{2}{3em}{$E_6$} & cat      & \multirow{2}{3em}{$91\%$} & 0.89      & \textbf{0.95}   & 0.92     \\ 
		                         & dog      &                           & \textbf{0.94}      & 0.89   & 0.91     \\
		\hline
		\multirow{2}{3em}{$E_7$} & cat      & \multirow{2}{3em}{$50\%$} & 0.00      & 0.00   & 0.00     \\ 
		                         & dog      &                           & 0.50      & 1.00   & 0.67     \\
		\hline
	\end{tabular}
	\label{table_2}
\end{center}  

Here we find that we can remove the parameters and weights in several layers in convolutional layers as well as the whole fully connected layers and retrain them. However, in $E_7$ we found that if we remove too many layers, our neural network will become really bad at this new task. (\textit{Note: Here we found that in $E_7$, the outputs are always `dog'. It is absurd that our neural network is that bad at classifying and we are still working on investigating the reasons}).

Among $E_3$ to $E_6$, we found that the accuracies, precision and recall are close. Thus users may choose whichever they need as their classifier. There is no significant difference between these classifiers yet.

In summary, we set up five experiments to demonstrate the influence of the different numbers when removing parameters from layers. Though the performance are different, there is no significant difference when choosing the number of layers, as long as we are not removing too many layers. From $E_3$ to $E_6$ and $E_7$, we found that it seems the accuracy drops immediately if we remove one more layer than $E_6$. The reason why this is happening may need more investigation and explanation. 

\section{Interpretation of the Deep Learning}

In this section, we will first introduce some types of interpretation methods in deep learning. After that, we will try to use visualization to show why transfer learning works.

\subsection{Taxonomy of Interpretation methods}

This subsection is mainly from \cite{molnar2020interpretable}.

By the difference in outputs, we can classify the interpretation methods to be:
\begin{itemize}
  \item \textbf{Feature summary statistic}. Many interpretation methods provide summary statistics for each feature. Some methods return a single number per feature, such as feature importance, or a more complex result, such as the pairwise feature interaction strengths, which consist of a number for each feature pair.
  \item \textbf{Feature summary visualization}. Most of the feature summary statistics can also be visualized. Some feature summaries are actually only meaningful if they are visualized and a table would be a wrong choice. The partial dependence of a feature is such a case. Partial dependence plots are curves that show a feature and the average predicted outcome. The best way to present partial dependences is to actually draw the curve instead of printing the coordinates.
  \item \textbf{Data Point}. This category includes all methods that return data points (already existed or newly created) to make a model interpretable. One method is called counterfactual explanations. To explain the prediction of a data instance, the method finds a similar data point by changing some of the features for which the predicted outcome changes in a relevant way (e.g. a flip in the predicted class). Another example is the identification of prototypes of predicted classes. To be useful, interpretation methods that output new data points require that the data points themselves can be interpreted. This works well for images and texts, but is less useful for tabular data with hundreds of features.
  \item \textbf{Model Internals}. The interpretation of intrinsically interpretable models falls into this category. Examples are the weights in linear models or the learned tree structure (the features and thresholds used for the splits) of decision trees. The lines are blurred between model internals and feature summary statistic in, for example, linear models, because the weights are both model internals and summary statistics for the features at the same time. Another method that outputs model internals is the visualization of feature detectors learned in convolutional neural networks. Interpretability methods that output model internals are by definition model-specific.
  \item \textbf{Intrinsically interpretable model}. Another solution to interpreting black-box models is to approximate them (either globally or locally) with an interpretable model. The interpretable model itself is interpreted by looking at internal model parameters or feature summary statistics.
\end{itemize}

\subsection{Visualization of pretrained VGG-net}

Here, we will use the visualization techniques to draw the model internals. More precisely, we visualize the filters in every convolutional layer in the pretrained VGG-net. These filters are illustrated in Fig.\ref{fig_4}. Note that we only draw a single filter in every layer to reduce the total number of filters.

\begin{center}
  \begin{figure}[h!]
    \centering
		\includegraphics[width=330pt]{vgg-5-filters.pdf}
		\caption{Illustration of the fifth filter in every convolutional layer in a pretrained VGG-net.}
		\label{fig_4}
	\end{figure}
\end{center}

Even though the figure cannot directly help us understand how neural network works, we can still find some patterns from the trend of these filters. From our perspective, we found three important patterns:
\begin{itemize}
  \item \textbf{Filters are becoming more and more complex}. In layer $0$, the filter is basically a single colour. Then there are some lines in layer $2$. In layer $10$, the filter becomes some polygons. We can conclude that the filters are becoming more and more complex in deeper convolutional layers. This observation abides with our hypothesis that the convolutional neural network can extract some features, and deeper convolutional neural network can extract deeper, more complex and higher-level features.
  \item \textbf{Filters are becoming less repetitive}. In layer $0$, the whole image is repeating a single pixel. Then in layer $2$, only some part of the image is repeating the line. In the last several layers, the repeating parts are becoming less and less.
  \item \textbf{Filters look more and more like a real object}. In layer $0$, the whole image is a single color and does not look like any object in real life. However, in layer $7$, $24$, $26$ and $28$, the filters look like a component of animals.  
\end{itemize}

We then compared the filters in layer $28$ in different experiments. More precisely, we illustrate the fifth filter in layer $28$ in $E_6$ and $E_7$ in Fig. \ref{fig_5}.

\begin{center}
  \begin{figure}[h!]
    \centering
		\includegraphics[width=330pt]{e6-e7.pdf}
		\caption{Illustration of the fifth filter in layer $28$ in $E_6$ and $E_7$.}
		\label{fig_5}
	\end{figure}
\end{center}

We found that the boundaries of objects in $E_6$ are clearer than in $E_7$. This finding might be a clue to why our classifier in $E_7$ does not work very well.

In summary, in this section, we illustrate the filters in different layers in a pretrained VGG-net and try to extract some patterns from the visualization. Then we compared the visualization of the same layers in $E_6$ and $E_7$, and found that the more layers we keep, the more clear boundaries we will get. However, with this information, we are still unable to fully make the black box models explainable.  

\section{Proposed Problem}

With the above experiments and visualization, we then find a problem that needs exploration. When we are performing transfer learning, we can choose how many layers we want to preserve, and how many layers we want to retrain. As we have discussed, convolutional layers are trying to extract high-level features from an image, but what we want to transfer is the general features. Is it always the best case if we preserve all the convolutional layers and retrain the classifier? Or will it be better if we also retrain some high-level convolutional layers?

In the past, in order to find the best transfer learning model, we usually give all the possibilities a try and use the most accurate model on the test data set. Can we use the techniques in interpretability research to explain our decision? Moreover, if our finding are a general rule, can we find find a concrete measurement indicating how many layers should be preserved?

More precisely, we can formalize the problem as below:

Assume we have a model $M_1$ trained on $\mathcal{D}_1$ and we want to transfer it to $\mathcal{D}_2$, can we find a function $f$ such that $T := f(\mathcal{D}_1, M_1)$ and if $i<T$, we preserve the parameters in the $layer_i$. Otherwise ($i\geq T$), we retrain the parameters in the $layer_i$ on $\mathcal{D}_2$. Then we will have a new $M_2$ from parameters in $M_1$ and new parameters that we trained from $\mathcal{D}_2$. We want to find the $f$ such that the accuracy of $M_2$ is maximum.

On top of this, if we can find such an $f$, can we then change how the classifier works? If the features are good enough, can we use a simpler but faster classifier to replace the fully connected layers?

This problem is important, because if we can automatically find the best position and we can interpret and explain why we are doing so, we can then greatly reduce the time and effort in performing transfer learning. After that, if we can use simpler but faster classifiers, we can reduce the time for inference also. 

\section{Appendix}

We have made the experiment logs publicly accessible at \url{https://ui.neptune.ai/xzyao/transfer-experiment/}.

The code for training and evaluating can be found at \url{https://colab.research.google.com/drive/1SMTnosU63uaj6pxHGYMndX9HgPrTL89q?usp=sharing}.

\bibliographystyle{plainnat}
\bibliography{refs.bib}

\end{document}
