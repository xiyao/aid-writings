\documentclass[abstracton,12pt]{scrreprt}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{times}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{neuralnetwork}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
\lstset{style=mystyle}

% --------- 

\titlehead{Department of Informatics, University of Zürich}
\subject{\vspace*{2cm}MSc Basic Module}
\title{Implementing Deconvolution to Visualize and Understand Convolutional Neural Networks.}
\author{
  Xiaozhe Yao\\[-5pt]
  \scriptsize Matrikelnummer: 19-759-570\\[-5pt]
  \scriptsize Email: \texttt{xiaozhe.yao@uzh.ch}
}
\date{\vspace*{2cm}July 11, 2020}
\publishers{
  % \small supervised by Prof.\ Dr.\ x.\ yyy and y.\ zzz \\[5cm]
  \begin{tikzpicture}[overlay]
    \node at (-3,-3) {\includegraphics[height=1.5cm]{IFIlogo}};
    \node at (7,-3) {\includegraphics[height=1.5cm]{dbtgBW}};
  \end{tikzpicture}
}

% --------- 

\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newenvironment{proof}
  {\noindent{\bf Proof:\rm}}{\hfill$\Box$\vspace{\medskipamount}}

\def\bbbr{{\rm I\!R}}
\def\bbbm{{\rm I\!M}}
\def\bbbn{{\rm I\!N}}
\def\bbbz{{\rm I\!Z}}

% --------- 

\begin{document}

\maketitle

% \chapter*{Acknowledgements}

% \tableofcontents

\chapter{Introduction}

Deep convolutional neural networks (CNNs) have played an important role in the recent development in the field of pattern recognition, especially in the computer vision tasks, such as image classification, object detection, etc. However, as a black-box model, it is still unclear how these neural networks works. Hence, much researches are working towards the interpretability of deep convolutional neural networks, such as \cite{matthew2014visualizing}. They proposed an approach that uses the deconvolution operation to visualize the features in convolutional layers. In their approach, there are three important components: 1) the pretrained deep convolutional neural network, including convolutional layers, pooling layers, fully connected layers and ReLu layers. 2) the deconvolution operation and 3) the max-unpooling operation. In this work, we will present a novel implementation of these components. On top of the implementation, we will build a cat classifier with the deep convolutional neural network, and then try to explain the classifications on a given dataset. After that, we will compare our result with a PyTorch implementation. 

This article will be organized into the following sections:
\begin{itemize}
	\item Related Work. In this section, we will briefly introduce the techniques that we need. More specifically, we will introduce \textit{Deconvolution Operation}.
	\item Approach. In this section, we will thoroughly explain the mechanisms of essential components in our system: \textit{the fully connected layer}, \textit{convolutional layer}, \textit{deconvolution layer}, \textit{max pooling} and \textit{max unpooling}.
	\item Example. In this section, we will present a minimal examples of using our approach to perform image classification and interpret the classification results.
	\item Implementation. In this section, we will explain how we implement the training, evaluating and interpreting process. More specifically, we will present the implementation of back propagation, SGD and other utilities.
	\item Experiments. In this section, we will present the architecture and training settings we used in the classification model. With the classification model, we will present the classification results on a given dataset, and try to interpret it with a deconvolutional network. Besides these, we will also compare the results of using our naive implementation and a PyTorch implementation.
	\item Appendices. In this section, we will present the theoretical inductions and implementations of all other necessary but not that important layers. 
\end{itemize}

\chapter{Related Work}

\null
\noindent
\textbf{Deconvolution}: The two fundamental components of the CNN are convolutional layers and pooling layers, which works together to transform images into feature maps. Deconvolutional operation is basically a transformation that goes in the opposite direction of a normal convolution operation, i.e. from the feature maps that we extracted with convolution operation to something that has the shape of the input to it. After being introduced, deconvolution has been used in many fields such as pixel-wise segmentation, generative models, etc. In this work, we use deconvolution to map the intermediate feature maps back to the input pixel space. With this approach, we can show what input pattern caused a given activation in the feature maps. There are also two components in the approach, called deconvolutional layers and unpooling layers, and we will explain these two concepts in more detail in \textit{Section 3}.

\chapter{Approach}

\section{Classification Model}

In a CNN-based image classification model, there will be three important layers: the fully connected layer, the convolutional layer and the pooling layer. In this section, we will thoroughly explain how they are computed in both the forward pass and backward pass.

\subsection{Fully Connected Layer}

The fully connected layer is the most fundamental, and brute force neural network component in CNNs. It is such a layer that connects all the nodes in one layer to all the nodes in the next layer. We can intuitively visualize the fully connected layer as in Fig. \ref{fig_1}, in which yellow nodes represent the bias (the nodes that are independent from the output of the previous layer) in each layer, green nodes represent the input to the neural network, blue nodes represent the intermediate activation and red nodes represent the output results.

\begin{center}
	\begin{neuralnetwork}[height=4, toprow=false,title=Fig 3.1.1 Illustration of the fully connected layers.]
		\label{fig_1}
		\newcommand{\x}[2]{$x_#2$}
		\newcommand{\y}[2]{$y_#2$}
		\newcommand{\hfirst}[2]{\small $h^{(1)}_#2$}
		\newcommand{\hsecond}[2]{\small $h^{(2)}_#2$}
		\inputlayer[count=2, bias=true, title=Input\\layer, text=\x]
		\hiddenlayer[count=2, bias=true, title=Hidden\\layer 1, text=\hfirst] \linklayers
		\outputlayer[count=2, title=Output\\layer 2, text=\y] \linklayers
	\end{neuralnetwork}  
\end{center}

From the definition of fully connected layer, we know that in the $i$-th layer, if there are $n$ input nodes, and $m$ output nodes, there will be $m\times n$ relations between them. These relations can be represented as a $n\times m$ matrix $w$ (also called weight). Besides these relations, there will be $m$ biases that can be represented as a $m$-d vector $b$. 

With these notations, we can calculate the output as $\hat{y}=wx+b$ in the forward pass.

Then in the training process, we need to know how the change on $w$ and $b$ will affect the loss, and the gradient that we need to pass to the previous layer. Formally, we already know the gradient of the loss function with respect to the output of the $i$-th layer $\frac{\partial l}{\partial \hat{y}_i}$ as it is given in back propagation, and we need to compute $\frac{\partial l}{\partial w}$, $\frac{\partial l}{\partial b}$ and $\frac{\partial l}{x}$. By using the chain rule, we have $\nabla_w=\frac{\partial l}{\partial w}=\frac{\partial l}{\partial y}\frac{\partial y}{\partial w}=\frac{\partial l}{\partial y}x$, $\nabla_b=\frac{\partial l}{\partial b}=\frac{\partial l}{\partial y}\frac{\partial y}{\partial b}=\frac{\partial l}{\partial y}$ and $\frac{\partial l}{\partial x}=\frac{\partial l}{\partial y}\frac{\partial y}{\partial x}=\frac{\partial l}{\partial y}w$. With these results, we know that we can update the weight as $w^{new}=w-\epsilon \nabla_w$ and bias as $b^{new}=b-\epsilon \nabla_b$, where $\epsilon$ is a preset hyperparameter named learning rate. After updating the paramters, we can pass the gradient $\frac{\partial l}{\partial x}$ to the previous layer and complete the backward process.

\subsection{Convolutional Layer}

In many neural network architectures, convolutional layers are the most important components. Compared with fully connected layer, convolution layer is more efficient and specialized. In convolutional layers, we have a spatially small window (also called filter) sliding on the image, and calculating the dot product of the window and corresponding image. For example, assume we have two matrices $A_{3\times 3}$ as the matrix representation of the input image and the sliding window $B_{2\times 2}$, and we have 

$$A=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 3 \\
		4 & 5 & 6   \\
		7 & 8 & 9   
		\end{array} } \right], B=\left[ {\begin{array}{*{20}c}
		1 & 2    \\
		3 & 4   
	\end{array} } \right]$$

To compute the output of the convolution operation, we use $a_{ij}$ and $b_{ij}$ to denote the elements in $A$ and $B$, then the output after the convolution output $C$ will be 

$$C=\left[ {\begin{array}{*{20}c}
		\sum_{i,j=1}^2 a_{ij}b_{ij} & \sum_{i=1}^{2}\sum_{j=2}^{3} a_{ij}b_{ij-1}   \\
		\sum_{i=2}^{3}\sum_{j=1}^{2} a_{ij}b_{i-1j} & \sum_{i,j=2}^3 a_{ij}b_{i-1j-1}    \\
		\end{array} } \right]=\left[ {\begin{array}{*{20}c}
		37 & 47    \\
		67 & 77   
	\end{array} } \right]$$

Formally, we can write the forward pass of convolutional layer as 
$$f(x_{ij}) = o_{ij} = \sum_{p=1}^{m}\sum_{q=1}^{n}w_{ij} \times x_{i+p-1, j+q-1} + b$$

From the formula of the forward pass, we find that there are many loops required to compute the output of the convolutional layer, and it also applies for the backward pass. As it is inefficient to compute the forward and backward processes with loops, we need a method that transform the process into matrix multiplication. Such techniques are called \textbf{GE}neral \textbf{M}atrix to \textbf{Matrix} Multiplication (GEMM). In order to perform such a transformation, we need to formalize the data in the convolutional layer in more detail.

The input to a convolutional layer can be regarded as a $c\times h\times w$ tensor where $c$ is the channel of the image, $h$ is the height of the image and $w$ is the width of the image. For example, a coloured square image with the side length $w=h=224$ can be represented as a $(3,224,224)$ tensor. Similarly, the filter can also be represented as a $c_f\times h_f\times w_f$ tensor but with slightly different meanings. In filters, $c_f$ is the number of filters and should be set manually as a hyperparameter. $h_f$ and $w_f$ are used to denote the height and width of the filters. Then as we are sliding the filters on the image, we will have a vertical stride $s_v$ and a horizontal stride $s_h$. With these notations, we can formalize the input, the filters and the output of a convolutional layer as below:

\begin{itemize}
	\item The input is a $(n, c, h, w)$ tensor, where $n$ is the number of images.
	\item The filters are a $(c_f, c, h_f, h_w)$ tensor. 
	\item The output is a $(n, c_f, p, q)$ tensor, where $p$ and $q$ are the height and width of the output.
\end{itemize}

The technique that we will use to perform the transformation is called \textit{im2col}, proposed in \cite{chetlur2014cudnn}. The idea is that we can convert the input $(n, c, h, w)$ tensor into a matrix with the shape $(ch_fh_w, npq)$, and reshape the filter tensor from $(c, c_f, h_f, h_w)$ to a $(c_f, ch_fw_f)$ matrix. Then if we multiply these two matrix, we will have $B_{c_f\times ch_fw_f)}\times A_{ch_fh_w \times npq}=C_{c_f\times npq}$. Then we can reshape the matrix back to the desired output shape $(n, c_f, p, q)$. In order to convert the input $(n,c,h,w)$ tensor into a $(ch_fh_w, npq)$ matrix, we can use a sliding window of the shape $(h_f, h_w)$ and extract all the corresponding entries from the input image, reshape them to columns, and then we stack these columns together. By doing so we will have a $(ch_fh_w, npq)$ matrix. 

For example, assume we have the same matrix $A$ and $B$ as the beginning of this section, we can proceed as follow:

\begin{itemize}
	\item Compute the output shape. The input is a $3\times 3$ matrix and the filter is a $2\times 2$ matrix, with the stride to be $1$, the output matrix should be a $2\times 2$ matrix.
	\item Convert the input tensor from $(1,1,3,3)$ into a $(4,4)$ matrix. There are four regions that our sliding window will slide, which are $A_1=\left[ {\begin{array}{*{20}c} 
			1 & 2  \\
			4 & 5 
			\end{array} } \right]$, $A_2=\left[ {\begin{array}{*{20}c} 
			2 & 3  \\
			5 & 6 
			\end{array} } \right]$, $A_3=\left[ {\begin{array}{*{20}c} 
			4 & 5  \\
			7 & 8 
			\end{array} } \right]$ and $A_4=\left[ {\begin{array}{*{20}c} 
			5 & 6  \\
			8 & 9 
			\end{array} } \right]$. We then reshape these four smaller matrix into columns as $A_1=[1,2,4,5]^T$, $A_2=[2,3,5,6]^T$, $[4,5,7,8]^T$ and $[5,6,8,9]^T$. By stacking them together, we will get $A^*=\left[ {\begin{array}{*{20}c} 
			1 & 2 & 4 & 5   \\
			2 & 3 & 5 & 6   \\
			4 & 5 & 7 & 8   \\
			5 & 6 & 8 & 9
		\end{array} } \right]$.
	\item Reshape the filter $B$ from a $(1,1,2,2)$ tensor to a $(1,4)$ matrix. It can be done by simply reshaping the matrix and we will get $B^*=\left[ {\begin{array}{*{20}c}
			1 & 2 & 3 & 4    \\
		\end{array} } \right]$.
	\item Compute the output. We have $C^*=B^*A^*=[37,47,67,77]$. Then we reshape $C^*$ back to a $2\times 2$ matrix and we will get $C^*_{2\times 2}=C$.
\end{itemize}

\begin{center}
	\begin{figure}[h!]
		\centering
		\includegraphics[width=330pt]{images/im2col.pdf}
		\caption{Illustration of convering input data and filter into matrices}
		\label{fig_2}
	\end{figure}
\end{center}

In Fig. \ref{fig_2}, we present a more general example. In this example, we have a coloured image, which can be represented as a $(1,3,3,3)$ tensor $A$, and filters with $2$ output channels which can be represented as a $(2,3,2,2)$ tensor $B$. We can then convert them into matrices $A^*$ and $B^*$ as shown in the image, and we will then have $C^*=A^*B^*$. We can then reshape $C^*$ to get the output of convolutional layer. 

The benefit of using the \textit{im2col} technique is that we can convert the complex convolutional operation into matrix multiplication. We can use $x^*$ to denote the input data in column format and $w^*$, $b^*$ to denote the parameters in the convolutional layer. Then similar to fully connected layer, we will have $f(x_{ij}^*)=w^*x_{ij}^*+b^*$, and the derivative also becomes straightforward as we have: 

\begin{itemize}
	\item $\frac{\partial l}{\partial w^*_{ij}}=\frac{\partial l}{\partial f}\frac{\partial f}{\partial w^*_{ij}}=\frac{\partial l}{\partial f}x^*_{ij}$.
	\item $\frac{\partial l}{\partial b^*_{ij}}=\frac{\partial l}{\partial f}$
	\item $\frac{\partial l}{\partial x^*_{ij}}=\frac{\partial l}{\partial f}\frac{\partial f}{\partial x^*_{ij}}=\frac{\partial l}{\partial f}w^*_{ij}$
\end{itemize}
	
After computing these derivatives, all we need to do is to convert it back to original tensors.

\subsection{Pooling Layer}

Pooling layer is another important component of convolutional neural networks. There are many ways of performing pooling in this layer, such as max pooling, average pooling, etc. In this part, we will only discuss max-pooling layer as it is used most commonly in convolutional neural networks.

In Max pooling layer, we also have a spatially small sliding window called the kernel. In the window, only the largest value will be remained and all other values will be dropped. For example, assume we have 

$$A=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 3 \\
		4 & 5 & 6   \\
		7 & 8 & 9   
	\end{array} } \right]$$ and a $2\times 2$ max-pooling kernel. Then the output $C$ will be 
$$C=\left[ {\begin{array}{*{20}c} 
		5 & 6 \\
		8 & 9 \\
	\end{array} } \right]$$

With the given kernel size $K_w$ and $K_h$, We can formalize the max-pooling process as 

\begin{equation}
	f(x_{ij})=
	\begin{cases}
		x_{ij} & \text{$x_{ij}\geq x_{mn}, \forall m\in [i-K_w, i+K_w], n\in [j-K_h,j+K_h]$} \\
		0      & \text{otherwise}                                                            \\
	\end{cases}
\end{equation}

Hence we can compute the derivative as below: 

\begin{equation}
	\frac{\partial l}{\partial x_{ij}}=\frac{\partial l}{\partial f}\frac{\partial f}{\partial x_{ij}}=
	\begin{cases}
		\frac{\partial l}{\partial f} & \text{$x_{ij}\geq x_{mn}, \forall m\in [i-K_w, i+K_w], n\in [j-K_h,j+K_h]$} \\
		0                             & \text{otherwise}                                                            \\
	\end{cases}
\end{equation}


\section{Interpretation Model}

When we try to interpret the model as suggested by \cite{matthew2014visualizing}, we attached a deconvolutional network to each layer in the original classification network. By doing so, we provide a continuous path back to the input image pixels. 

For example, assume we have a simple classification neural network that has a \textit{convolution}-\textit{pooling}-\textit{fully connected} architecture. Then in order to understand the impact of the input pixels on the activation maps after pooling, we first perform unpooling - the inverse operation of pooling, and then deconvolution. After these steps, we can reconstruct the pixels in the input image that impact the pooling layer. By using the same strategy, we can also reconstruct the activities in the intermediate layer, for example, the result of convolutional layer in our case, and analyse how they impact the pooling layer.

In the interpretation model, we need to use the weight in classification model and cannot perform any training in the interpretation process. Hence, in the inverse operations, we only need to compute the forward pass.

From the example above, we need two essential components in the Interpretation model, the deconvolutional layer and the unpooling layer.

\subsection{Deconvolutional Layer}

In order to define the deconvolution operation, we need to investigate the convolution operation again. In our previous example, we have a $3\times 3$ matrix $x=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 3 \\
		4 & 5 & 6   \\
		7 & 8 & 9   
		\end{array} } \right]$ and a $2\times 2$ filter $w=\left[ {\begin{array}{*{20}c}
		1 & 2    \\
		3 & 4   
	\end{array} } \right]$, and our goal is to find the output $y$. The convolution operation can be characterized as a convolution matrix $M$ where $M$ solely depends on the filter $w$. In our example, the matrix $M$ can be computed as:

$$M=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 & 0 \\
		0 & 1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 \\
		0 & 0 & 0 & 1 & 2 & 0 & 3 & 4 & 0 \\
		0 & 0 & 0 & 0 & 1 & 2 & 0 & 3 & 4
	\end{array} } \right]$$

Then if we reshape the input into a $9\times 1$ column vector $x^*$, we will have $y^*=Mx^*$. Now if we want to do the inverse operation, i.e. we want to go back from the $4\times 1$ matrix $y^*$ to a $9\times 1$ matrix $x^*$, we only need to transpose our convolution matrix $M$. In our case, we have 

$x^{-1}=M^Ty^*=\left[ {\begin{array}{*{20}c} 
	1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 & 0 \\
	0 & 1 & 2 & 0 & 3 & 4 & 0 & 0 & 0 \\
	0 & 0 & 0 & 1 & 2 & 0 & 3 & 4 & 0 \\
	0 & 0 & 0 & 0 & 1 & 2 & 0 & 3 & 4
\end{array} } \right]^T \left[ {\begin{array}{*{20}c}
	37 & 47 & 67 & 77   
\end{array} } \right] = $


\subsection{Max Unpooling Layer}

Unfortunately, as some inputs are being dropped in the pooling layer, we cannot fully inverse the max pooling operation. However, if we have the position of each maximum value when performing max pooling, then we can simply put the maximum value back to its original position. After putting them back, we can set values at other positions to be zero.



\chapter{Example}

\chapter{Implementation}

\chapter{Experiments}

\chapter{Appendices}

\bibliographystyle{alpha}
\bibliography{refs.bib}

\end{document}