\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{minted}
\usemintedstyle{vs}
\title{VGG Net and Motivation behind AID}
\author{Xiaozhe Yao}
\date{May 2020}
\usepackage{biblatex}
\usepackage{graphicx}
\addbibresource{refs.bib}

\begin{document}

\maketitle

In this article, we will explain the motivation behind AID, and some use
cases/real examples. The article will be organized into the following sections:
\begin{enumerate}
    \item Introducing several terms and definitions in deep learning, so that we
    can be on the same page of all terms.
    \item Introducing how the deep learning model converts users' input into
    output, and how the data changes during the processing. More specifically,
    we will discuss an example for image classification task: VGG-Net.
    \item Discussing some use cases of multiple solvers in a single package. In
    this section, we will investigate an example, the face recognition, to
    demonstrate the importance of multiple solvers.
    \item Discussing the motivation behind model serving.
\end{enumerate}

\section{Definition and Terms}

In this article, we will introduce several terms. They are defined below:

\begin{itemize}
    \item \textbf{Model Training}. The process of computing parameters (it is
    also called weight) based on given neural network architecture and a
    dataset.
    \item \textbf{Model Loading}. The process of loading models into memory. 
    \item \textbf{Model Serving}. The process of deploying models so that other
    systems can send requests and get output. The serving program is usually a
    web server that reads users' input and returns the output.
    \item \textbf{Model Compression}. The techniques to try to remove some
    parameters from a model while retaining the accuracy.
\end{itemize}

There are also several terms in this article, such as \textit{Transfer
Learning}. Explaining them requires more background and thus I will try to
explain them where it appears. We will use \textit{italics} to denote new terms
in the following text.

\section{Deep Learning Mechanism}

In the meeting last week, we viewed the VGG-net together briefly. In this
section, we will introduce how the deep learning model converts the users' input
to the output. We will take image classification as an example. In this case,
the deep learning model converts an image into a classification result (e.g.
cup).

Convolutional neural network (CNN or ConvNet) is a class of deep neural
networks, which plays an important role in image classification. Though there
are several different types of CNNs, they have similar architectures. From the
aspect of different functions, their common components can be categorized into
the following:
\begin{itemize}
    \item \textbf{Convolution and Pooling Layers}. They begin with several
    stacks of \textit{convolution} and \textit{pooling} layers. Usually, we
    periodically insert a pooling layer in-between successive convolution layers
    in a ConvNet architecture. These stacks convert the vector into a
    high-dimensional feature map, i.e. these stacks are trying to get
    easy-to-classify features.
    \item \textbf{Fully Connected Layers}. After several stacks of convolution
    and pooling layers, there will be several fully-connected layers. These
    layers are used to classify images into different categories. 
    \item \textbf{Softmax Layer}. At the end of the CNNs, there will be a
    softmax layer which converts the results of fully connected layers into
    probabilities of each category.
\end{itemize}

After these layers, we see that an image will be converted into a list of
probabilities of each category. In the following part of this section, we will
first explain how each layer works in more detail, and then investigate an
example, the VGG net. After that, we will spend some time in discovering what's
included in a deep learning model file.

\subsection{Convolutional Neural Network}

If we have an image with the size $w\times h$, then naturally, we can represent
the image by a $w\times h$ matrix. Then, in computers, the images are encoded
into color channels, i.e. the image data is represented into each color
intensity in a color channel at a given point. The most commonly used color
channels are RGB, i.e. Red, Green and Blue. Thus, a color image can be
represented with a $w\times h\times 3$ tensor. Here, the last scalar in the
representation is called the \textbf{depth}.

\subsubsection{Convolution Layers}

With the above image representation, we can perform convolution operation on the
tensor. The Convolution layer's parameters consist of a set of learnable
filters, or called kernels. The filters are spatially small, and with the same
depth with the input data. When performing the convolution operation, we slide
(more precisely, convolve) the filters across the width and height of the input
data, and compute dot products between the entries of the filter and the input
at every position. As we slide the filter over the width and height of the
input, we will produce a 2-dimensional matrix that gives the responses of that
filter at every spatial position. The sliding process is illustrated in Fig
\ref{fig_1}.

Each filter can be regarded as a pattern extractor. By using a manually-chosen
filter, we can extract some high-level features from an image, for example,
boundaries. In Fig  \ref{fig_2}, we demonstrate the high-level features after an
edge detection filter. Thus, the key idea behind filter is that, by learning the
parameters in filters, we can have several high-level features from the original
image, and achieve higher classification accuracy with these features.

From the computation process of convolution operation, we see that the depth of
filters in the first convolution layer must be the same with the depth of the
input image. If it is not the case, we will not be able to compute the dot
product. Then, the output of convolution operation, no matter the depth of the
original image, is always a 2-dimensional matrix because the output of the dot
product is a scalar. This 2-dimensional matrix is called \textbf{feature map} or
\textbf{activation map}.

In each layer, we can have multiple filters stacking together to extract
different features. They are independent of each other, and we simply stack them
into a single tensor to give the output. Therefore, the output of the
convolution layer is also a tensor with width, height and depth, here the depth
is the number of filters.

\begin{figure}[h!]
\includegraphics[width=330pt]{convolution.pdf}
  \caption{Illustration of Convolution Operation with the kernel size of 
  $2\times 2\times 1$ on a $5\times 5\times 1$ image. There are two boxes on 
  the left upper corner as examples. After these two computations, we continue 
  to slide the filter to the right corner. After the first two rows, we then 
  slide on the second and third row. We will keep sliding the filter again and 
  again until we go through the whole image. After the sliding process, we will 
  get a $4\times 4$ matrix.}
  \label{fig_1}
\end{figure}

\begin{figure}[h!]
\includegraphics[width=330pt]{edge-detection.pdf}
\caption{An example of a filter works as a feature extractor. In this example, 
we apply a manually chosen filter to an image, and get the boundaries of the 
object in the image.}
 \label{fig_2}
\end{figure}

To summarize, Convolution layers are used as feature extractor, and we want it
to extract some highly classifiable features so that our classification
algorithm easily recognize these features. In deep learning, we do not manually
set the parameters in a filter, but we initialize them with random values, and
then find a good one (it's usually not the global optimal one, and even not the
local optimal, so it is only acceptable) during the training.

\subsubsection{Pooling Layer}

Pooling layer is another building block of a CNN. Like filters, pooling layer
also has pooling kernel with a width and height, and it basically chooses an
element as output in the kernel. The process is illustrated in Fig \ref{fig_3}.
Pooling layer aims to progressively reduce the spatial size of the intermediate
tensors to reduce the number of parameters and computation in the network.
Pooling layers do not need any parameters to compute.

\begin{figure}[h!]
\includegraphics[width=330pt]{pooling.pdf}
\caption{Illustration of max pooling layer with a $2\times 2$ kernel.}
\label{fig_3}
\end{figure}

There are also some other kinds of poolings, such as average pooling, which
returns the average value as output. In practice, people found that max pooling
usually works better.

Pooling layer operates independently on every depth slice of the input. The
output of a pooling layer is also a 3-dimensional tensor, with the same depth as
the last layer, but a smaller width and height.

\subsubsection{Fully Connected Layers}

Fully connected layers in convolutional neural networks are the same with
regular neural networks. The input to the first fully connected layer is the
output of pooling layer. More precisely, the output of the pooling layer, as a
3-dimensional tensor, will be first flattened to a vector, and then fed into the
fully connected layers. In Fig \ref{fig_4}, we illustrated a structure of
several fully connected layers.

In each layer, we perform the computation as $f(\textbf{X})=g(\textbf{w}^TX+b)$,
where $\textbf{w}$ (for weight) and $\textbf{b}$ (for bias) are parameters that
we learned during the model training and $g$ is an \textit{activation function}.
If we assume that layer $i$ requires a $p_i\times 1$ vector as input, and its
output is $q_i$, then the weight will be a $p_i\times q_i$ matrix and the bias
will be a $q_i\times 1$ vector. The weight and bias will be learned during the
training process.

\begin{figure}[h!]
\includegraphics[width=330pt]{fcn.pdf}
\caption{Illustration of several fully connected layers.}
\label{fig_4}
\end{figure}

After computing the $\textbf{w}^TX+b$, we usually apply an activation function
on the result. The purpose of such functions is to add some non-linearity to the
neural networks. As we see above, all operations we have are linear. Images in
real life are usually not linear, and thus it cannot be approximated by such a
linear system. To tackle the problems, we add some non-linear functions at the
end of each fully connected layer. The most successful one is $ReLU$(Rectified
Linear Unit) function, which basically equals to $g(x)=max\{0,x\}$.

After the fully connected layers, we will have $n$ nodes where $n$ equals the
number of categories. Each of them is a non-negative value.

\subsubsection{Softmax Layer}

At the end of our neural networks, we want our output to be a probability
distribution. Thus we apply softmax function to each node. The softmax function
is defined as
$$\delta(z_i)=\frac{e^{z_i}}{\sum_{j=1}^{n}{e^{z_j}}}$$ where $n$ represents the
number of categories.

At the end of the whole neural network, we get a list of probabilities of each
category. As we see in the above process, to perform the image classification,
the convolution layer and fully connected layers require some parameters to
compute the output. These parameters will be computed in the model training
process by using gradient descent approach. After the training process, these
parameters will be saved to a weight file. When performing inference, we just
load the parameters from the file into memory, and compute the output of the
neural networks.

\subsection{VGG Net}

With these backgrounds in the convolutional neural network, the VGG net becomes
simple. In the original paper\cite{simonyan2014very} of VGG net, they call the
convolution layer and fully connected layer as weight layer, as they require the
parameters. There are two types of VGG Net in their paper: VGG-16 and VGG-19.
They both have three the same fully connected layers, thus VGG-16 has 13
convolution layers and VGG-19 has 16 convolution layers.

To simplify the illustration, we will use VGG-16 as an example. The architecture
of VGG-16 is illustrated in Fig \ref{fig_5}. In the illustration, we see that it
requires the input to be a $224\times\ 224\times 3$ tensor, and then perform
convolution operation and max pooling. After several convolution and max pooling
layers, we will have a high-level feature map of that image, and the output will
be fed into three fully connected layers for classification. In the end, there
will be $1000$ nodes as the final output, since there are $1000$ categories in
the original dataset (the imagenet dataset\cite{deng2009imagenet}) used by VGG
net.

The contribution of VGG net is that it shows the depth of the neural network is
a critical component for good performance. It has a much deeper network
architecture and much more parameters (138 million) than others at that time. As
it has a rather clear architecture, but are expensive to evaluate, it becomes a
good starting point for other researches, such as model serving, model
compression, etc.

From our previous understanding of convolution layers and fully connected
layers, we can understand that the first 13 weight layers, i.e. the convolution
layers work as the feature extractor while the last three weight layers, i.e.
the fully connected layers work as the classifier. One thing that needed to be
pointed out here is that, after training with the large imagenet dataset, we can
remove the parameters for the last 3 fully connected layers, and only remain the
parameters for the first 13 convolution layers. Then if we need to build an
image classification algorithm on a new dataset, we can fix the parameters for
the first 13 convolution layers, and only compute new parameters for the last 3
fully connected layers. This process is called \textit{Transfer Learning}. This
possibility allows us to share parameters across different classifiers, and
builds the basis for AID to allow its solvers combine multiple solvers in a
single package, which we will discuss in more detail in \textbf{Section 3: Use
Cases of Multiple Solvers}.

\begin{figure}[h!]
\includegraphics[width=330pt]{vgg16.pdf}
\caption{Illustration of VGG-16 Architecture.}
\label{fig_5}
\end{figure}

\subsection{Inside a Model File}

In the meeting last week, we met a problem about what's inside a model file. The
format varies from framework to framework. For example, they are different in
PyTorch and TensorFlow:
\begin{itemize}
    \item In PyTorch, the learnable parameters is called a \textit{state\_dict}.
    It works like key-value storage and maps each layer to its parameter tensor.
    After training, all the parameters will be saved to disk, but there will not
    be any information about the network architecture. Thus, to load a PyTorch
    model file, users will need to know how the models' architecture looks like.
    \item In TensorFlow, the model file is called a \textit{SavedModel}, which
    contains the parameters and the model architecture. Users can also save
    parameters only in other formats.
\end{itemize}

To summarize, though the format may vary, all the model files, no matter what
framework they are using, will always include the parameters after training.
Otherwise, we will not be able to load it for inference. Some frameworks allow
users to save the network architecture into the model file to help them deploy
and share the model.

\section{Multiple Solvers}

In this section, we will discuss the motivations and use cases of multiple
solvers inside an AID package.

In AID, we proposed a concept called \textit{package} to allow multiple solvers
to share a same weight file, or other configurations. This feature becomes
especially useful when we want to transfer the parameters that we learned in a
task to solve a new kind of task. 

For example, as we discussed in VGG-net, we have two tasks with the same
parameters for the first 13 convolution layers. Then we can save these fixed
parameters into a single weight file. For the last 3 fully connected layers, we
store two different versions of the weight file. Therefore, we will have three
weight files inside our package: 1 for convolution layers, 2 for different fully
connected layers.

Then we will have two different solvers for those two tasks: one for image
classification to imagenet categories, one for image classification to manually
collected categories. They load the same weight file for convolution layers but
different weight file for fully connected layers. The relations inside the
package are illustrated in Fig. \ref{fig_6}:

\begin{figure}[h!]
\includegraphics[width=270pt]{multiple-solvers.pdf}
\caption{Illustration of the package contains multiple solvers.}
\label{fig_6}
\end{figure}

In real applications, it is usually not practical to train the whole neural
network on our own dataset because the neural network may have hundreds of
layers and it cost too much to train it from the very beginning. We usually use
some pretrained models, and then remove the parameters for the last several
layers, and train those layers with our own data. Therefore, as the dataset is
different, there will be multiple different final layers. If we don't support
them to share the same weight, we will have to copy the weight for the first
hundreds of layers without any benefit.

From the perspective of research, we can also investigate how to better handle
the common parameters in two solvers. For example, if one solver has already
loaded the parameters into memory, can we tell another solver to directly use
those parameters, rather than read them again?

\section{Model Serving}

In this section, we will discuss the motivations of having model solvers in AID.
The primary use cases for the solver is to help people reproduce deep learning
experiments and reuse the models. We will first look into why reproducing is
important in the deep learning community, and then discuss why our solution can
be helpful in such cases.

\subsection{Reproducibility of deep learning models}

Unfortunately, there is still no standard terminology defining reproducibility
to this day. By \cite{goodman2016does}, there are three types of reproducibility
in computational sciences:
\begin{itemize}
    \item Methods Reproducibility: A method is reproducible if reusing the
    original code leads to the same results.
    \item Results Reproducibility: A result is reproducible if a
    re-implementation of the method generates statistically similar values.
    \item Inferential Reproducibility: A finding or a conclusion is reproducible
    if one can draw it from a different experimental setup.
\end{itemize}

Another unfortunate situation in the deep learning community is that the method
and results reproducibility are difficult to achieve. There are some sources
contribute to the run-to-run variation, for example:
\begin{itemize}
    \item Random Initialization of layer weights. As we have seen in the
    convolutional neural networks, the weights of convolution and fully
    connected layers are randomly initialized. If we don't have the exact the
    same parameters as the initial value, our models will perform differently.
    \item Shuffling of the dataset. When some users try to train their model,
    they usually shuffle the training dataset randomly, and pick a small amount
    as test dataset. Then when other users want to reproduce their result, they
    will probably shuffle their dataset in a different manner. Therefore, their
    result will be different from the first users.
    \item Updates in Deep Learning Frameworks. Updates to deep learning
    libraries can sometimes lead to subtly different behavior, and migrating a
    model from one framework to another can lead to larger variations between
    the results. For example, TensorFlow warns users to rely on approximate
    accuracy, not on the specific bits computed across different versions.
\end{itemize}

These reasons make it difficult and tedious to reproduce previous works. For
example, if someone wants to reproduce a result by others, he will need to check
the deep learning framework versions, find the proper dataset and then manually
set the configuration for parameter initialization. It not only cost much time
for researchers, but also sometimes is impractical because some information is
missing in the original paper.

Our aim in setting the solver is to provide an environment that can be used
directly for reproducing the deep learning experiment. Hence, people can save
time and effort in managing environments, versions, and other configurations. 

\subsection{Model Serving in Deep Learning Pipeline}

In the deep learning pipeline, models serving itself is a significant step, as
it is the only part that requires high performance (more specifically,
low-latency). There are already several works on this, such as
\cite{deshpande2006mauvedb} which incorporate machine-learning models into a
traditional relational database while preserving the declarativity of SQL-based
query languages, and then the approach can use existing optimization techniques
to query the most efficient models. In the meantime, some people are also
working on general-purpose model serving techniques by using some optimizations
on computer hardware.

In recent years, there's a growing number of techniques in model compression. It
would be interesting to investigate how these two techniques can be used
together to improve performance. For example, if we have already a serving
system, and other systems are keeping using the algorithm to infer, can we
automatically collect the requests they send, and continuously compressing our
model?

We still need more time to investigate and verify these ideas, but serving
should play an important role in the deep learning pipeline, as it is the entry
for other systems and there are lots of ways to improve it. 

\printbibliography
\end{document}
