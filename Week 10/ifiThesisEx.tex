\documentclass[abstracton,12pt]{scrreprt}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{times}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{neuralnetwork}
\usepackage[ruled,vlined]{algorithm2e}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
\lstset{style=mystyle}

% --------- 

\titlehead{Department of Informatics, University of Zürich}
\subject{\vspace*{2cm}MSc Basic Module}
\title{Implementing Deconvolution to Visualize and Understand Convolutional Neural Networks}
\author{
  Xiaozhe Yao\\[-5pt]
  \scriptsize Matrikelnummer: 19-759-570\\[-5pt]
  \scriptsize Email: \texttt{xiaozhe.yao@uzh.ch}
}
\date{\vspace*{2cm}July 11, 2020}
\publishers{
  % \small supervised by Prof.\ Dr.\ x.\ yyy and y.\ zzz \\[5cm]
  \begin{tikzpicture}[overlay]
    \node at (-3,-3) {\includegraphics[height=1.5cm]{IFIlogo}};
    \node at (7,-3) {\includegraphics[height=1.5cm]{dbtgBW}};
  \end{tikzpicture}
}

% --------- 

\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newenvironment{proof}
  {\noindent{\bf Proof:\rm}}{\hfill$\Box$\vspace{\medskipamount}}

\def\bbbr{{\rm I\!R}}
\def\bbbm{{\rm I\!M}}
\def\bbbn{{\rm I\!N}}
\def\bbbz{{\rm I\!Z}}

% --------- 

\begin{document}

\maketitle

% \chapter*{Acknowledgements}

% \tableofcontents

\chapter{Introduction}

Deep convolutional neural networks (CNNs) have played an important role in the recent development in the field of pattern recognition, especially in the computer vision tasks, such as image classification, object detection, etc. However, as a black-box model, it is still unclear how these neural networks works. Hence, much researches are working towards the interpretability of deep convolutional neural networks, such as \cite{matthew2014visualizing}. They proposed an approach that uses the deconvolution operation to visualize the features in convolutional layers. In their approach, there are three important components: 1) the pretrained deep convolutional neural network, including convolutional layers, pooling layers, fully connected layers and ReLu layers. 2) the deconvolution operation and 3) the max-unpooling operation. In this work, we will present a novel implementation of these components. On top of the implementation, we will build a cat classifier with the deep convolutional neural network, and then try to explain the classifications on a given dataset. After that, we will compare our result with a PyTorch implementation. 

This report will be organized into the following sections:
\begin{itemize}
	\item Related Work. In this section, we will briefly introduce the techniques that we need. More specifically, we will introduce the \textit{Deconvolution Operation}.
	\item Approach. In this section, we will thoroughly explain the mechanisms of essential components in our system: \textit{the fully connected layer}, \textit{convolutional layer}, \textit{deconvolution layer}, \textit{max pooling} and \textit{max unpooling}.
	\item Example. In this section, we will present a minimal example of using our approach to perform image classification and interpret the classification results.
	\item Implementation. In this section, we will explain how we implement the training, evaluating and interpreting process. More specifically, we will present the implementation of back propagation, SGD and other utilities.
	\item Experiments. In this section, we will present the architecture and training settings we used in the classification model. With the classification model, we will present the classification results on a given dataset, and try to interpret it with a deconvolutional network. Besides these, we will also compare the results of using our naive implementation and a PyTorch implementation.
	\item Appendices. In this section, we will present the theoretical inductions and implementations of all other necessary but not that important layers. 
\end{itemize}

\chapter{Preliminaries}

\textbf{Convolutional Neural Network}:  Convolutional Neural Network (CNN) is a class of neural networks, and has been proven to be effective for most computer vision tasks. In a CNN architecture for image classification, there are usually three important components: the convolutional layer, the pooling layer and the fully connected layer. The first two types of the layer are designed to extract high-level features from images, and the fully connected layer can be used as a classifier to output the classification results. In a convolutional neural network, the convolutional and fully connected layers are equipped with parameters called \textit{weight} and \textit{bias}, which will be updated during training. In this work, we implemented these components along with other necessary components, such as activations, losses, etc. 

\noindent
\textbf{Deconvolution}: The two fundamental components of the CNN are convolutional layers and pooling layers, which works together to transform images into feature maps. Deconvolutional operation is basically a transformation that goes in the opposite direction of a normal convolution operation, i.e. from the feature maps that we extracted with convolution operation to something that has the shape of the input to it. After being introduced, deconvolution has been used in many fields such as pixel-wise segmentation, generative models, etc. In this work, we use deconvolution to map the intermediate feature maps back to the input pixel space. With this approach, we can show what input pattern caused a given activation in the feature maps. There are also two components in the approach, called deconvolutional layers and unpooling layers, and we will explain these two concepts in more detail in \textit{Section 3}.


\noindent
\textbf{Stochastic Gradient Descent}: In neural networks, we want to find a function $F$ such that $y-F(x)$ is minimal. The function that we are looking for is usually non-linear, non-convex and there is generally no formula for it. As a result, gradient descent becomes one of the most popular methods to find the local minimum of the function. The method is based on a fact that the function $f$ decreases fastest along the direction of the negative gradient. Formally, if we let $a_{n+1}=a_n-\epsilon\nabla f(a)$ and we want to find the lowest value of $f(a)$ around the point $a$. Then if $\epsilon$ is small enough, we will have $f(a_{n+1})\leq f(a_n)$ and $f(a_{n+1})$ is the smallest value around a small enough interval of $a$. Considering this, if we want to find the local minimal of the function $f$, we can start at a random point $a_0$, and follows the negative direction of the gradient. With this approach, we will have a sequence $a_1, a_2,\cdots a_n$ that satisfies $a_{n+1}=a_n-\epsilon\nabla f(a)$. Then the output of the function $f$ will satisfy the rule that $f(a_n)\leq f(a_{n-1})\leq\cdots \leq f(a_{0})$. By doing so, we could find an approximate value $a_n$ such that $f(a_n)$ is the local minimal.

\noindent
\textbf{Back Propagation}: In the process of gradient descent, we found that we need to compute the gradient of our function $f$ in every step. Back propagation, as an application the chain rule, is an efficient algorithm for calculating the gradient in deep neural networks. In short, it first computes the gradient of the loss function to the weight of the last layer in a neural network, and passes the gradient of the loss function to the input of the layer to previous layers. There are two bases for the algorithm: First in the $i$-th layer, we can receive the gradient of the loss $l$ with respects to the output of $i$-th layer, i.e. $\frac{\partial l}{\partial \hat{y}_i}$. Secondly, since the output of $(i-1)$-th layer is the input of the $i$-th layer, we have $\frac{\partial l}{\partial x_i}=\frac{\partial l}{\partial \hat{y}_{i-1}}$. With back propagation, we can efficiently compute the gradient of the loss function with respect to the weight of every layer.

\noindent
\textbf{Notations} In the below sections, we will by default use the following notations:

\begin{itemize}
	\item  $x_j^{(i)}$ is the $j$-th input of the $i$-th layer. We will use $x^{i}$ to denote the vector that contains all the inputs of the $i$-th layer. In fully connected layers, 
	
	All input nodes will form a vector (in fully connected layers) or a matrix (in convolutional layers), which will be represented by $X$.
	\item $\hat{y}_j^{(i)}$ is $j$-th the output of the $i$-th layer. If the $i$-layer is the last layer, there might be some corresponding ground truths. In this case, the ground truths will be denoted as $y_j^{(i)}$. We will use $\hat{y}^{i}$ to denote the vector that contains all the outputs of the $i$-th layer.
	\item $\ell$ is the value of loss function. In our classification model, we use the cross-entropy loss and it is defined as $\ell = -\sum_{i}^{n}y_j^{(i)} log(\hat{y}_{j}^{(i)})$ where $i$ is the index of the last layer.
	\item $\nabla_i$ is the gradient of the loss value with respect to the output of the $i$-th layer. Formally, we have $\nabla_i$=$\frac{\partial\ell}{\partial \hat{y}^i}$. Since the output of the $i$-th layer is also the input of the $(i+1)$-th layer, we also have $\nabla_i=\frac{\partial\ell}{\partial x^{(i+1)}}$
\end{itemize}


\chapter{Approach}

In this work, we will build a classification to categorize the input image, and an interpretation model that performs the inverse operation of the classification model and try to interpret the intermediate activities. In the classifier, we use a VGG-like network consisted of convolutional layers, max pooling layers and fully connected layers, while in the interpretation model, we use the corresponding deconvolutional layers, max unpooling layers to perform the inverse operations. In this section, we will induce the computation of the forward and backward pass of these layers, and describe our network architecture.

\section{Classification Model}

After proposed, VGG-net \cite{simonyan2014very} has been proven to be effective in image classification task and we applied a VGG-like architecture in our neural network. There are three essential components inside a VGG-like network: the fully connected layer, the convolutional layer and the pooling layer. In order to training the classifier on our own dataset with the SGD method, we need to know how to compute the forward and backward pass of these layers.

\subsection{Fully Connected}

\textbf{Definition} The fully connected layer is the simplest and most fundamental neural network component as it basically connects all the nodes in a layer to all the nodes in the next layer. For example, in \ref{fig_1} we present a simple fully connected layer that connects all nodes, in which yellow nodes represent the bias (the nodes that are independent from the output of the previous layer) in each layer, green nodes represent the input to the neural network, blue nodes represent the intermediate activities and red nodes represent the output results.

\begin{center}
	\begin{neuralnetwork}[height=4, toprow=false,title=Fig 3.1.1 Illustration of the fully connected layers.]
		\label{fig_1}
		\newcommand{\x}[2]{$x_#2$}
		\newcommand{\y}[2]{$y_#2$}
		\newcommand{\hfirst}[2]{\small $h^{(1)}_#2$}
		\newcommand{\hsecond}[2]{\small $h^{(2)}_#2$}
		\inputlayer[count=2, bias=true, title=Input\\layer, text=\x]
		\hiddenlayer[count=2, bias=true, title=Hidden\\layer 1, text=\hfirst] \linklayers
		\outputlayer[count=2, title=Output\\layer 2, text=\y] \linklayers
	\end{neuralnetwork}  
\end{center}

From the definition of fully connected layer, we know that in the $i$-th layer, if there are $n$ input nodes and $m$ output nodes, there will be $m\times n$ relations between them. These relations can be represented as a $m\times n$ matrix $w$ (weight). Besides of these relations, there will be $m$ biases that can be represented as a $m$-d vector $b$ (bias).

\noindent
\textbf{Forward Pass} With the above notations, we can directly compute the forward pass. For example, in \ref{fig_1}, we can compute $h_1^{(1)}$ as $h_1^{(1)}=x_1w^{(1)}_{11}+x_2w^{(1)}_{21}+x_0$ where $w^{(i)}_{jk}$ is the relation between $x_j$ and $h^{(i)}_{k}$. In matrix form, we have $\hat{y}_i=wx+b$ where $w$ is the weight matrix, $x$ is the input vector and $b$ is the bias vector. In the matrix form, $w$ is a $n\times m$ matrix, $x$ is a $n\times 1$ vector and $b$ is a $m\times 1$ vector.

\noindent
\textbf{Backward Pass} In the training process, we want to update the relations between the input nodes and the output nodes. More specifically, we want to know how the weight $w$ and bias $b$ will impact the loss value, i.e. we want to compute $\frac{\partial\ell}{\partial w}$ and $\frac{\partial\ell}{\partial b}$. In the backward pass, the $i$-th layer will receive the gradient of the loss value with respect to the input of the $(i+1)$-th layer. As the input of the $(i+1)$-th layer is the output of the $i$-th layer, we have $\frac{\partial\ell}{\partial \hat{y}_i}$ known indeed. 

With these conditions, we can apply the chain rule to compute the gradients. We will have 

\begin{itemize}
	\item $\frac{\partial\ell}{\partial w}=\frac{\partial\ell}{\partial \hat{y}^i}\frac{\partial \hat{y}^i}{\partial w}=\nabla_i X^T$ (A direct result from the computation rules of matrix derivatives, see appendix for the induction details)
	\item $\frac{\partial\ell}{\partial b}=\frac{\partial\ell}{\partial \hat{y}^i}\frac{\partial \hat{y}^i}{\partial b}=\nabla_i$
\end{itemize}

With these results, we can update the weight and bias in the $i$-th layer with $w^{new}=w^{old}-\epsilon \nabla_i x^T$ and $b^{new}=b^{old}-\epsilon \nabla_i$ where $\epsilon$ is a preset hyperparameter called learning rate. After updating these parameters, we need to pass the gradient of the loss value with respect to the input of this layer to the previous layer. Formally, we also have $\frac{\partial\ell}{\partial x^{i}}=\frac{\partial\ell}{\partial \hat{y}_i}\frac{\partial \hat{y}^i}{\partial x^i}=\nabla_i w^T$.

\subsection{Conv}

\textbf{Definition} In fully connected layers, the input is always a one-dimensional vector. However, images are usually stored as a multi-dimensional matrix and have implicit spatial structures. For example, the eyes are always on top of the nose, etc. These properties are not well expressed using a fully connected layer and we use the convolutional layers to preserve them. For example, assume we have a single channel input matrix $A_{3\times 3}$ and single filter matrix $B_{2\times 2}$, and 

$$A=\left[ {\begin{array}{*{20}c} 
		a_{11} & a_{12} & a_{13} \\
		a_{21} & a_{22} & a_{23} \\
		a_{31} & a_{32} & a_{33}   
		\end{array} } \right], B=\left[ {\begin{array}{*{20}c}
		b_{11} & b_{12}    \\
		b_{21} & b_{22}   
	\end{array} } \right]$$

Then, we slide the filter $B$ with a unit stride, i.e. move one column or one row at a time. If we use $a_{ij}$ and $b_{ij}$ to denote the element in $A$ and $B$ at the $(i,j)$ location. Then we can obtain the output of the convolutional layer with the following steps:
\begin{itemize}
	\item At the beginning, the $2\times 2$ filter is placed at the upper left corner. At this time, we perform the dot product and we will have $y_{11}=a_{11}b_{11}+a_{12}b_{12}+a_{21}b_{21}+a_{22}b_{22}$.
	\item Then we slide the filter across the width for a unit stride, i.e. we move the slide to the upper right corner. At this time, we perform the dot product and we will have $y_{12}=a_{12}b_{11}+a_{13}b_{12}+a_{22}b_{21}+a_{23}b_{22}$.
	\item Then we found that there is no more values on the right side, so we start to slide the filter on the next row again. At this time, we start at the bottom left corner and we can obtain that $y_{21}=a_{21}b_{11}+a_{22}b_{12}+a_{31}b_{21}+a_{32}b_{22}$.
	\item Then we again slide the filter to the right side, i.e. the bottom right corner and we obtain that $y_{22}=a_{22}b_{11}+a_{23}b_{12}+a_{32}b_{21}+a_{33}b_{22}$.
\end{itemize}

After these steps, we found that we get four outputs, and we can obtain the final output if we place the output values to corresponding locations, i.e. the value we computed at the upper left corner is placed at the upper left corner and so on so forth. Hence, in this example, we get a $(1,2,2)$ output matrix $C=\left[ {\begin{array}{*{20}c}
		y_{11} & y_{12}    \\
		y_{21} & y_{22}   
	\end{array} } \right]$

More generally, we can formalize the input, the filters and the output of a convolutional layer as below:
\begin{itemize}
	\item The input is a $(N, C, H, W)$ tensor, where $N$ is the number of the input matrix in a single batch, $C$ is the channel of the matrix, $H$ and $W$ are the height and width of the input matrix. For example, $10$ coloured images with the size $(224, 224)$ can be represented as a $(10, 3, 224, 224)$ tensor.
	\item The filters is a $(K, C, H_f, W_f)$ tensor, where $K$ is the number of filters, $C$ is the channel of the filters and it will always be identical to the channel of the input matrix. $H_f$ and $W_f$ are the height and width of the filters.
	\item The output is a $(N, K, H_{out}, W_{out})$ tensor.
	\item The stride that we use to slide the filters are denoted as $S$.
\end{itemize}

With these notations, we can compute the output of a convolution layer as the algorithm below:

\begin{algorithm}[H]
	\SetAlgoLined
	\KwIn{A $(N, C, H, W)$ tensor as data and a $(K, C, H_f, W_f)$ tensor as filter}.
	\KwResult{The output of a convolution layer, as a tensor of the shape $(N, K, H_{out}, W_{out})$}
	Let output be a zero matrix of the shape $(N, K, H_{out}, W_{out})$ \\
	\While{$n<N$} {
		\While{$o_y<H_{out}$}{
			\While{$o_x<W_{out}$}{
				\While{$o_k<K$}{
					\While{$o_h<H_f$}{
						\While{$o_w<W_f$}{
							\While{$o_c<C$}{
								$prod \gets input[n][o_c][o_y *S + o_h][o_x * S+o_w] \cdot filter[o_k][o_c][o_h][o_w] $
								$output[o_k][o_c][o_y][o_x] += prod$
							}
						}
					}
				}
			}
		}
	}
	\caption{Direct Convolution Computation}
\end{algorithm}

We see that there are $7$ loops needed to compute the output of the convolutional layers, and it is infeasible when the size of the dataset are too large. Therefore, we need a method that transform the process into matrix multiplication so that we can efficiently compute. As suggested by \cite{dumoulin2016guide}, we can convert the filters into a matrix. In the following, we will use the same matrix as the beginning of this section, the input matrix $A_{3\times 3}$ and the filter matrix $B_{2\times 2}$ as an example to demonstrate how it works.

\textbf{Forward Pass} Recall that the input $A$, the filter $B$ and the expected output $C$ we have in the above example is $$X=\left[ {\begin{array}{*{20}c} 
	a_{11} & a_{12} & a_{13} \\
	a_{21} & a_{22} & a_{23} \\
	a_{31} & a_{32} & a_{33}   
	\end{array} } \right], W=\left[ {\begin{array}{*{20}c}
	b_{11} & b_{12}    \\
	b_{21} & b_{22}   
\end{array} } \right], Y=\left[ {\begin{array}{*{20}c}
	y_{11} & y_{12}    \\
	y_{21} & y_{22}   
\end{array} } \right]$$

If we unroll the input and the output into vectors from left to right, top to bottom, we can also represent the filters as a sparse matrix where the non-zero elements are the elements in the filters. For example, We can unroll the input and output in our case as $X^*_{9\times 1}=[1,2,3,4,5,6,7,8,9]^T$ and $Y^*_{4\times 1}=[37,47,67,77]$. Then we will want a $4\times 9$ matrix $W^*$ such that $Y^* = W^*X^*$. From the direct computation of convolutional layers, we can transform the original filters $W$ into 

$$W^*=\left[ {\begin{array}{*{20}c} 
		b_{11} & b_{12} & 0 & b_{21} & b_{22} & 0 & 0 & 0 & 0 \\
		0 & b_{11} & b_{12} & 0 & b_{21} & b_{22} & 0 & 0 & 0 \\
		0 & 0 & 0 & b_{11} & b_{12} & 0 & b_{21} & b_{22} & 0 \\
		0 & 0 & 0 & 0 & b_{11} & b_{12} & 0 & b_{21} & b_{22}
	\end{array} } \right]$$ 

Then we can verify that $$W^*X^*=\left[ {\begin{array}{*{20}c} 
	b_{11} & b_{12} & 0 & b_{21} & b_{22} & 0 & 0 & 0 & 0 \\
	0 & b_{11} & b_{12} & 0 & b_{21} & b_{22} & 0 & 0 & 0 \\
	0 & 0 & 0 & b_{11} & b_{12} & 0 & b_{21} & b_{22} & 0 \\
	0 & 0 & 0 & 0 & b_{11} & b_{12} & 0 & b_{21} & b_{22}
\end{array} } \right] \times [a_{11},a_{12},a_{13}\cdots, a_{33}]^T = Y^*$$. By using this form of representation, we can convert the loop-based computation into a single, well-optimized matrix multiplication.

\textbf{Backward Pass} From the forward pass, we converted the filters into a sparse matrix $W^*$, and we found that the convolution operation is also a linear operation, i.e. $Y^*=W^*X^*$. Similar to the backward pass of fully connected layers, we can directly compute the gradient of the loss value with respect to the weight matrix as $\frac{\partial\ell}{\partial W^*}=\frac{\partial\ell}{\partial Y^*}\frac{\partial Y^*}{\partial W^*}=\nabla_i (X^*)^T$. Hence, we can update the weight matrix in convolutional layers as $(W^*)^{new}=(W^*)^{old}-\epsilon \nabla_i(X^*)^T$ and it is exactly the same as in fully connected layers.

Besides, we will need to compute the gradient that we want to pass to previous layers, i.e. the gradient of the loss value with respect to the input matrix. We will have $\frac{\partial\ell}{\partial X^*}=\frac{\partial\ell}{\partial Y^*}\frac{\partial Y^*}{\partial X^*}=\nabla_i(W^*)^T$.

\subsection{Pooling} 

\textbf{Definition} Pooling layer is another important component of the convolutional neural network, which is used to reduce the spatial size of the intermediate results to reduce the amount of parameters. The most common kind of pooling is max pooling, which splits the input in non-overlapping patches, and output the maximum value of each patch. For example, assume the input that we have is, 
$$X=\left[ {\begin{array}{*{20}c} 
		1 & 2 & 3 \\
		4 & 5 & 6   \\
		7 & 8 & 9   
	\end{array} } \right]$$

Assume that we have a $2\times 2$ max pooling layer, then the pooling layer will first split the input matrix into small $2\times 2$ patches: the upper left, the upper right, the bottom left and the bottom right corners. Then in each patch, we can find the maxmium values are $[5,6, 8,9]$, hence, the output of the max pooling layer is $P=\left[ {\begin{array}{*{20}c}
		5 & 6    \\
		8 & 9
	\end{array} } \right]$.

\textbf{Forward Pass} In the forward pass, only the maximum value in each patch will be left, and all other values will be dropped. Similar to the computation process of convolutional layers, we also have a spatially small sliding window called the kernel, in which the largest value will be kept. Assume that the input of the pooling layer is a $(H,W)$ matrix and if we set the height and width of the kernel as $(K_h, K_w)$ and we use a stride $S$ to slide the kernel from left to right, from top to bottom. Then in each kernel, we select the maximum value as output. We can induce the output shape of a pooling layer as $(\lfloor \frac{H-K_h}{S} \rfloor+1, \lfloor \frac{W-K_w}{S} \rfloor+1)$.

The forward computation can be achieved with the following algorithm:

\begin{algorithm}[H]
	\SetAlgoLined
	\KwResult{The output of a pooling layer}
	Let output be a matrix of the shape $(\lfloor \frac{H-K_h}{S} \rfloor+1, \lfloor \frac{W-K_w}{S} \rfloor+1)$ filled with $-\infty$ \\
	\While{$h<\lfloor \frac{H-K_h}{S} \rfloor+1$} {
		\While{$w<\lfloor \frac{W-K_w}{S} \rfloor+1$}{
			\While{$i_h<K_h$}{
				\While{$i_w<K_w$}{
					$h_{index}\gets h * S + i_h$ \\
					$w_{index}\gets w * S + i_w$ \\
					\If(){output[$h$][$w$] < input[$h_{index}$][$w_{index}$]}{
						output[$h$][$w$] = input[$h_{index}$][$w_{index}$]
					}
														
				}
			}
		}
	}
	\caption{Algorithm for Computing Pooling Output}
\end{algorithm}

\textbf{Backward Pass} In the backward pass, since there is no parameter that need to be adjusted during the training, we only need to compute the gradient that we want to pass to the previous layer. The backward pass of pooling layer can be computed by two parts:

\begin{itemize}
	\item If the value $x_{k,j}$ in the input matrix is kept, then we have $\frac{\partial \ell}{\partial x_{k,j}}=\frac{\partial\ell}{\partial y}\frac{\partial y}{\partial x_{k,j}}=\frac{\partial\ell}{\partial y}=\nabla_i$.
	\item If the value $x_{k,j}$ in the input matrix is dropped, then we have $\frac{\partial \ell}{\partial x_{k,j}}=\frac{\partial\ell}{\partial y}\frac{\partial y}{\partial x_{k,j}}=0$.
\end{itemize}

\subsection{Relu Layer}

\textbf{Definition} From the above sections, we have found that both the fully connected layer and convolutional layer can be categorized into linear functions. However, the reality is usually non-linear and cannot be fitted by compositing linear functions. Hence, we need to bring some non-linearity into the neural network architecture. Among the many non-linear functions, rectifier linear unit (ReLu) is the most commonly used non-linearity. It is defined as a function that $f(x)=max(0, x)$

\textbf{Forward Pass} The input of a ReLu layer is a $n$-d tensor, and we perform element-wise ReLu operation on the input, i.e. it will be kept if it is positive, otherwise it will be set to zero.

\textbf{Backward Pass} Similar to pooling layers, we can separate the backward pass of ReLu layer into two parts:

\begin{itemize}
	\item In the location $(i,j)$, if the value is kept unchanged, then the derivatives will be $\frac{\partial\ell}{\partial x_{i,j}}=\frac{\partial\ell}{\partial y}\frac{\partial y}{\partial x_{i,j}}=\nabla_i$.
	\item Otherwise, if $x_{i,j}\leq 0$, then $\frac{\partial\ell}{\partial x_{i,j}}=\frac{\partial\ell}{\partial y}\frac{\partial y}{\partial x_{i,j}}=\nabla_i\times 0=0$.
\end{itemize}

\section{Interpretation Model}

We use a deconvolutional neural network to interpret and understand the result of our classification model. Here they are not used for any training purpose, but only as a probe of the trained classification model. Therefore, in this section, we only need to have the forward pass of a deconvolution neural network and do not need to compute the backward pass.

\subsection{Deconv}

\textbf{Definition} In the convolution section, we unrolled the filter from a $2\times 2$ matrix into a $4\times 9$ matrix, so that we can perform the convolution by a matrix multiplication. After the convolution operation, the input data changes from a $3\times 3$ matrix to a $2\times 2$ output. The deconv operation is defined as the inverse of the convolution operation, i.e. change the input data from a $2\times 2$ matrix into an output with the shape $3\times 3$ in our example. The deconv operation does not guarantee that we will have an exact the same values in the output as the original matrix.

\textbf{Forward Pass} When computing the forward pass of deconv operation, we can simply transpose the unrolled filters matrix, for example it will be a $4\times 9$ matrix in our case. After the transpose, we can define the deconv operation as $X=(W^*)^T Y$, i.e. we use the transposed, and unrolled filter matrix to multiply the output of the convolution operation.

\subsection{Max Unpooling}

Unfortunately, as some inputs are being dropped in the pooling layer, we cannot fully inverse the max pooling operation. However, if we have the position of each maximum value when performing max pooling, then we can simply put the maximum value back to its original position. After putting them back, we can set values at other positions to be zero.

\subsection{UnReLu}

In \cite{matthew2014visualizing}, they passed the reconstructed data into the ReLu non-linearity during the interpretation process. Hence, in this report, we will also use the ReLu function as the inverse of ReLu, as suggested by their approach.

\chapter{Example}

In this chapter, I am going to place a large example, that illustrate the changes of data through the classification model and the interpretation model. I will use a real image, or a real matrix to perform the computation of each operation, and show how the interpretation model works. 

\renewcommand{\cleardoublepage}{}
\renewcommand{\clearpage}{}
\chapter{Implementation}

In this chapter, I am going to paste and clarify how we implement the back propagation and some important functions.

\chapter{Experiments}

In this chapter, I am going to introduce the training dataset, settings and the results we have.

\chapter{Appendices}

\bibliographystyle{alpha}
\bibliography{refs.bib}

\end{document}