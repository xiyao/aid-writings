\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{minted}
\usemintedstyle{vs}
\title{Explanation of Solver in AID}
\author{Xiaozhe Yao}
\date{May 2020}
\usepackage{biblatex}
\usepackage{graphicx}
\addbibresource{refs.bib}

\begin{document}

\maketitle

The solver is the key concept in AID. It is defined as the minimal unit to
handle a deep learning task. To understand the concept of solver, we will start
by introducing deep learning tasks in our context, as the task in our context is
slightly different from other definition and it is important to understand how
we devise solver. Afterwards, we will explain what solver is in AID.

\section{Deep Learning Task}

The deep learning pipelines may vary from application to application. In order
to find a generic approach to demonstrate the pipeline for deep learning task,
we investigated several deep learning pipelines, including
\cite{noauthor_continuous_nodate}, \cite{mlsystems_design} and
\cite{smith2018machine}. From our own experience and these practices, we found
that in general, all machine learning pipelines can be separated into two
phases: Model Training and Model Serving.

\begin{itemize}
    \item \textbf{Model Training}. With a neural network architecture, users can
    feed their dataset in and then get a weight file containing the parameters
    of the neural network.
    \item \textbf{Model Serving}. After training, the users will have both the
    network architecture and the weight file. The process that takes a trained
    model and makes it available to serve inference requests. To serve the
    model, the serving program will need to load the model into RAM first. More
    specifically, the serving program will first read the parameters from the
    weight file and assign them to the neural network architecture. After
    loading the model, the serving program can perform inference from the given
    data based on the network with parameters. In the process, there are two
    steps.
    \begin{itemize}
        \item Preprocess the user's input, and convert the input data into
        vectors.
        \item Infer from the user's input vector.
    \end{itemize}
\end{itemize}

In our context, AID focused on the model serving phase of a deep learning
pipeline, so we assume users already have well-established models, including
both the network architecture and its parameters. Thus, we abandoned the
\textit{model training} phase from our consideration. Therefore, there are two
steps only in our context: \textit{model loading} and \textit{inference}. In
summary, we define a deep learning task as a \textbf{process that reads the
users input and give an output based on preset neural network architecture and
its parameters}.

\section{Solver}

The solver, by definition, is the \textbf{minimal unit to handle a deep learning
task}. Here the minimal means that all functions required by AID in a solver is
needed for handling deep learning tasks. If we remove any one of them, it won't
work anymore. Due to the tight connections between solver and deep learning
task, we illustrate how a general workflow of deep learning task is in
Fig.\ref{fig_1} to demonstrate how solvers are associated with the needs of a
deep learning task.0

\begin{figure}[h!]
  \caption{General Workflow of Deep Learning Task in AID context.}
  \label{fig_1}
\includegraphics[width=330pt]{task.pdf}
\end{figure}

From Fig.\ref{fig_1}, we see that there are two independent phases in a deep
learning task, the initializing and inferring. The initializing process is only
performed once but the inferring will be performed many times. Hence, it is
standard to separate these two phases into different phases in all deep learning
serving systems. In AID, we also define solvers as a class with two functions.
Hence, from the perspective of objective-oriented programming, \textbf{solver is
a class with a model loading function and an inference function}. 

A minimal running example for solver is illustrated below:

\inputminted[linenos]{python}{solver.py}

Here we see that the solver extends from the base class \textit{aid.Solver} and
implements the \mintinline{python}{__init__} function (for loading the preset
neural network architecture and parameters into the memory), and
\mintinline{python}{infer} function (for receiving the user's input,
preprocessing it and performing inference on it). The
\mintinline{python}{__init__} function corresponds to the process of
initializing deep learning model and the \mintinline{python}{infer} function
corresponds to the inferring process in Fig.\ref{fig_1}.

Inside the \mintinline{python}{__init__} function, the solver will load the
model into memory(\#L5) and call \mintinline{python}{ready} function(\#L6) to
notify users that the model has been loaded successfully. Then in the
\mintinline{python}{infer} function, the solver will load the data into memory
as a vector(\#L8), and preprocess it (\#L9) and perform inference on the
preprocessed new data (\#L10). After all these steps, the function will return
the result to the users(\#L11). In this example, we see that the steps inside a
solver are tightly connected with steps in the general workflow of a deep
learning task.

     To summarize, the solver is defined as a unit to handle a deep learning
     task with two functions, the model loading function and the inference
     function. If we remove any one of these two functions, the solver will be
     unable to solve the machine learning task and that is the reason why we
     called it minimal.

\printbibliography
\end{document}
