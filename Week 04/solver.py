from aid.solver import Solver
class ImageClassificationSolver(Solver):
    def __init__(self, toml_file=None):
        super().__init__(toml_file)
        self.model = VGG16(weights="./pretrained/model.h5")
        self.ready()
    def infer(self, data):
        image = load_image(data['input_file_path'])
        image = preprocess_input(image)
        result = self.model.predict(image)
        return result