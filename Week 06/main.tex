\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2019

% ready for submission
% \usepackage{neurips_2019}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%     \usepackage[preprint]{neurips_2019}

% to compile a camera-ready version, add the [final] option, e.g.:
\usepackage[preprint]{neurips_2019}
% to avoid loading the natbib package, add option nonatbib:
%     \usepackage[nonatbib]{neurips_2019}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{caption}
\title{Week 6: Example of Multiple Solvers in AID and Some More Explanation}

\author{%
  Xiaozhe Yao
  \texttt{xiaozhe.yao@uzh.ch} \\
}

\begin{document}

\maketitle

\begin{abstract}
	In this article, we will discuss some examples of the usage of multiple solvers in AID, including the motivation, techniques, and limitations of using several solvers in a single AID package. When introducing the examples, we will introduce and explain some concepts in the machine learning field, such as \textit{Transfer Learning}. Besides these, we will also discuss the current implementation of the system, its limitations and possible improvements. After that we will go over some papers about speeding up machine learning inference. At the end of this article, we will take a look at the MLOps system, and explain why it deserves some research.
	
	This article will be organized into the following parts:
	\begin{itemize}
		\item \textbf{Definition and Terms.} As usual, we will put all new terms and define them in this section.
		\item \textbf{Transfer Learning.} One important motivation behind using multiple solvers is that we can transfer the weights from one task to another. By using multiple solvers, we can save time and space when sharing deep learning models. In this section, we will introduce the transfer learning techniques in more detail, so that we could come up with some new ideas about utilising the techniques.
		\item \textbf{Examples of Multiple Solvers.} We will describe a concrete example of using multiple solvers in AID. More specifically, we will have an AID package that has two solvers, and they will share some weights in common. 
		\item \textbf{Current Implementation.} In this section, we will introduce more details about how we currently implement AID, and its limitations. This section will include how we distribute the users' requests to multiple nodes to speed up the inference.
		\item \textbf{Inference Engine.} We will introduce the motivation and expectations of \textit{inference engines} for deep learning models. We will also include some techniques for improving the performance of inference in this section.
		\item \textbf{MLOps System.} In this section, we will introduce MLOps System in more detail. We will give an example of using the MLOps system and its advantage. Besides these, we will also discuss some possibilities of MLOps research and why it is needed. This is where AID originally focuses on and we will discuss the possibilities of database research in this field.
	\end{itemize}
	
\end{abstract}

\section{Definition and Terms}

In this article, we will introduce some new terms. They are defined below:

\begin{itemize}
	\item \textbf{MLOps.} MLOps is a set of best practices that combines software development, IT operations and machine learning. The aim of MLOps is to shorten the development lifecycle and provide highly accurate machine learning based software. 
	\item \textbf{Inference Engine.} As we have seen in previous weekly articles, the deep learning process can be divided into two parts, the training phase and the serving phase. The inference engine is the component that is responsible for the serving phase, i.e. it reads the user's input and gives the predication.
	\item \textbf{Supervised Learning.} Supervised Learning is a learning process where we have both the input data and ground truths (we usually call the truths as labels). In supervised learning, we are trying to learn an approximation function between the input and the labels. One simple example for supervised learning is linear regression where we have the $x$ and $y$, and learn a function that predicts $\overline{y}$ from $x$. In contract, \textit{Unsupervised Learning} is a process where we only have the input data but no labels. One example for unsupervised learning is clustering, where we group a set of objects in a way that objects in the same group are more similar.
	\item \textbf{Pretrained Model.} The pretrained model is a deep learning model that is trained on a given dataset. It can be used directly for prediction, or users could use some of its layers as well-established feature extractors. Pretrained models are usually trained on large datasets so that the feature extractors can extract general features.
\end{itemize}

\section{Transfer Learning}

Generally, transfer learning is a process where a model trained on one problem is used in some way on a second related problem. In deep learning, it refers to the technique whereby a deep learning model is first trained on a problem $P_1$, then one or more layers from the trained model are then used in a new model trained on the problem $P_2$ of our interest. 

In \cite{goodfellow2016deep} (Page. 536), they mentioned that "this is typically understood in a supervised learning context, where the input is the same but the target maybe of different nature."

For example, assume we have two tasks stated below:
\begin{itemize}
	\item $P_1$: Classify images from the ImageNet \cite{deng2009imagenet} dataset. ImageNet is a large dataset with over 14 million images in 1000 categories.
	\item $P_2$: Classify images from a hand-crafted dataset with only hundreds images in 10 categories.
\end{itemize}

In the above example, the inputs are the same images, but the target are different: In $P_1$, the prediction is a category in the 1000 categories, but in $P_2$, the prediction is a category in the 10 categories. Hence, we found that even though the inputs are the same, the predictions we want are different if we use a different neural network. Then the problem occurs: Do we need two different neural networks to handle those two tasks? The problem can be then divided into two parts:
\begin{itemize}
	\item \textbf{Do we need different neural network architectures?}
	\item \textbf{Do we need the weights in every layer to be different?}
\end{itemize}

To answer these questions, we need to take a look at how the neural network works. We will take a convolutional neural network (more precisely, the VGG net) as an example. The architecture of VGG-16 are illustrated in Fig. \ref{fig_1}, and we found it consists of only convolutional blocks (including the convolutional layers and pooling layers) and fully connected layers. In the previous article (Week 5), we mentioned that the convolutional blocks can work as the feature extractor, while the fully connected layers are the actual classifiers. Hence, we conclude that there are two functional blocks in VGG-16 net: the feature extractor and classifier. The feature extractor is used to acquire the high-level features (for example, boundaries of objects) from the input image and the classifier is used to predict the category based on the high-level features.

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{vgg16.pdf}
		\caption{Illustration of VGG-16 Architecture. There are 5 convolutional blocks and 1 fully connected layers in the architecture.}
		\label{fig_1}
	\end{figure}
\end{center}

One important assumption is that, with the same high-level features, the different classifiers can output different predictions. For example, in our above example, given a single cat image, our two tasks might be:
\begin{itemize}
	\item $P_1$: Since the Imagenet dataset only includes a general cat category, we expect our deep learning model predict the image to be a cat. 
	\item $P_2$: In $P_2$, we have 10 different types of cats, and we expect our deep learning model predict the image to be the exact type. For example, assume the ten types are of different colors.
\end{itemize}

Ideally, the feature extractor can extract the high-level feature that sufficiently represent the image. In our case, the output of feature extractor might be $(hairy, fat, spotted, with \: tail)$. In different tasks, we will use the same feature and find out if the different classifiers can get different predictions.

With the first classifier in $P_1$, we can infer that the image represents a cat. The classifier make such a prediction because in the dataset, most objects with the property $(fat, hairy, with \: tail)$ are cats. In $P_2$, we may have a different classifier which also takes the color into consideration. In this case, the prediction given by the classifier will be a spotted cat as the color feature is also captured.

From this simple example, we can answer the two questions above: We do not necessarily need a completely different neural network architecture as we can use the same architecture but with different weights to achieve the goal. Besides, we do not need the weights in every layer to be different. As long as the feature extractor can capture enough high-level features, we can change the classifier partially and then it can predict the image to our desired category.

Then there comes a new question: what if our feature extractor works well in the first case, but not in the second case? This can sometimes happen because if we consider the convolutional blocks as feature extractors, then the lower level convolutional layers (the layers that are closer to the input) will extract some low level, general features (such as lines, colors, etc) while higher level convolutional layers will extract some high level, specific features (such as hairy, boundaries, has tail or not, etc). More precisely, the features that we learned through a convolutional neural network are hierarchical. If the features are too specific, we may not be able to get a good classifier for our task. For example, with the same feature $(hairy, fat, spotted, with \: tail)$, but our task is to identify what color of the cat is in the image. In this case, no matter how we tried to train our classifier, it will not work very well. Therefore, in some cases, we still need to train the convolutional layers. However, we can still reuse some low-level convolutional layers.

To conclude, we can have four strategies when facing a new deep learning task as illustrated in Fig. \ref{fig_2}, from left to right they are:
\begin{itemize}
	\item Use the pretrained model directly. This strategy usually not happens, because in most cases, our task is somehow different from the task where the pretrained model was trained. In this case, the output of the pretrained model may not be what we need (for example, here we expect our model predict the image to be a `spotted cat', not a `cat').
	\item Train the classifier only and leave the convolutional layers fixed. This is an extreme case where we use the deep learning model as a feature extractor and build our own classifier. We will usually use this strategy when our computational power is limited, the dataset that we have is small, or the pretrained model solves a problem that are similar to the ones they we are interested in.
	\item Train some high-level convolutional layers and the classifier, leave others fixed. 
	\item Train the entire model. In this case, we will use the architecture in the deep learning model but not the weights at all and train it according to our dataset. The model is learning from scratch, and will take a longer time and need a larger dataset to finish. Even though we trained it from scratch, sometimes the accuracy is still lower than using pretrained models if the dataset in $P_2$ cannot provide good features (Here good features refer to the features that have similar patterns with the images when we actually serve the model.). 
\end{itemize}

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{types_of_transfer_learning.pdf}
		\caption{Illustration of different strategies for transfer learning.}
		\label{fig_2}
	\end{figure}
\end{center}

\subsection{Summary}

In this section, we introduce the transfer learning in more detail. By using transfer learning, we could achieve higher accuracy with less computational power and dataset. The computational power and dataset are two bottlenecks for deep learning applications, and since transfer learning can tackle the problem, it soon becomes the de facto best practices in deep learning tasks. The key idea behind transfer learning is simple: fix some generic, problem-agnostic layers, and train other problem-specific layers on new dataset. 

Even though the idea and implementation of transfer learning is simple and straightforward, it needs to be pointed out that the use of transfer learning is wide and promising. It is widely used as most data scientists are now using pretrained models open-sourced by big companies and leading research institutes rather than build it from scratch because it saves costs and time. At the same time, big companies and research institutes are willing to open source their pretrained models trained on extremely huge dataset. For example, Tencent released its largest multi-label image dataset named Tencent ML Images \cite{wu2019tencent} in 2019 with 20 million images and 10 thousands categories. It is promising because the idea behind transfer learning: extract the generic knowledge from the data, and then apply it somewhere else, seems to be (though there's no evidence to prove this yet) important to achieve general intelligence.

\section{Example of Multiple Solvers}

The key motivation behind allowing multiple solvers in an AID package is because different solvers can share something in common with this mechanism. These includes weight files, dependencies, environment variables and other configurations. One important use case of multiple solvers in a single package is when users need to perform transfer learning. In this section, we will take an example in transfer learning to find out how we organize multiple solvers in a single AID package. 

\subsection{Problem Statement} 
Assume we have a pretrained image classification model based on the ImageNet dataset, and we now want to build a new image classification model to classify cats with 10 different colors. It can be divided into the following phases:

\begin{itemize}
	\item $P_1$: In $P_1$, we want to use an existing pretrained model as feature extractor. In this phase, we can simply download an existing pretrained model and read its weight file. We will denote the model in this phase as $M_1$.
	\item $P_2$: In $P_2$, we will be given a smaller dataset with cats (in different colors) images. In this phase, we will train the classifier, i.e. the fully connected layers from our own data. We will denote the model in this phase as $M_2^{i}$ (there might be several models for testing, thus there is an $i$ in the superscripts).
\end{itemize}

Then when users upload an image, we will first use the solver for $P_1$ to identify the objects in the image. If there is any cat, we will use the solver for $P_2$ to find the color of the cat. If there is no cat but something else, we will use other solvers or tells users that we cannot process the image.

One challenge for $P_2$ is that it is impossible to predict how accurate our deep learning model will be without the experiments. Therefore, in practice we usually repeat $P_2$ for many times. These challenges applies for both test dataset and users' requests.

\begin{itemize}
	\item For the test dataset, we collect a small amount of data, build the classifier, and then if we find out it does not work very well, we will collect more data, rebuild the classifier, and test it again. This process will be repeated until we are satisfied with the accuracy on our test dataset.
	\item When the training dataset is large enough and our classifier works well on the test dataset, we will then serve it online. After the deployments, we can periodically ask users or use other techniques to monitor the accuracy of our model. If it is not accurate as we expected, we then need to repeat the data collection and classifier building process again to acquire a new classifier.
\end{itemize}

From the solutions to the challenge, we find that there will be more than one models to evaluate. If we package those solvers in different isolate packages, we will need to store the $M_1$ and all $M_2^i$ models and it will cost a total size of $n\times sizeof(M_1)+\sum_{i=1}^{n}sizeof(M_2^i)$. But if we can make the $M_1$ shareable across different solvers, the disk space that we need will be $sizeof(M_1)+\sum_{i=1}^{n}sizeof(M_2^i)$.

The size of the pretrained model is sometimes large. As examples, we have listed some recent pretrained models in image classification and natural language processing and their size in Table. \ref{table_1}:

\begin{center}
	\captionof{table}{Table of pretrained models, their task area and size. Note that the T5 model has not been released yet and therefore we can only estimate its size by the number of parameters it has.} 
	\begin{tabular}{||c c c c||}
		\hline
		Model Name        & Task Area                   & Model Size     &   \\ [0.5ex] 
		\hline\hline
		VGG-16            & Computer Vision             & 528 MB         &   \\ 
		\hline
		Xception          & Computer Vision             & 88 MB          &   \\
		\hline
		ResNet            & Computer Vision             & 98 MB - 232 MB &   \\
		\hline
		Tencent ML Images & Computer Vision             & 465 MB         &   \\
		\hline
		BERT-Large        & Natural Language Processing & 1.2 GB         &   \\
		\hline
		T5                & Natural Language Processing & aprox. 700 GB  &   \\
		\hline
	\end{tabular}
	 
	\label{table_1}
\end{center}

This is a real issue when we try to manage the models with an online deep learning service. In a previous project\footnote{IndustryAI: https://www.youtube.com/watch?v=T1GqKRe7OlQ}, we did not make the $M_1$ shareable, and then when users trying to train their models with transfer learning, it sometimes do not have enough disk space.

\subsection{Solutions in AID}

In AID, even though we do not plan to directly support training phase, we still need to take the challenge into consideration because we still need to serve different solvers online and collect the feedbacks from users. We allow solvers to share the same weight file as well as other configurations. The organization of multiple solvers are illustrated in Fig. \ref{fig_3}.

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{multiple_solvers.pdf}
		\caption{Illustration of Multiple Solvers in AID.}
		\label{fig_3}
	\end{figure}
\end{center}

In this example, we use a single weight file to store the weights of convolutional layers and two different files to store the weights of fully connected layers. If there're new solvers that are built on top of the same convolutional layers, we can only store the weight for the third solver, rather than the whole weight file. By doing so, we can save the disk spaces for the weights of convolutional layers and other configurations.

Besides the disk space issues, there are some other benefits of putting multiple solvers in a single package:
\begin{itemize}
	\item Requests between different solvers do not necessarily rely on HTTP. As we can see in this illustration, $P_1$ solver can directly use the infer function in $P_2$ solver. By doing so we can eliminate the extra cost caused by HTTP requests.
	\item $P_2$ solver will be forced to use exactly the same version of libraries with $P_1$ solver. Without AID, when performing transfer learning, users have to install the dependencies manually and sometimes the versions maybe mismatched. With AID, they are put into a single package and shared the same dependencies.
\end{itemize}

\subsection{Summary}

In summary, there are many things in common in two different deep learning models, including the weight file, the dependencies, environment variables, etc. By allowing multiple solvers in a single package, we can eliminate the redundancies, save the disk spaces, reduce the overhead between HTTP requests and benefit from the same versions of dependencies.

\section{Current Implementation of Redistributing Requests}

In practice, the inference is time-consuming, and may cause failure in users requests. To tackle the problem, AID supports distributing requests to different nodes to improve the performance. In this section, we will introduce how AID helps to solve the problem in more detail, and because currently the implementation is still naive and trivial, we will discuss current limitations and some possibilities in future development.

When serving the deep learning model, the serving program will need to start a web server that reads the user's input, and return the predict value. In AID, every solver can be packaged into a self-contained image, and then exported to a single file. Users can easily send the file into different nodes, and use AID to import it again. After the import, users can then serve the deep learning model on new nodes. Then when users are requesting prediction on the first node, AID will randomly redistribute the requests to different nodes. The process is illustrated in Fig. \ref{fig_4}

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{distribute_requests.pdf}
		\caption{Illustration of how AID is redistributing requests.}
		\label{fig_4}
	\end{figure}
\end{center}

Though the implementation is simple and trivial (it is barely a simple load balancer), it needs to be pointed out that the reasons why we can do so is important. When designing this part, we force every solver to be \textbf{stateless}, which means the user's requests do not influence how the serving works, and the serving program make the prediction based only on the single request. That is to say, our inference must not use data from earlier requests.

We've made a small experiment to demonstrate that the method is useful when handling lots of requests. The results are illustrated in table \ref{table_2}, and we can conclude that we can greatly improve the speed of requests (the P(95) duration, which means the time duration when 95 percent requests can be finished, reduced from 21 seconds to 1.99 seconds) and the robustness (the successful rates grow from less than 95 percent to 100 percent even with more requests) of our serving program.

\begin{center}
	\captionof{table}{The comparison between using distribution serving or not. Here the P(95) during means the time duration when 95 percent requests can be finished. Each node is equipped with a 2-core EPYC CPU and 4 GB RAM and all of them are located in a data center in China. We perform the load tests with maximum 500 virtual users from Zurich.} 
	\begin{tabular}{|c c c c|}
		\hline
		w/o distributing & Total Requests & Successful Rates & P(95) duration (s) \\ [1ex] 
		Distributing     & 2294           & 100              & 1.99               \\ 
		Non-Distributing & 623            & 94.25            & 21                 \\ [1ex]
		\hline
	\end{tabular}
	\label{table_2}
\end{center}

However, such implementations has its shortcomings:
\begin{itemize}
	\item It requires each node to read the full model and works independently. For very large models (e.g. the 700 GB language model), our current mechanism can hardly help and will cost a lot. It would be interesting to find ways to distribute different layers to different nodes, and use several smaller nodes to perform inference on a large model. For example, can we use 200 nodes, each node has 4 GB RAM and 800GB RAM in total, to load the language model to perform inference? If so, how can we efficiently store and manage the weight of different layers?
\end{itemize}

\section{Inference Engine}

\subsection{Definition and Motivation}

In the above section, we have identified the problem of inference: it is time-consuming and may cause failures to perform lots of inferences in a short time period. In AID, we simply distribute the requests to different servers and try to improve the quality of service. However, there are many cases where we cannot use distributed servers to tackle the problem. For example, some deep learning models are running on users' mobile devices so that users can use deep learning when they are offline, and in this case, we do not have multiple nodes. 

In practice, we have two objectives for the inference program: it is accurate and fast as well. Usually for the same model, these two expectations cannot be met at the same time. Therefore, we want our inference program to find a balance between these two factors. \textbf{The \textit{Inference Engine} is such a system that loads a deep learning model, process it with some techniques and then perform the inference.} The techniques people used in an inference engine aims to improve the inference speed while maintaining acceptable accuracies.

\subsection{Types of Inference Engine}

The inference engines can be roughly categorized into two types:
\begin{itemize}
	\item Hardware Accelerator. In convolutional neural networks, the most common operation is matrix multiplication and it can be parallelized efficiently. Therefore, a cheap GPU with thousands computing unit can outperform an expensive desktop CPU. Then a natural idea is that we can build some cheap hardware, which has limited single-core performance (e.g. hundreds MHz) but has lots of computing unit. By utilising the multi-core architecture, we could boost the performance of the hardware on deep learning inference because they can efficiently perform matrix multiplication, but they can hardly do anything else. Besides this, in generic-purpose CPU and GPU, we need to support 32-bit floating-point numbers, but for the deep learning task, we can compromise on this. We can support only 8-bit floating-point numbers so that we can perform computation faster with some loss in accuracy. In Fig. \ref{fig_6}, we showed a real setup of using specialized deep learning chips to perform inference. This is a fast-growing field, and many companies (e.g. The deep learning chip in \ref{fig_5} is from Intel) and institutes (e.g. Institute for Neural Informatics at UZH) are working on deep learning chips.
	
	\item Software Accelerator. In contrast to hardware accelerators, we can also improve the speed of inference from the aspects of software. Since the software accelerator is what we are interested in, we will introduce more techniques in the next subsection.
\end{itemize}

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{dlchip.pdf}
		\caption{An example of Deep Learning Chip. It uses an ARMv7 CPU (\$35), with almost no deep learning ability, and a deep learning chip (\$100). With this cheap setup, we can perform body detection in real time, which may cost \$1,000 if we perform inference on desktop CPU.}
		\label{fig_6}
	\end{figure}
\end{center}

\subsection{Techniques in Software Accelerator}

In this section, we will go over some techniques in accelerating the inference speed of deep learning models. This section is largely inspired from \cite{sze2017efficient}.

\subsubsection{Quantitization}

One direct inspiration from the hardware accelerators is that we can make every parameter in the weight file to be less accurate. For example, we can approximate $2.1001$ to be $2.1$ or even $2$. In practice, we first find out the maximum and minimum of our weight, and then map all the values into $[-127,128]$, i.e. the range of int8. In summary, quantitization is the process of mapping weight into a range where we can reduce the computational cost.

This process will make our model less accurate, but still acceptable. We can also expand the range from int8 to int32 or larger. The motivation behind quantitization is that in our computing unit, integer operations is faster then floating operations.

\subsubsection{Pruning}

The idea of pruning (or cutting branch) is simple: if some parameters in the weight is near $0$, then we directly set it as $0$. If the parameters are zero, then it will not be connected to other nodes. Then our neural network will look sparse, as shown in Fig \ref{fig_7}.

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{pruning.pdf}
		\caption{An example of Deep Learning Chip. It uses an ARMv7 CPU (\$35), with almost no deep learning ability, and a deep learning chip (\$100). With this cheap setup, we can perform body detection in real time, which may cost \$1,000 if we perform inference on desktop CPU.}
		\label{fig_7}
	\end{figure}
\end{center}

After the pruning, our models will be more sparse, and we can then perform some compression so that our models will occupy less disk space.

\section{MLOps System}

In last week's meeting, we discussed a bit about MLOps but it is still not very clear. In the end of this article, we want to address the importance of MLOps system and why it deserves some database research. 

\subsection{Definition of MLOps}

MLOps is the abbreviation of Machine Learning Operations, and it is a concept built on top of DevOps (the combinations of software development and IT Operations). The principal goal of MLOps, from the Microsoft Azure \cite{mlops_azure}, includes:

\begin{itemize}
	\item Faster experimentation and development of models. This usually happens in the training phase where users build the neural network architecture, feed in the training dataset and perform the training and some experiments.
	\item Faster deployment of models into production. This is in the serving and inference phase. Here we interpret the "faster" to be of two-fold:
  \begin{itemize}
    \item We can deploy the deep learning models faster. This means we can quickly integrate the deep learning models into either a web service or into users' mobile phone. Here the goal of MLOps system is to reduce the redundant code in converting models to web server or device-specific code.
    \item We can deploy faster deep learning models. This means we can run the inference faster with specific inference engine, compared with generic deep learning frameworks (e.g. TensorFlow, PyTorch, etc).
  \end{itemize}
	\item Quality Assurance. This means we need to ensure our deep learning models can satify our quality requirements. In the context of machine learning, the quality usually refers to the accuracy of prediction. From the side of deployment, the quality usually means the quality of service, the latency of responding, etc.
\end{itemize}

MLOps is tightly connected with the machine learning systems in production, and to figure out what problem they are trying to solve, we need to find out what problems are there in machine learning systems. In \cite{43146}, some developers from Google mentioned some pitfalls in operation machine learning systems in production. For example, "data scientists can implement and train an ML model with predictive performance on an offline holdout dataset, given relevant training data for their use case. However, the real challenge isn't building an ML model, the challenge is building an integrated ML system and to continuously operate it in production". 

Similarly, in the first book about MLOps, \cite{mlops_book}, they also mentioned that "Data scientists are not software engineers: Most are specialized in model building and  assessment,  and they are not necessarily experts in writing applications. Though this may  start to shift over time as some data scientists become special‐ists more on  the deployment or operationalization side, for now, many data scientists find  themselves having to juggle many roles, making it challenging to do any of  them thoroughly. Data scientists being stretched too thin becomes especially  problematic at scale with increasingly more models to manage. The complexity becomes exponential when considering the turnover of staff on data teams when suddenly, data scientists have to manage models they did not create."

In summary, from the problems they proposed, we conclude that MLOps is a field that tries to fill the gap between model building and model serving. The problems it tries to resolve is that the data scientists and software engineers are using two different technical stacks, and it is challenging for data scientists to build an integrated machine learning system and continuously operate it in production.

\subsubsection{Database Researches in MLOps}

The field of MLOps is new, and it is hard to judge if it deserves some effort in doing some researches in this field. In this section, we will list and discuss some possibilities for research in this field.

\begin{center}
	\begin{figure}[h!]
		\includegraphics[width=330pt]{venn.pdf}
		\caption{Illustration of research topics in MLOps System. ML: Machine Learning. HCI: Human Computer Interaction. DB: Database}
		\label{fig_5}
	\end{figure}
\end{center}

From different aspects in computer science, we listed some interesting topics in Fig. \ref{fig_5}. They are explained as below:
\begin{itemize}
  \item \textbf{Machine Learning}: In the machine learning community, people usually focus on how to build and assess the deep learning model. In the MLOps systems, they might be interested in continuously fine-tuning the models (it is also called \textit{lifelong learning}) over time and users' feedback. Unlike manually labelled data, the users' input might be wrong, and thus it is interesting and important to find out the wrong feedbacks.
  \item \textbf{Human Computer Interaction}: As we are building a system focus on machine learning, it is natural for HCI-enthusiasts to investigate how users could interact with the system. Since we collect large dataset for building our model, then how do we visualize these data for developers to understand? Can we allow users to interact with our deep learning models to learn how it actual works? This field of research may also include \textit{Interpretability of Machine Learning Models} as visualization and interaction are important ways to interpret.
  \item \textbf{Database Research}: The first questions that the database community may have is that how we can abstract and model the common components in different deep learning models. By using a unified interface for a wide variety of deep learning models, we could simplify our MLOps system as we do not need to support every framework and every model. Another interesting question is that because deep learning models are a collection of information and data, can we load the models into a database? If we could load the deep learning model into a database, can we then use the optimizations in the database to optimize the query for deep learning models? The answer to the question is also important to MLOps system. For example, when we want to manipulate a model, currently we usually load the whole model into the RAM, and read all the parameters, then find the value that we need to change. If we can partially read the model into our memory, it will save our time and cost. Moreover, like other systems, can we design a data warehouse to store, process and analyse the data that we collected in the MLOps system? For example, the users' feedback, the models' performance, etc.
\end{itemize}

\bibliographystyle{plainnat}
\bibliography{refs.bib}

\end{document}
