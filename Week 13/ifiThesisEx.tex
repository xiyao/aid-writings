\documentclass[abstracton,12pt]{scrreprt}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{times}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{neuralnetwork}
\usepackage[ruled,vlined]{algorithm2e}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
\lstset{style=mystyle}

% --------- 

\titlehead{Department of Informatics, University of Zürich}
\subject{\vspace*{2cm}MSc Basic Module}
\title{Implementing Deconvolution to Visualize and Understand Convolutional Neural Networks}
\author{
  Xiaozhe Yao\\[-5pt]
  \scriptsize Matrikelnummer: 19-759-570\\[-5pt]
  \scriptsize Email: \texttt{xiaozhe.yao@uzh.ch}
}
\date{\vspace*{2cm}\today}
\publishers{
  % \small supervised by Prof.\ Dr.\ x.\ yyy and y.\ zzz \\[5cm]
  \begin{tikzpicture}[overlay]
    \node at (-3,-3) {\includegraphics[height=1.5cm]{IFIlogo}};
    \node at (7,-3) {\includegraphics[height=1.5cm]{dbtgBW}};
  \end{tikzpicture}
}

% --------- 

\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newenvironment{proof}
  {\noindent{\bf Proof:\rm}}{\hfill$\Box$\vspace{\medskipamount}}

\def\bbbr{{\rm I\!R}}
\def\bbbm{{\rm I\!M}}
\def\bbbn{{\rm I\!N}}
\def\bbbz{{\rm I\!Z}}

% --------- 

\begin{document}

\maketitle

% \chapter*{Acknowledgements}

% \tableofcontents

\chapter{Introduction}

Convolutional neural networks (CNNs) have played an important role in computer vision tasks, such as image classification, object detection, etc. The main motivation is that CNNs could automatically extract features from an image dataset. However, it is still unclear what features are extracted and why CNNs could achieve such good performance. Without a clear explanation and understanding about how CNNs work, the development of better models will become trial-and-error. Hence, the lack of interpretability is deeply unsatisfactory. To tackle the problem, \cite{matthew2014visualizing} proposed an approach to visualize the intermediate features learned by CNNs to help people understand how CNNs work. This approach was proven to be useful in revealing the learned features and designing better neural networks.

Visualizing the convolution layers is challenging because we cannot map the learned features back to pixels. A possible solution, proposed by \cite{matthew2014visualizing}, is the deconvolutional network. The primary components in CNNs are convolution, pooling and relu layers. In contrast, the components in a deconvolutional network are deconvolution, unpooling and unrelu layers. These layers can be regarded as an inverse of the corresponding layer in CNNs and could map the learned features back to pixels. The goal of this basic module is to implement the deconvolutional network from scratch and apply it to visualize the intermediate features of convolution layers.

This report presents a step-by-step explanation of the mechanisms of CNNs and deconvolutional networks. Besides, we will also train a standard CNN and attach a corresponding deconvolutional network to the CNN to visualize and understand the learned features. Finally, the implementation will be compared with a PyTorch version and we will investigate the differences between these two implementations.

We will organize this report into the following sections:

\begin{itemize}
  \item \textbf{Preliminaries}. In this section, we will briefly introduce the principal operations in convolutional and deconvolutional neural networks. Additionally, we will also introduce the fundamental knowledge for training a standard CNN, including the \textit{Gradient Descent} and the \textit{Backpropagation}. 
  \item \textbf{Approach}. We will thoroughly explain and induce the computation rules for the convolutional and deconvolutional networks in this section.
  \item \textbf{Running Example}. In this section, we will present a running example of applying the CNNs for image classification. Besides, we will also demonstrate how to attach a deconvolutional network to visualize the learned features.
  \item \textbf{Implementation}. We will explain how we implement the training, evaluating and visualizing processes in this section. 
  \item \textbf{Experiment}. In this section, we will present the training settings in our classification model. After that, we will use this model to classify a set of images and provide the accuracy on that dataset. Then we will attach a deconvolutional network to visualize the learned features in CNNs and try to explain how our classification model works. Finally, we will compare our results with a given PyTorch implementation.
\end{itemize}

\begingroup
\let\clearpage\relax
\chapter{Preliminaries}
\endgroup

\noindent
\textbf{Convolutional Neural Network} Convolutional neural network is a class of neural networks. They are usually formed by three principal components: the convolution layers, the pooling layers and the fully connected layers. The convolutional layers and the fully connected layers are equipped with parameters called \textit{weights} and \textit{biases}. Then we define two operations for a CNN: the \textit{forward pass} and the \textit{backward pass}. In the forward pass, we compute the output with the given weights and biases and compare the output with the ground truth. In contrast, we update the weights and biases with the computed differences between the ground truth and the predicted output in the backward pass. The measurement for the differences is called \textit{loss functions}. When constructing the classification model, we perform the forward pass and backward pass for several times to make the differences as small as possible. This process is called \textit{training} and the technique we used to update the parameters is called \textit{Stochastic Gradient Descent}.

\null
\noindent
\textbf{Stochastic Gradient Descent}. As said before, we want to find the parameters that make the differences between the ground truth and the predicted output minimal. Formally, we want to find parameters $A$ for a function $\hat{Y}=F_A(X)$ such that the differences $\ell(\hat{Y}, Y)$ is minimal. The function $F_A(X)$ in neural networks is usually non-linear, non-convex and we do not have formula for it, hence gradient descent becomes the most popular method to find the local minimum of the differences. Gradient descent is based on a fact that a function $f$ decreases fastest along the negative direction of the gradient. In the process of gradient descent, for each parameter $a$ in $A$, we first compute the gradient of the difference with respect to the parameter, i.e. $\frac{\partial \ell}{\partial a}$, and then update the parameter with $a^{new}=a-\epsilon\frac{\partial \ell}{\partial a}$ where $\epsilon$ is a preset value called \textit{learning rate}. We could repeat the updates for several times until the difference is satisfactory. Now the only problem left is how do we compute the gradient, i.e. $\frac{\partial \ell}{\partial a}$? In neural networks, we usually use backpropagation to compute the gradient of the loss function with respect to the parameters in the network.

\null
\noindent
\textbf{Backpropagation}. In CNNs, we already have the definition of each layer and we can compute the output with the given weights. With the definition of each layer, we can compute $\frac{\partial y}{\partial a}$. In order to compute $\frac{\partial \ell}{\partial a}$, we can apply the chain rule. Then we will have $\frac{\partial \ell}{\partial a}=\frac{\partial \ell}{\partial y}\frac{\partial y}{\partial a}$. Since $\frac{\partial y}{\partial a}$ is known to us, we only need to figure out $\frac{\partial \ell}{\partial y}$, i.e. the gradient of the loss function with respect to the output of this layer. Another useful property in CNNs is that the output of the $i$th layer is the input of the next layer. Hence, in order to compute the desired gradient, we compute the gradient of the loss function with respect to the input of the next layer instead. Iteratively, we could repeat this procedure until the last layer, and in the last layer, we can compute the gradient of the loss function with respect to the input. 

From the above description, we see that in order to compute the gradient of the loss function with respect to the parameters of a single layer, we need to iterate the process until the last layer. It is inefficient and will cause lots of redundant computations. Therefore, in the backward pass, we start from the loss function and compute the gradient of the loss function with respect to the predicted output. After that, we compute the gradient of every parameter in a reversed order, i.e. from the last layer to the first layer. In each layer, we compute the gradient of the loss function with respect to three values: the weight $\frac{\partial \ell}{\partial w}$, the bias $\frac{\partial \ell}{\partial b}$ and the input $\frac{\partial \ell}{\partial x}$. After that, we update the weight and bias with gradient descent and pass $\frac{\partial \ell}{\partial x}$ to the previous layer.

\null
\noindent
\textbf{Notations} In the below sections, we will use the following notations:
\begin{itemize}
  \item $x_{j}^{(i)}$ or $x_{kj}^{(i)}$ is the single input of the $i$th layer. If the input is a vector, we will use $x_{j}^{(i)}$ to denote the $j$th item in the vector. In contrast, if the input is a matrix, we will use the $x_{kj}^{(i)}$ to denote the $(k, j)$ element of the matrix. We will use $X^{(i)}$ to represent the vector or matrix of the input.
  \item Similarly, $\hat{y}_{j}^{(i)}$ or $\hat{y}_{kj}^{(i)}$ is the single output of the $i$th layer. Depending on the shape of the output, we will use different subscripts. We will use $\hat{Y}^{(i)}$ to represent the vector or matrix of the input. The output of the last layer in a neural network has corresponding ground truth and the ground truth will be denoted by $Y$. Additionally, the output of the network will be denoted by $\hat{Y}$.
  \item $\nabla_Xf$ refers to the the derivative of function $f$ with respect to $X$, i.e. $\nabla_Xf=\frac{\partial f}{\partial X}$. In order to make the notations cleaner, we will use $\nabla^{(i)}$ to denote the gradient of the loss function with respect to the output of the $i$th layer. Similarly to the notations in the input and output, we will use $\nabla_{j}^{(i)}$ or $\nabla_{kj}^{(i)}$ to denote the single element in the gradient.
  \item $\ell(\hat{Y}, Y)$ is the loss function. It computes the differences between the ground truth and the predicted output. In this report, we will by default use the mean square loss, defined by $\ell=\frac{1}{n}\sum_{n}(y_i-\hat{y}_i)^2$, to demonstrate the computation process.
  \item We use some other notations for mathematical concepts and they are listed below:
  \begin{itemize}
    \item \textit{Scalars and Matrices}. $\mathbb{R}$ is for the set of real numbers and $\mathbb{R}^{m\times n}$ is for the set of $m\times n$ real matrix. For elements in the matrix $A$, we will use the corresponding lower-case letter with subscripts $a_{kj}$ to denote the single element. Besides, we will use $\epsilon$ to represent a small enough real number. 
    \item \textit{Functions}. We use $f: \mathbb{A}\to\mathbb{B}$ to denote the function with the domain $\mathbb{A}$ and the range $\mathbb{B}$.
    \item \textit{Matrix Transpose}. If we have $A\in\mathbb{R}^{m\times n}$, we will use $A^T$ to denote its transpose.
    \item \textit{Matrix Differentiation}. If we have $A\in\mathbb{R}^{m\times n}$, and $f:A\to\mathbb{R}$, then we define $$\frac{\partial f}{\partial A}=\left[\begin{array}{*{20}c}
      \frac{\partial f}{a_{11}} & \cdots & \frac{\partial f}{a_{1n}} \\
      \vdots  & \frac{\partial f}{a_{kj}} & \vdots \\
      \frac{\partial f}{a_{m1}} & \cdots & \frac{\partial f}{a_{mn}} \\
    \end{array}\right]$$
  \end{itemize}
\end{itemize}

\chapter{Approach}

In this work, we will build a classification model based on CNNs to categorize the input image and an interpretation model based on deconvolutional networks to perform the visualization. In order to train the CNN on an image dataset, we will need to know how to perform the forward pass and the backward pass for every component of CNNs. In contrast, the deconvolutional network are not used in any learning capacity. More specifically, the deconvolutional network will use the learned weight in the corresponding CNN, and hence we do not need to perform the backward pass for the deconvolutional network. 

In this section, we will first provide the preprocessing of the image dataset, and then thoroughly induce the forward and backward rules of the CNNs. After that, we will introduce how to perform the forward of the deconvolutional networks.

\section{Preprocessing}

\textit{\underline{This section is planned to be finished in the next week}}

\noindent
In this section, we will introduce how to transform the images into matrices.

\section{Classification Model}

In the classification model, we build a convolutional neural network on top of three components: the convolution layer, the pooling layer and the fully connected layer. In this section, we will explain induce how to compute the forward and backward pass of these three components. As the fully connected layer and convolution layer are both linear transformations, which we will show later, it is fundamental to know how to perform the differentiation for linear transformations. Hence, before we introduce the forward and backward pass of these three components, we will first explain how to compute the gradient for linear transformations.

\subsection{Differentiation of Linear Transformations}

Assume that we have two functions, one turns an $m\times n$ matrix into a real number, another function is a linear function which receives a matrix, and transforms the matrix into another matrix. Besides, the new matrix is an $m\times n$ matrix so that it can be converted into a real number by the first function. Then we want to figure out how the input matrix will impact the real number.

Formally, we can define the problem as follows: we have function $f(Y):\mathbb{R}^{m\times n}\to\mathbb{R}$ and a linear function $g(X): \mathbb{R}^{p\times n}\to\mathbb{R}^{m\times n}$. As $g(X)$ is a linear function, we will define $Y=g(X)=AX+B$ where $A\in\mathbb{R}^{m\times p}$ and $B\in\mathbb{R}^{m\times n}$. Then we want to compute $\frac{\partial f}{\partial X}$. We could solve this problem step by step as below:

\begin{itemize}
  \item Recall that at the point $x_0$, if there are two functions $u=\phi(x)$ and $v=\psi(x)$ that have partial derivatives with respect to $x$ defined, i.e. we have $\frac{\partial u}{\partial x}$ and $\frac{\partial v}{\partial x}$ defined. Then if we define a composited function $f(u(x), v(x))$, we can compute the partial derivative of $f$ with respect to $x$ by applying the chain rule. Formally, we will have $\frac{\partial f}{\partial x}=\frac{\partial f}{\partial u}\frac{\partial u}{\partial x}+\frac{\partial f}{\partial v}\frac{\partial v}{\partial x}$. For this composite function $f(u(x), v(x))$, we can think of $u(x)$ and $v(x)$ as the intermediate variables. In our case, we denote the intermediate variables as $y_{kl}$. By applying the chain rules, we will have $\frac{\partial f}{\partial x_{ij}}=\sum_{kl}\frac{\partial f}{\partial y_{kl}}\frac{\partial y_{kl}}{\partial x_{ij}}$ (1).
  \item From the definition of $g(X)$, we know that $y_{kl}=\sum_{s}a_{ks}x_{sl}+b_{kl}$. Hence we have $\frac{\partial y_{kl}}{\partial x_{ij}}=\frac{\partial \sum_{s}a_{ks}x_{sl}}{\partial x_{ij}}$. Among these numerators, only $a_{ki}x_{il}$ is possible to have impact on $x_{ij}$. Therefore, we will have $\frac{\partial \sum_{s}a_{ks}x_{sl}}{\partial x_{ij}}=\frac{\partial a_{ki}x_{il}}{\partial x_{ij}}$. Then we found that only when $l=j$, the partial derivative will be non-zero. Hence, we will have $\frac{\partial a_{ki}x_{il}}{\partial x_{ij}}=a_{ki}\delta_{lj}$ and $\delta_{lj}=1$ when $l=j$, otherwise $\delta_{lj}=0$. In summary, we have $\frac{\partial y_{kl}}{\partial x_{ij}}=a_{ki}\delta_{lj}$ (2).
  \item Take (2) into (1), we will have 
\end{itemize}

\bibliographystyle{alpha}
\bibliography{refs.bib}

\end{document}